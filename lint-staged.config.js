module.exports = {
  '*.ts': ['pnpm affected:lint --fix --files', 'pnpm format:write --files'],
};
