import { DependencyInjectionContainer } from '@unvision/di';

import { OAuth2Client } from '../entities/oauth2-client.entity';
import { OAuth2Consent } from '../entities/oauth2-consent.entity';
import { OAuth2User } from '../entities/oauth2-user.entity';
import { InvalidRequestException } from '../exceptions/invalid-request.exception';
import { AUTHORIZATION_CODE_SERVICE, PKCE_METHOD } from '../metadata/metadata.keys';
import { PkceMethod } from '../pkce-methods/pkce-method';
import { OAuth2AuthorizationCodeService } from '../services/oauth2-authorization-code.service';
import { CodeAuthorizationRequest } from '../types/code.authorization-request';
import { CodeAuthorizationResponse } from '../types/code.authorization-response';
import { CodeResponseType } from './code.response-type';

const client = <OAuth2Client>{ id: 'client_id' };
const user = <OAuth2User>{ id: 'user_id' };

describe('Code Response Type', () => {
  let responseType: CodeResponseType;

  const authorizationCodeServiceMock = jest.mocked<OAuth2AuthorizationCodeService>({
    create: jest.fn(),
    findOne: jest.fn(),
    revoke: jest.fn(),
  });

  const pkceMethodsMocks = [
    jest.mocked<PkceMethod>({ name: 'plain', verify: jest.fn() }),
    jest.mocked<PkceMethod>({ name: 'S256', verify: jest.fn() }),
  ];

  beforeEach(() => {
    const container = new DependencyInjectionContainer();

    container.bind<OAuth2AuthorizationCodeService>(AUTHORIZATION_CODE_SERVICE).toValue(authorizationCodeServiceMock);
    pkceMethodsMocks.forEach((pkceMethod) => container.bind<PkceMethod>(PKCE_METHOD).toValue(pkceMethod));
    container.bind(CodeResponseType);

    responseType = container.resolve(CodeResponseType);
  });

  describe('constructor', () => {
    it('should reject not providing any pkce methods.', () => {
      expect(() => new CodeResponseType(<OAuth2AuthorizationCodeService>authorizationCodeServiceMock, [])).toThrow(
        TypeError
      );
    });
  });

  describe('name', () => {
    it('should have "code" as its name.', () => {
      expect(responseType.name).toBe('code');
    });
  });

  describe('defaultResponseMode', () => {
    it('should have "query" as its default response mode.', () => {
      expect(responseType.defaultResponseMode).toBe('query');
    });
  });

  describe('handle()', () => {
    let parameters: CodeAuthorizationRequest;

    beforeEach(() => {
      parameters = { response_type: 'code', client_id: '', redirect_uri: '', scope: 'foo bar', code_challenge: '' };
    });

    it('should reject not providing a "code_challenge" parameter.', async () => {
      Reflect.deleteProperty(parameters, 'code_challenge');

      const consent = <OAuth2Consent<CodeAuthorizationRequest>>{ client, parameters, user };

      await expect(responseType.handle(consent)).rejects.toThrow(InvalidRequestException);
    });

    it('should reject providing an unsupported "code_challenge_method".', async () => {
      Reflect.set(parameters, 'code_challenge_method', 'unknown');

      const consent = <OAuth2Consent<CodeAuthorizationRequest>>{ client, parameters, user };

      await expect(responseType.handle(consent)).rejects.toThrow(InvalidRequestException);
    });

    it('should create a code authorization response.', async () => {
      const expected: CodeAuthorizationResponse = { code: 'authorization_code', state: undefined };
      const consent = <OAuth2Consent<CodeAuthorizationRequest>>{ client, parameters, user };

      await expect(responseType.handle(consent)).resolves.toStrictEqual(expected);
    });

    it('should create a code authorization response and pass the state unmodified.', async () => {
      Reflect.set(parameters, 'state', 'client-state');

      const expected: CodeAuthorizationResponse = { code: 'authorization_code', state: 'client-state' };
      const consent = <OAuth2Consent<CodeAuthorizationRequest>>{ client, parameters, user };

      await expect(responseType.handle(consent)).resolves.toStrictEqual(expected);
    });
  });
});
