import { DependencyInjectionContainer } from '@unvision/di';

import { OAuth2AccessToken } from '../entities/oauth2-access-token.entity';
import { OAuth2Client } from '../entities/oauth2-client.entity';
import { OAuth2Consent } from '../entities/oauth2-consent.entity';
import { OAuth2User } from '../entities/oauth2-user.entity';
import { InvalidRequestException } from '../exceptions/invalid-request.exception';
import { ACCESS_TOKEN_SERVICE } from '../metadata/metadata.keys';
import { OAuth2AccessTokenService } from '../services/oauth2-access-token.service';
import { AuthorizationRequest } from '../types/authorization-request';
import { TokenAuthorizationResponse } from '../types/token.authorization-response';
import { TokenResponseType } from './token.response-type';

const client = <OAuth2Client>{ id: 'client_id' };
const user = <OAuth2User>{ id: 'user_id' };

const accessTokenServiceMock = jest.mocked<OAuth2AccessTokenService>({
  create: jest.fn(async (scopes) => {
    return <OAuth2AccessToken>{ token: 'access_token', scopes, expiresAt: new Date(Date.now() + 3600000) };
  }),
  findOne: jest.fn(),
  revoke: jest.fn(),
});

describe('Token Response Type', () => {
  let responseType: TokenResponseType;

  beforeEach(() => {
    const container = new DependencyInjectionContainer();

    container.bind<OAuth2AccessTokenService>(ACCESS_TOKEN_SERVICE).toValue(accessTokenServiceMock);
    container.bind(TokenResponseType).toSelf().asSingleton();

    responseType = container.resolve(TokenResponseType);
  });

  describe('name', () => {
    it('should have "token" as its name.', () => {
      expect(responseType.name).toBe('token');
    });
  });

  describe('defaultResponseMode', () => {
    it('should have "fragment" as its default response mode.', () => {
      expect(responseType.defaultResponseMode).toBe('fragment');
    });
  });

  describe('createAuthorizationResponse()', () => {
    let parameters: AuthorizationRequest;

    beforeEach(() => {
      parameters = { response_type: 'token', client_id: '', redirect_uri: '', scope: 'foo bar' };
    });

    it('should reject using "query" as the "response_mode".', async () => {
      Reflect.set(parameters, 'response_mode', 'query');

      const consent = <OAuth2Consent<AuthorizationRequest>>{ client, parameters, scopes: ['foo', 'bar'], user };

      await expect(responseType.handle(consent)).rejects.toThrow(InvalidRequestException);
    });

    it('should create a token response with the default scope of the client.', async () => {
      const consent = <OAuth2Consent<AuthorizationRequest>>{ client, parameters, scopes: ['foo', 'bar'], user };

      await expect(responseType.handle(consent)).resolves.toMatchObject<TokenAuthorizationResponse>({
        access_token: 'access_token',
        token_type: 'Bearer',
        expires_in: 3600,
        scope: 'foo bar',
        refresh_token: undefined,
        state: undefined,
      });
    });

    it('should create a token response with the default scope of the client and pass the state unmodified.', async () => {
      Reflect.set(parameters, 'state', 'client_state');

      const consent = <OAuth2Consent<AuthorizationRequest>>{ client, parameters, scopes: ['foo', 'bar'], user };

      await expect(responseType.handle(consent)).resolves.toMatchObject<TokenAuthorizationResponse>({
        access_token: 'access_token',
        token_type: 'Bearer',
        expires_in: 3600,
        scope: 'foo bar',
        refresh_token: undefined,
        state: 'client_state',
      });
    });
  });
});
