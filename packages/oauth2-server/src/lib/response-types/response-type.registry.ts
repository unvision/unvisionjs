import { Constructor } from '@unvision/di';

import { CodeResponseType } from './code.response-type';
import { ResponseType } from './response-type';
import { TokenResponseType } from './token.response-type';

export const responseTypeRegistry: Record<string, Constructor<ResponseType>> = {
  code: CodeResponseType,
  token: TokenResponseType,
};
