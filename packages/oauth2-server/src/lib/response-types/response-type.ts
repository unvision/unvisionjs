import { OAuth2Consent } from '../entities/oauth2-consent.entity';
import { AuthorizationRequest } from '../types/authorization-request';
import { AuthorizationResponse } from '../types/authorization-response';

/**
 * Interface of a Response Type.
 *
 * @see https://www.rfc-editor.org/rfc/rfc6749.html#section-4
 */
export interface ResponseType {
  /**
   * Name of the Response Type.
   */
  readonly name: string;

  /**
   * Default Response Mode of the Response Type.
   */
  readonly defaultResponseMode: string;

  /**
   * Creates the Authorization Response with the Authorization Grant used by the Client on behalf of the End User.
   *
   * @param consent Consent with the scopes granted by the End User.
   * @returns Authorization Response.
   */
  handle(consent: OAuth2Consent<AuthorizationRequest>): Promise<AuthorizationResponse>;
}
