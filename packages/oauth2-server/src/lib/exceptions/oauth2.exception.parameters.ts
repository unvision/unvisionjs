/**
 * Parameters of the OAuth 2.0 Exception.
 */
export interface OAuth2ExceptionParameters extends Record<string, any> {
  /**
   * Description of the OAuth 2.0 Exception.
   */
  error_description?: string;

  /**
   * URI of the page containing the details of the OAuth 2.0 Exception.
   */
  error_uri?: string;

  /**
   * State of the Client Application prior to the OAuth 2.0 Request.
   */
  state?: string;
}
