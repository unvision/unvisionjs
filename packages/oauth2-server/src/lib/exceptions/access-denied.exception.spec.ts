import { AccessDeniedException } from './access-denied.exception';
import { ErrorCode } from './error-code.enum';

test('should instantiate a new access denied exception.', () => {
  const exception = new AccessDeniedException({});

  expect(exception.code).toBe(ErrorCode.AccessDenied);
  expect(exception.statusCode).toBe(400);
});
