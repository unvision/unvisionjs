import { ErrorCode } from './error-code.enum';
import { InvalidClientException } from './invalid-client.exception';

test('should instantiate a new invalid client exception.', () => {
  const exception = new InvalidClientException({});

  expect(exception.code).toBe(ErrorCode.InvalidClient);
  expect(exception.statusCode).toBe(401);
});
