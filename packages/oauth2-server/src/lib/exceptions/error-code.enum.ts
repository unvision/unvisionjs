/**
 * OAuth 2.0 Error Codes.
 */
export enum ErrorCode {
  /**
   * The Resource Owner or Authorization Server did not authorize the Client.
   */
  AccessDenied = 'access_denied',

  /**
   * Client Authentication failed.
   */
  InvalidClient = 'invalid_client',

  /**
   * The provided Authorization Grant is invalid.
   */
  InvalidGrant = 'invalid_grant',

  /**
   * The OAuth 2.0 Request is invalid.
   */
  InvalidRequest = 'invalid_request',

  /**
   * The requested Scope is invalid.
   */
  InvalidScope = 'invalid_scope',

  /**
   * The Authorization Server encountered an unexpected error.
   */
  ServerError = 'server_error',

  /**
   * The Authorization Server is temporarily unavailable.
   */
  TemporarilyUnavailable = 'temporarily_unavailable',

  /**
   * The Client is not authorized to use the requested Grant.
   */
  UnauthorizedClient = 'unauthorized_client',

  /**
   * The requested **grant_type** is not supported by the Authorization Server.
   */
  UnsupportedGrantType = 'unsupported_grant_type',

  /**
   * The requested **interaction_type** is not supported by the Authorization Server.
   */
  UnsupportedInteractionType = 'unsupported_interaction_type',

  /**
   * The requested **response_type** is not supported by the Authorization Server.
   */
  UnsupportedResponseType = 'unsupported_response_type',

  /**
   * The requested **token_type** is not supported by the Authorization Server.
   */
  UnsupportedTokenType = 'unsupported_token_type',
}
