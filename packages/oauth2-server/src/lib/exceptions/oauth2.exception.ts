import { Exception } from '@unvision/primitives';

import { OutgoingHttpHeader, OutgoingHttpHeaders } from 'http';

import { OAuth2ExceptionParameters } from './oauth2.exception.parameters';

/**
 * Exception Class for the errors that may happen during the authorization process.
 */
export abstract class OAuth2Exception extends Exception {
  /**
   * OAuth 2.0 Error Code.
   */
  public abstract readonly code: string;

  /**
   * HTTP Response Status Code of the OAuth 2.0 Exception.
   */
  public readonly statusCode: number = 400;

  /**
   * HTTP Response Headers of the OAuth 2.0 Exception.
   */
  public readonly headers: OutgoingHttpHeaders = {};

  /**
   * Parameters of the OAuth 2.0 Exception.
   */
  private readonly body: OAuth2ExceptionParameters;

  /**
   * Instantiates a new OAuth 2.0 Exception.
   *
   * @param parameters Parameters of the OAuth 2.0 Exception.
   */
  public constructor(parameters: OAuth2ExceptionParameters, originalError?: Error) {
    super(parameters.error_description ?? null, <Error>originalError);

    this.body = parameters;
  }

  /**
   * Sets a HTTP Response Header of the OAuth 2.0 Exception.
   *
   * @param header Name of the HTTP Response Header.
   * @param value Value of the HTTP Response Header.
   */
  public setHeader(header: string, value: OutgoingHttpHeader): OAuth2Exception {
    this.headers[header] = value;
    return this;
  }

  /**
   * Sets multiple HTTP Response Headers of the OAuth 2.0 Exception.
   *
   * @param headers HTTP Response Headers.
   */
  public setHeaders(headers: OutgoingHttpHeaders): OAuth2Exception {
    Object.assign(this.headers, headers);
    return this;
  }

  /**
   * Parameters of the OAuth 2.0 Exception.
   */
  public toJSON(): Record<string, any> {
    return { error: this.code, ...this.body };
  }
}
