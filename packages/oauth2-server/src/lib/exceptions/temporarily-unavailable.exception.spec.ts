import { ErrorCode } from './error-code.enum';
import { TemporarilyUnavailableException } from './temporarily-unavailable.exception';

test('should instantiate a new temporarily unavailable exception.', () => {
  const exception = new TemporarilyUnavailableException({});

  expect(exception.code).toBe(ErrorCode.TemporarilyUnavailable);
  expect(exception.statusCode).toBe(503);
});
