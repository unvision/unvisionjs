import { DependencyInjectionContainer } from '@unvision/di';

import { S256PkceMethod } from './S256.pkce-method';

describe('S256 PKCE Method', () => {
  let pkceMethod: S256PkceMethod;

  beforeEach(() => {
    const container = new DependencyInjectionContainer();

    container.bind(S256PkceMethod).toSelf().asSingleton();

    pkceMethod = container.resolve(S256PkceMethod);
  });

  it('should have "S256" as its name.', () => {
    expect(pkceMethod.name).toBe('S256');
  });

  it('should return false when comparing a challenge to a different verifier.', () => {
    expect(pkceMethod.verify('8xJ5XjIsh0YabzxJ4JiXxZyg1aNiRdKgDwjLxm7ul20', 'abc123')).toBe(false);
  });

  it('should return true when comparing a challenge to its verifier.', () => {
    expect(pkceMethod.verify('8xJ5XjIsh0YabzxJ4JiXxZyg1aNiRdKgDwjLxm7ul20', 'abcxyz')).toBe(true);
  });
});
