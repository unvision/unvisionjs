import { DependencyInjectionContainer } from '@unvision/di';

import { PlainPkceMethod } from './plain.pkce-method';

describe('Plain PKCE Method', () => {
  let pkceMethod: PlainPkceMethod;

  beforeEach(() => {
    const container = new DependencyInjectionContainer();

    container.bind(PlainPkceMethod).toSelf().asSingleton();

    pkceMethod = container.resolve(PlainPkceMethod);
  });

  it('should have "plain" as its name.', () => {
    expect(pkceMethod.name).toBe('plain');
  });

  it('should return false when comparing two different strings.', () => {
    expect(pkceMethod.verify('abcxyz', 'abc123')).toBe(false);
  });

  it('should return true when comparing the same strings.', () => {
    expect(pkceMethod.verify('abcxyz', 'abcxyz')).toBe(true);
  });
});
