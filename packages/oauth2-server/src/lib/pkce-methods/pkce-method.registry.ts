import { Constructor } from '@unvision/di';

import { PkceMethod } from './pkce-method';
import { S256PkceMethod } from './S256.pkce-method';
import { PlainPkceMethod } from './plain.pkce-method';

export const pkceMethodRegistry: Record<string, Constructor<PkceMethod>> = {
  S256: S256PkceMethod,
  plain: PlainPkceMethod,
};
