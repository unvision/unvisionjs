import { OAuth2Consent } from '../entities/oauth2-consent.entity';
import { OAuth2Session } from '../entities/oauth2-session.entity';
import { AuthorizationRequest } from '../types/authorization-request';

/**
 * Representation of the Consent Service.
 *
 * The Consent Service contains the operations regarding the Custom OAuth 2.0 Consent.
 */
export interface OAuth2ConsentService {
  /**
   * Creates a Consent representing the consent given to the Client by the End-User.
   *
   * @param parameters Parameters of the Authorization Request.
   * @param session Session containing the Authentication of the End User.
   * @returns Generated Consent.
   */
  create(parameters: AuthorizationRequest, session: OAuth2Session): Promise<OAuth2Consent<AuthorizationRequest>>;

  /**
   * Searches the application's storage for a Consent containing the provided Identifier.
   *
   * @param id Identifier of the Consent.
   * @returns Consent based on the provided Identifier.
   */
  findOne(id: string): Promise<OAuth2Consent<AuthorizationRequest> | null>;

  /**
   * Persists the provided Consent into the application's storage.
   *
   * @param consent Consent to be persisted.
   */
  save(consent: OAuth2Consent<AuthorizationRequest>): Promise<void>;

  /**
   * Removes the provided Consent.
   *
   * @param consent Consent to be removed.
   */
  remove(consent: OAuth2Consent<AuthorizationRequest>): Promise<void>;
}
