import { OAuth2Client } from '../entities/oauth2-client.entity';
import { OAuth2Session } from '../entities/oauth2-session.entity';
import { AuthorizationRequest } from '../types/authorization-request';

/**
 * Representation of the Session Service.
 *
 * The Session Service contains the operations regarding the Custom OAuth 2.0 Session.
 */
export interface OAuth2SessionService {
  /**
   * Creates a Session representing the consent given to the Client by the End-User.
   *
   * @param parameters Parameters of the Authorization Request.
   * @param client Client requesting authorization.
   * @returns Generated Session.
   */
  create(parameters: AuthorizationRequest, client: OAuth2Client): Promise<OAuth2Session>;

  /**
   * Searches the application's storage for a Session containing the provided Identifier.
   *
   * @param id Identifier of the Session.
   * @returns Session based on the provided Identifier.
   */
  findOne(id: string): Promise<OAuth2Session | null>;

  /**
   * Persists the provided Session into the application's storage.
   *
   * @param session Session to be persisted.
   */
  save(session: OAuth2Session): Promise<void>;

  /**
   * Removes the provided Session.
   *
   * @param session Session to be removed.
   */
  remove(session: OAuth2Session): Promise<void>;
}
