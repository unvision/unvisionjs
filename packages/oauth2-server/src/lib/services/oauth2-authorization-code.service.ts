import { OAuth2AuthorizationCode } from '../entities/oauth2-authorization-code.entity';
import { OAuth2Client } from '../entities/oauth2-client.entity';
import { OAuth2User } from '../entities/oauth2-user.entity';
import { CodeAuthorizationRequest } from '../types/code.authorization-request';

/**
 * Representation of the Authorization Code Service.
 *
 * The Authorization Code Service contains the operations regarding the OAuth 2.0 Authorization Code.
 */
export interface OAuth2AuthorizationCodeService {
  /**
   * Creates an Authorization Code to be exchanged by the Client at the Token Endpoint for an Access Token.
   *
   * @param parameters Parameters of the **Code** Response Type.
   * @param client Client requesting authorization.
   * @param user End User that granted authorization.
   * @returns Issued Authorization Code.
   */
  create(
    parameters: CodeAuthorizationRequest,
    client: OAuth2Client,
    user: OAuth2User
  ): Promise<OAuth2AuthorizationCode>;

  /**
   * Searches the application's storage for an Authorization Code containing the provided Code.
   *
   * @param code Code of the Authorization Code.
   * @returns Authorization Code based on the provided Code.
   */
  findOne(code: string): Promise<OAuth2AuthorizationCode | null>;

  /**
   * Revokes the provided Authorization Code.
   *
   * @param authorizationCode Authorization Code to be revoked.
   */
  revoke(authorizationCode: OAuth2AuthorizationCode): Promise<void>;
}
