import { Injectable } from '@unvision/di';

import { randomUUID } from 'crypto';

import { OAuth2Consent } from '../../entities/oauth2-consent.entity';
import { OAuth2Session } from '../../entities/oauth2-session.entity';
import { AuthorizationRequest } from '../../types/authorization-request';
import { OAuth2ConsentService } from '../oauth2-consent.service';

@Injectable()
export class DefaultConsentService implements OAuth2ConsentService {
  protected readonly consents: OAuth2Consent<AuthorizationRequest>[] = [];

  public constructor() {
    console.warn('Using default Consent Service. This is only recommended for development.');
  }

  public async create(
    parameters: AuthorizationRequest,
    session: OAuth2Session
  ): Promise<OAuth2Consent<AuthorizationRequest>> {
    const consent: OAuth2Consent<AuthorizationRequest> = {
      id: randomUUID(),
      scopes: parameters.scope.split(' '),
      parameters,
      createdAt: new Date(),
      client: session.client,
      user: session.user!,
      session,
    };

    this.consents.push(consent);

    return consent;
  }

  public async findOne(id: string): Promise<OAuth2Consent<AuthorizationRequest> | null> {
    return this.consents.find((consent) => consent.id === id) ?? null;
  }

  public async save(consent: OAuth2Consent<AuthorizationRequest>): Promise<void> {
    const index = this.consents.findIndex((savedConsent) => savedConsent.id === consent.id);

    if (index > -1) {
      this.consents[index] = consent;
    }
  }

  public async remove(consent: OAuth2Consent<AuthorizationRequest>): Promise<void> {
    const index = this.consents.findIndex((savedConsent) => savedConsent.id === consent.id);

    if (index > -1) {
      this.consents.splice(index, 1);
    }
  }
}
