import { Injectable } from '@unvision/di';

import { OAuth2User } from '../../entities/oauth2-user.entity';
import { OAuth2UserService } from '../oauth2-user.service';

@Injectable()
export class DefaultUserService implements OAuth2UserService {
  protected readonly users: OAuth2User[] = [
    {
      id: '16907c32-687b-493c-85ba-f41f2c9d4daa',
      username: 'johndoe',
      password: 'secretpassword',
    },
  ];

  public constructor() {
    console.warn('Using default User Service. This is only recommended for development.');
  }

  public async findOne(id: string): Promise<OAuth2User | null> {
    return this.users.find((user) => user.id === id) ?? null;
  }

  public async findByResourceOwnerCredentials(username: string, password: string): Promise<OAuth2User | null> {
    return this.users.find((user) => user.username === username && user.password === password) ?? null;
  }
}
