import { Injectable } from '@unvision/di';

import { randomUUID } from 'crypto';

import { OAuth2Client } from '../../entities/oauth2-client.entity';
import { OAuth2Session } from '../../entities/oauth2-session.entity';
import { AuthorizationRequest } from '../../types/authorization-request';
import { OAuth2SessionService } from '../oauth2-session.service';

@Injectable()
export class DefaultSessionService implements OAuth2SessionService {
  protected readonly sessions: OAuth2Session[] = [];

  public constructor() {
    console.warn('Using default Session Service. This is only recommended for development.');
  }

  public async create(parameters: AuthorizationRequest, client: OAuth2Client): Promise<OAuth2Session> {
    const session: OAuth2Session = {
      id: randomUUID(),
      parameters,
      createdAt: new Date(),
      client,
      user: null,
    };

    this.sessions.push(session);

    return session;
  }

  public async findOne(id: string): Promise<OAuth2Session | null> {
    return this.sessions.find((session) => session.id === id) ?? null;
  }

  public async save(session: OAuth2Session): Promise<void> {
    const index = this.sessions.findIndex((savedSession) => savedSession.id === session.id);

    if (index > -1) {
      this.sessions[index] = session;
    }
  }

  public async remove(session: OAuth2Session): Promise<void> {
    const index = this.sessions.findIndex((savedSession) => savedSession.id === session.id);

    if (index > -1) {
      this.sessions.splice(index, 1);
    }
  }
}
