import { Injectable } from '@unvision/di';

import { randomBytes } from 'crypto';
import { promisify } from 'util';

import { OAuth2RefreshToken } from '../../entities/oauth2-refresh-token.entity';
import { OAuth2Client } from '../../entities/oauth2-client.entity';
import { OAuth2User } from '../../entities/oauth2-user.entity';
import { OAuth2RefreshTokenService } from '../oauth2-refresh-token.service';

const randomBytesAsync = promisify(randomBytes);

@Injectable()
export class DefaultRefreshTokenService implements OAuth2RefreshTokenService {
  protected readonly refreshTokens: OAuth2RefreshToken[] = [];

  public constructor() {
    console.warn('Using default Refresh Token Service. This is only recommended for development.');
  }

  public async create(scopes: string[], client: OAuth2Client, user: OAuth2User): Promise<OAuth2RefreshToken> {
    const now = Date.now();

    const refreshToken: OAuth2RefreshToken = {
      token: (await randomBytesAsync(12)).toString('hex'),
      scopes,
      isRevoked: false,
      issuedAt: new Date(now),
      expiresAt: new Date(now + 86400000),
      validAfter: new Date(now),
      client,
      user,
    };

    this.refreshTokens.push(refreshToken);

    return refreshToken;
  }

  public async findOne(token: string): Promise<OAuth2RefreshToken | null> {
    return this.refreshTokens.find((refreshToken) => refreshToken.token === token) ?? null;
  }

  public async revoke(refreshToken: OAuth2RefreshToken): Promise<void> {
    refreshToken.isRevoked = true;
  }
}
