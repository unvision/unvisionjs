import { Injectable } from '@unvision/di';

import { randomBytes } from 'crypto';
import { promisify } from 'util';

import { OAuth2AccessToken } from '../../entities/oauth2-access-token.entity';
import { OAuth2Client } from '../../entities/oauth2-client.entity';
import { OAuth2User } from '../../entities/oauth2-user.entity';
import { OAuth2AccessTokenService } from '../oauth2-access-token.service';

const randomBytesAsync = promisify(randomBytes);

@Injectable()
export class DefaultAccessTokenService implements OAuth2AccessTokenService {
  protected readonly accessTokens: OAuth2AccessToken[] = [];

  public constructor() {
    console.warn('Using default Access Token Service. This is only recommended for development.');
  }

  public async create(scopes: string[], client: OAuth2Client, user?: OAuth2User): Promise<OAuth2AccessToken> {
    const now = Date.now();

    const accessToken: OAuth2AccessToken = {
      token: (await randomBytesAsync(16)).toString('hex'),
      scopes,
      isRevoked: false,
      issuedAt: new Date(now),
      expiresAt: new Date(now + 3600000),
      validAfter: new Date(now),
      client,
      user,
    };

    this.accessTokens.push(accessToken);

    return accessToken;
  }

  public async findOne(token: string): Promise<OAuth2AccessToken | null> {
    return this.accessTokens.find((accessToken) => accessToken.token === token) ?? null;
  }

  public async revoke(accessToken: OAuth2AccessToken): Promise<void> {
    accessToken.isRevoked = true;
  }
}
