import { Injectable } from '@unvision/di';

import { randomUUID } from 'crypto';

import { OAuth2AuthorizationCode } from '../../entities/oauth2-authorization-code.entity';
import { OAuth2Client } from '../../entities/oauth2-client.entity';
import { OAuth2User } from '../../entities/oauth2-user.entity';
import { CodeAuthorizationRequest } from '../../types/code.authorization-request';
import { OAuth2AuthorizationCodeService } from '../oauth2-authorization-code.service';

@Injectable()
export class DefaultAuthorizationCodeService implements OAuth2AuthorizationCodeService {
  protected readonly authorizationCodes: OAuth2AuthorizationCode[] = [];

  public constructor() {
    console.warn('Using default Authorization Code Service. This is only recommended for development.');
  }

  public async create(
    parameters: CodeAuthorizationRequest,
    client: OAuth2Client,
    user: OAuth2User
  ): Promise<OAuth2AuthorizationCode> {
    const now = Date.now();

    const authorizationCode: OAuth2AuthorizationCode = {
      code: randomUUID(),
      scopes: parameters.scope.split(' '),
      redirectUri: parameters.redirect_uri,
      codeChallenge: parameters.code_challenge,
      codeChallengeMethod: parameters.code_challenge_method ?? 'plain',
      isRevoked: false,
      issuedAt: new Date(now),
      expiresAt: new Date(now + 300000),
      validAfter: new Date(now),
      client,
      user,
    };

    this.authorizationCodes.push(authorizationCode);

    return authorizationCode;
  }

  public async findOne(code: string): Promise<OAuth2AuthorizationCode | null> {
    return this.authorizationCodes.find((authorizationCode) => authorizationCode.code === code) ?? null;
  }

  public async revoke(authorizationCode: OAuth2AuthorizationCode): Promise<void> {
    authorizationCode.isRevoked = true;
  }
}
