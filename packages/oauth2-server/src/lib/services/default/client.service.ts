import { Injectable } from '@unvision/di';

import { OAuth2Client } from '../../entities/oauth2-client.entity';
import { OAuth2ClientService } from '../oauth2-client.service';

@Injectable()
export class DefaultClientService implements OAuth2ClientService {
  protected readonly clients: OAuth2Client[] = [
    {
      id: 'b1eeace9-2b0c-468e-a444-733befc3b35d',
      secret: 'z9IyV0Pd6_-0XRJP5DN-UvFYeP56sbNX',
      name: 'Dev Client #1',
      redirectUris: ['http://localhost:3000/oauth/callback'],
      responseTypes: ['code'],
      grantTypes: ['authorization_code', 'refresh_token'],
      applicationType: 'web',
      authenticationMethod: 'client_secret_basic',
      scopes: ['foo', 'bar', 'baz', 'qux'],
      createdAt: new Date(),
    },
  ];

  public constructor() {
    console.warn('Using default Client Service. This is only recommended for development.');
  }

  public async findOne(id: string): Promise<OAuth2Client | null> {
    return this.clients.find((client) => client.id === id) ?? null;
  }
}
