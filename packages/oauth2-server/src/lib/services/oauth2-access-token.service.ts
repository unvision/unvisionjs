import { OAuth2AccessToken } from '../entities/oauth2-access-token.entity';
import { OAuth2Client } from '../entities/oauth2-client.entity';
import { OAuth2User } from '../entities/oauth2-user.entity';

/**
 * Representation of the Access Token Service.
 *
 * The Access Token Service contains the operations regarding the OAuth 2.0 Access Token.
 */
export interface OAuth2AccessTokenService {
  /**
   * Creates an Access Token for authorized use by the Client on behalf of the End-User.
   *
   * @param scopes Scopes granted to the Client.
   * @param client Client requesting authorization.
   * @param user End-User that granted authorization.
   * @returns Issued Access Token.
   */
  create(scopes: string[], client: OAuth2Client, user?: OAuth2User): Promise<OAuth2AccessToken>;

  /**
   * Searches the application's storage for a Access Token containing the provided Token.
   *
   * @param token Token of the Access Token.
   * @returns Access Token based on the provided Token.
   */
  findOne(token: string): Promise<OAuth2AccessToken | null>;

  /**
   * Revokes the provided Access Token.
   *
   * @param accessToken Access Token to be revoked.
   */
  revoke(accessToken: OAuth2AccessToken): Promise<void>;
}
