import { OAuth2AccessToken } from '../entities/oauth2-access-token.entity';
import { OAuth2Client } from '../entities/oauth2-client.entity';
import { OAuth2RefreshToken } from '../entities/oauth2-refresh-token.entity';
import { OAuth2User } from '../entities/oauth2-user.entity';

/**
 * Representation of the Refresh Token Service.
 *
 * The Refresh Token Service contains the operations regarding the OAuth 2.0 Refresh Token.
 */
export interface OAuth2RefreshTokenService {
  /**
   * Creates a Refresh Token for reissuing Access Tokens for authorized use by the Client on behalf of the End-User.
   *
   * @param scopes Scopes granted to the Client.
   * @param client Client requesting authorization.
   * @param user End-User that granted authorization.
   * @param accessToken Access Token issued to the Client.
   * @returns Issued Refresh Token.
   */
  create(
    scopes: string[],
    client: OAuth2Client,
    user: OAuth2User,
    accessToken: OAuth2AccessToken
  ): Promise<OAuth2RefreshToken>;

  /**
   * Searches the application's storage for a Refresh Token containing the provided Token.
   *
   * @param token Token of the Refresh Token.
   * @returns Refresh Token based on the provided Token.
   */
  findOne(token: string): Promise<OAuth2RefreshToken | null>;

  /**
   * Revokes the provided Refresh Token.
   *
   * @param refreshToken Refresh Token to be revoked.
   */
  revoke(refreshToken: OAuth2RefreshToken): Promise<void>;
}
