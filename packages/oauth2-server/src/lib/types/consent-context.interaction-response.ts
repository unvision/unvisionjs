import { OAuth2Client } from '../entities/oauth2-client.entity';

/**
 * Parameters of the custom OAuth 2.0 Consent Context Interaction Response.
 */
export interface ConsentContextInteractionResponse {
  /**
   * Indicates if the application can skip displaying the consent screen.
   */
  readonly skip: boolean;

  /**
   * Scope requested by the Client.
   */
  readonly requested_scope: string;

  /**
   * Identifier of the Subject of the Authentication.
   */
  readonly subject: string;

  /**
   * Request Url.
   */
  readonly request_url: string;

  /**
   * Login Challenge of the Grant.
   */
  readonly login_challenge: string;

  /**
   * Client requesting authorization.
   */
  readonly client: OAuth2Client;

  /**
   * Context for the Login Interaction.
   */
  readonly context: Record<string, any>;
}
