/**
 * Parameters of the custom OAuth 2.0 Interaction Request.
 */
export interface InteractionRequest extends Record<string, any> {
  /**
   * Interaction Type requested by the Client.
   */
  readonly interaction_type: string;
}
