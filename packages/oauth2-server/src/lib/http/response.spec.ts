import { Buffer } from 'buffer';
import { URL } from 'url';

import { Response } from './response';

describe('Response', () => {
  it('should create a standard http response.', () => {
    expect(new Response()).toMatchObject<Partial<Response>>({
      body: Buffer.alloc(0),
      cookies: {},
      headers: {},
      statusCode: 200,
    });
  });

  it('should set a custom status code.', () => {
    expect(new Response().setStatus(201)).toMatchObject<Partial<Response>>({
      body: Buffer.alloc(0),
      cookies: {},
      headers: {},
      statusCode: 201,
    });
  });

  it('should set a custom header.', () => {
    expect(new Response().setHeader('X-Foo', 'foo-value')).toMatchObject<Partial<Response>>({
      body: Buffer.alloc(0),
      cookies: {},
      headers: { 'X-Foo': 'foo-value' },
      statusCode: 200,
    });
  });

  it('should set multiple custom headers.', () => {
    expect(new Response().setHeaders({ 'X-Foo': 'foo-value', 'X-Bar': 'bar-value' })).toMatchObject<Partial<Response>>({
      body: Buffer.alloc(0),
      cookies: {},
      headers: { 'X-Foo': 'foo-value', 'X-Bar': 'bar-value' },
      statusCode: 200,
    });
  });

  it('should set a custom cookie.', () => {
    expect(new Response().setCookie('foo', 'foo-value')).toMatchObject<Partial<Response>>({
      body: Buffer.alloc(0),
      cookies: { foo: 'foo-value' },
      headers: {},
      statusCode: 200,
    });
  });

  it('should set multiple custom cookies.', () => {
    expect(new Response().setCookies({ foo: 'foo-value', bar: 'bar-value' })).toMatchObject<Partial<Response>>({
      body: Buffer.alloc(0),
      cookies: { foo: 'foo-value', bar: 'bar-value' },
      headers: {},
      statusCode: 200,
    });
  });

  it('should create a json response.', () => {
    expect(new Response().json({ foo: 'bar' })).toMatchObject<Partial<Response>>({
      body: Buffer.from(JSON.stringify({ foo: 'bar' }), 'utf8'),
      cookies: {},
      headers: { 'Content-Type': 'application/json' },
      statusCode: 200,
    });
  });

  it('should create a redirect response.', () => {
    expect(new Response().redirect('https://example.com')).toMatchObject<Partial<Response>>({
      body: Buffer.alloc(0),
      cookies: {},
      headers: { Location: 'https://example.com' },
      statusCode: 303,
    });

    expect(new Response().redirect(new URL('https://example.com'))).toMatchObject<Partial<Response>>({
      body: Buffer.alloc(0),
      cookies: {},
      headers: { Location: expect.stringContaining('https://example.com') },
      statusCode: 303,
    });
  });

  it('should create an html response.', () => {
    expect(new Response().html('<html></html>')).toMatchObject<Partial<Response>>({
      body: Buffer.from('<html></html>', 'utf8'),
      cookies: {},
      headers: {},
      statusCode: 200,
    });
  });
});
