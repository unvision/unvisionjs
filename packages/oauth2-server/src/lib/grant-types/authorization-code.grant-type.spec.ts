import { DependencyInjectionContainer } from '@unvision/di';

import { OAuth2AccessToken } from '../entities/oauth2-access-token.entity';
import { OAuth2AuthorizationCode } from '../entities/oauth2-authorization-code.entity';
import { OAuth2Client } from '../entities/oauth2-client.entity';
import { OAuth2RefreshToken } from '../entities/oauth2-refresh-token.entity';
import { InvalidGrantException } from '../exceptions/invalid-grant.exception';
import { InvalidRequestException } from '../exceptions/invalid-request.exception';
import {
  ACCESS_TOKEN_SERVICE,
  AUTHORIZATION_CODE_SERVICE,
  PKCE_METHOD,
  REFRESH_TOKEN_SERVICE,
} from '../metadata/metadata.keys';
import { PkceMethod } from '../pkce-methods/pkce-method';
import { OAuth2AccessTokenService } from '../services/oauth2-access-token.service';
import { OAuth2AuthorizationCodeService } from '../services/oauth2-authorization-code.service';
import { OAuth2RefreshTokenService } from '../services/oauth2-refresh-token.service';
import { AuthorizationCodeTokenRequest } from '../types/authorization-code.token-request';
import { TokenResponse } from '../types/token-response';
import { AuthorizationCodeGrantType } from './authorization-code.grant-type';

describe('Authorization Code Grant Type', () => {
  let grantType: AuthorizationCodeGrantType;

  const authorizationCodeServiceMock = jest.mocked<OAuth2AuthorizationCodeService>({
    create: jest.fn(),
    findOne: jest.fn(),
    revoke: jest.fn(),
  });

  const accessTokenServiceMock = jest.mocked<OAuth2AccessTokenService>({
    create: jest.fn(),
    findOne: jest.fn(),
    revoke: jest.fn(),
  });

  const refreshTokenServiceMock = jest.mocked<OAuth2RefreshTokenService>({
    create: jest.fn(),
    findOne: jest.fn(),
    revoke: jest.fn(),
  });

  const pkceMethodsMocks = [
    jest.mocked<PkceMethod>({ name: 'plain', verify: jest.fn() }),
    jest.mocked<PkceMethod>({ name: 'S256', verify: jest.fn() }),
  ];

  beforeEach(() => {
    const container = new DependencyInjectionContainer();

    container.bind<OAuth2AuthorizationCodeService>(AUTHORIZATION_CODE_SERVICE).toValue(authorizationCodeServiceMock);
    container.bind<OAuth2AccessTokenService>(ACCESS_TOKEN_SERVICE).toValue(accessTokenServiceMock);
    container.bind<OAuth2RefreshTokenService>(REFRESH_TOKEN_SERVICE).toValue(refreshTokenServiceMock);
    container.bind(AuthorizationCodeGrantType).toSelf().asSingleton();

    pkceMethodsMocks.forEach((pkceMethod) => container.bind<PkceMethod>(PKCE_METHOD).toValue(pkceMethod));

    grantType = container.resolve(AuthorizationCodeGrantType);
  });

  describe('name', () => {
    it('should have "authorization_code" as its name.', () => {
      expect(grantType.name).toBe('authorization_code');
    });
  });

  describe('constructor', () => {
    it('should reject not providing any pkce methods.', () => {
      expect(() => new AuthorizationCodeGrantType([], <any>{}, <any>{}, <any>{})).toThrow(TypeError);
    });
  });

  describe('handle()', () => {
    let parameters: AuthorizationCodeTokenRequest;

    beforeEach(() => {
      parameters = {
        grant_type: 'authorization_code',
        code: 'code',
        code_verifier: 'code_challenge',
        redirect_uri: 'https://example.com/callback',
      };

      pkceMethodsMocks[0]!.verify.mockRestore();
    });

    it('should throw when not providing a "code" parameter.', async () => {
      Reflect.deleteProperty(parameters, 'code');

      const client = <OAuth2Client>{};

      await expect(grantType.handle(parameters, client)).rejects.toThrow(
        new InvalidRequestException({ error_description: 'Invalid parameter "code".' })
      );

      expect(authorizationCodeServiceMock.revoke).not.toHaveBeenCalled();
    });

    it('should throw when not providing a "code_verifier" parameter.', async () => {
      Reflect.deleteProperty(parameters, 'code_verifier');

      const client = <OAuth2Client>{};

      await expect(grantType.handle(parameters, client)).rejects.toThrow(
        new InvalidRequestException({ error_description: 'Invalid parameter "code_verifier".' })
      );

      expect(authorizationCodeServiceMock.revoke).not.toHaveBeenCalled();
    });

    it('should throw when not providing a "redirect_uri" parameter.', async () => {
      Reflect.deleteProperty(parameters, 'redirect_uri');

      const client = <OAuth2Client>{};

      await expect(grantType.handle(parameters, client)).rejects.toThrow(
        new InvalidRequestException({ error_description: 'Invalid parameter "redirect_uri".' })
      );

      expect(authorizationCodeServiceMock.revoke).not.toHaveBeenCalled();
    });

    it('should throw when an authorization code is not found.', async () => {
      Reflect.set(parameters, 'code', 'unknown');

      authorizationCodeServiceMock.findOne.mockResolvedValueOnce(null);

      const client = <OAuth2Client>{};

      await expect(grantType.handle(parameters, client)).rejects.toThrow(
        new InvalidGrantException({ error_description: 'Invalid Authorization Code.' })
      );

      expect(authorizationCodeServiceMock.revoke).not.toHaveBeenCalled();
    });

    it('should throw on a mismatching client identifier.', async () => {
      authorizationCodeServiceMock.findOne.mockResolvedValueOnce(<OAuth2AuthorizationCode>{
        client: { id: 'another_client_id' },
      });

      const client = <OAuth2Client>{ id: 'client_id' };

      await expect(grantType.handle(parameters, client)).rejects.toThrow(
        new InvalidGrantException({ error_description: 'Mismatching Client Identifier.' })
      );

      expect(authorizationCodeServiceMock.revoke).toHaveBeenCalledTimes(1);
    });

    it('should throw on an authorization code not yet valid.', async () => {
      authorizationCodeServiceMock.findOne.mockResolvedValueOnce(<OAuth2AuthorizationCode>{
        validAfter: new Date(Date.now() + 3600000),
        client: { id: 'client_id' },
      });

      const client = <OAuth2Client>{ id: 'client_id' };

      await expect(grantType.handle(parameters, client)).rejects.toThrow(
        new InvalidGrantException({ error_description: 'Authorization Code not yet valid.' })
      );

      expect(authorizationCodeServiceMock.revoke).toHaveBeenCalledTimes(1);
    });

    it('should throw on an expired authorization code.', async () => {
      authorizationCodeServiceMock.findOne.mockResolvedValueOnce(<OAuth2AuthorizationCode>{
        expiresAt: new Date(Date.now() - 300000),
        client: { id: 'client_id' },
      });

      const client = <OAuth2Client>{ id: 'client_id' };

      await expect(grantType.handle(parameters, client)).rejects.toThrow(
        new InvalidGrantException({ error_description: 'Expired Authorization Code.' })
      );

      expect(authorizationCodeServiceMock.revoke).toHaveBeenCalledTimes(1);
    });

    it('should throw on a revoked authorization code.', async () => {
      authorizationCodeServiceMock.findOne.mockResolvedValueOnce(<OAuth2AuthorizationCode>{
        isRevoked: true,
        expiresAt: new Date(Date.now() + 3600000),
        client: { id: 'client_id' },
      });

      const client = <OAuth2Client>{ id: 'client_id' };

      await expect(grantType.handle(parameters, client)).rejects.toThrow(
        new InvalidGrantException({ error_description: 'Revoked Authorization Code.' })
      );

      expect(authorizationCodeServiceMock.revoke).toHaveBeenCalledTimes(1);
    });

    it('should throw on a mismatching redirect uri.', async () => {
      authorizationCodeServiceMock.findOne.mockResolvedValueOnce(<OAuth2AuthorizationCode>{
        redirectUri: 'https://bad.example.com/callback',
        isRevoked: false,
        expiresAt: new Date(Date.now() + 3600000),
        client: { id: 'client_id' },
      });

      const client = <OAuth2Client>{ id: 'client_id', redirectUris: ['https://example.com/callback'] };

      await expect(grantType.handle(parameters, client)).rejects.toThrow(
        new InvalidGrantException({ error_description: 'Mismatching Redirect URI.' })
      );

      expect(authorizationCodeServiceMock.revoke).toHaveBeenCalledTimes(1);
    });

    it('should throw when using an unsupported pkce method.', async () => {
      authorizationCodeServiceMock.findOne.mockResolvedValueOnce(<OAuth2AuthorizationCode>{
        redirectUri: 'https://example.com/callback',
        codeChallengeMethod: 'unknown',
        isRevoked: false,
        expiresAt: new Date(Date.now() + 3600000),
        client: { id: 'client_id' },
      });

      const client = <OAuth2Client>{ id: 'client_id', redirectUris: ['https://example.com/callback'] };

      await expect(grantType.handle(parameters, client)).rejects.toThrow(
        new InvalidRequestException({ error_description: 'Unsupported PKCE Method "unknown".' })
      );

      expect(authorizationCodeServiceMock.revoke).toHaveBeenCalledTimes(1);
    });

    it('should throw on a mismatching pkce code challenge.', async () => {
      authorizationCodeServiceMock.findOne.mockResolvedValueOnce(<OAuth2AuthorizationCode>{
        redirectUri: 'https://example.com/callback',
        codeChallenge: 'code_challenge',
        codeChallengeMethod: 'plain',
        isRevoked: false,
        expiresAt: new Date(Date.now() + 3600000),
        client: { id: 'client_id' },
      });

      pkceMethodsMocks[0]!.verify.mockReturnValueOnce(false);

      const client = <OAuth2Client>{ id: 'client_id', redirectUris: ['https://example.com/callback'] };

      await expect(grantType.handle(parameters, client)).rejects.toThrow(
        new InvalidGrantException({ error_description: 'Invalid PKCE Code Challenge.' })
      );

      expect(authorizationCodeServiceMock.revoke).toHaveBeenCalledTimes(1);
    });

    it('should create a token response without a refresh token if its service is not provided.', async () => {
      const now = Date.now();

      authorizationCodeServiceMock.findOne.mockResolvedValueOnce(<OAuth2AuthorizationCode>{
        redirectUri: 'https://example.com/callback',
        codeChallenge: 'code_challenge',
        codeChallengeMethod: 'plain',
        isRevoked: false,
        expiresAt: new Date(now + 3600000),
        client: { id: 'client_id' },
      });

      accessTokenServiceMock.create.mockResolvedValueOnce(<OAuth2AccessToken>{
        token: 'access_token',
        expiresAt: new Date(now + 86400000),
        scopes: ['foo', 'bar'],
      });

      pkceMethodsMocks[0]!.verify.mockReturnValueOnce(true);

      const client = <OAuth2Client>{
        id: 'client_id',
        grantTypes: ['authorization_code'],
        redirectUris: ['https://example.com/callback'],
      };

      Reflect.deleteProperty(grantType, 'refreshTokenService');

      await expect(grantType.handle(parameters, client)).resolves.toStrictEqual<TokenResponse>({
        access_token: 'access_token',
        token_type: 'Bearer',
        expires_in: 86400,
        scope: 'foo bar',
        refresh_token: undefined,
      });
    });

    it('should create a token response without a refresh token if the client does not use it.', async () => {
      const now = Date.now();

      authorizationCodeServiceMock.findOne.mockResolvedValueOnce(<OAuth2AuthorizationCode>{
        redirectUri: 'https://example.com/callback',
        codeChallenge: 'code_challenge',
        codeChallengeMethod: 'plain',
        isRevoked: false,
        expiresAt: new Date(now + 3600000),
        client: { id: 'client_id' },
      });

      accessTokenServiceMock.create.mockResolvedValueOnce(<OAuth2AccessToken>{
        token: 'access_token',
        expiresAt: new Date(now + 86400000),
        scopes: ['foo', 'bar'],
      });

      pkceMethodsMocks[0]!.verify.mockReturnValueOnce(true);

      const client = <OAuth2Client>{
        id: 'client_id',
        grantTypes: ['authorization_code'],
        redirectUris: ['https://example.com/callback'],
      };

      await expect(grantType.handle(parameters, client)).resolves.toStrictEqual<TokenResponse>({
        access_token: 'access_token',
        token_type: 'Bearer',
        expires_in: 86400,
        scope: 'foo bar',
        refresh_token: undefined,
      });
    });

    it('should create a token response with a refresh token.', async () => {
      const now = Date.now();

      authorizationCodeServiceMock.findOne.mockResolvedValueOnce(<OAuth2AuthorizationCode>{
        redirectUri: 'https://example.com/callback',
        codeChallenge: 'code_challenge',
        codeChallengeMethod: 'plain',
        isRevoked: false,
        expiresAt: new Date(now + 3600000),
        client: { id: 'client_id' },
      });

      accessTokenServiceMock.create.mockResolvedValueOnce(<OAuth2AccessToken>{
        token: 'access_token',
        expiresAt: new Date(now + 86400000),
        scopes: ['foo', 'bar'],
      });

      refreshTokenServiceMock.create.mockResolvedValueOnce(<OAuth2RefreshToken>{ token: 'refresh_token' });

      pkceMethodsMocks[0]!.verify.mockReturnValueOnce(true);

      const client = <OAuth2Client>{
        id: 'client_id',
        grantTypes: ['authorization_code', 'refresh_token'],
        redirectUris: ['https://example.com/callback'],
      };

      await expect(grantType.handle(parameters, client)).resolves.toStrictEqual<TokenResponse>({
        access_token: 'access_token',
        token_type: 'Bearer',
        expires_in: 86400,
        scope: 'foo bar',
        refresh_token: 'refresh_token',
      });
    });
  });
});
