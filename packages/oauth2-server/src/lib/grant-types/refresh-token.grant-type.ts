import { Inject, Injectable } from '@unvision/di';

import { AuthorizationServerOptions } from '../authorization-server/options/authorization-server.options';
import { OAuth2Client } from '../entities/oauth2-client.entity';
import { OAuth2RefreshToken } from '../entities/oauth2-refresh-token.entity';
import { InvalidGrantException } from '../exceptions/invalid-grant.exception';
import { InvalidRequestException } from '../exceptions/invalid-request.exception';
import { ScopeHandler } from '../handlers/scope.handler';
import { ACCESS_TOKEN_SERVICE, AUTHORIZATION_SERVER_OPTIONS, REFRESH_TOKEN_SERVICE } from '../metadata/metadata.keys';
import { OAuth2AccessTokenService } from '../services/oauth2-access-token.service';
import { OAuth2RefreshTokenService } from '../services/oauth2-refresh-token.service';
import { RefreshTokenTokenRequest } from '../types/refresh-token.token-request';
import { TokenResponse } from '../types/token-response';
import { createTokenResponse } from '../utils/create-token-response';
import { GrantType } from './grant-type';

/**
 * Implementation of the **Refresh Token** Grant Type.
 *
 * In this Grant Type the Client requests the Authorization Server for the issuance of a new Access Token
 * without the need to repeat the User Consent process.
 *
 * @see https://www.rfc-editor.org/rfc/rfc6749.html#section-6
 */
@Injectable()
export class RefreshTokenGrantType implements GrantType {
  /**
   * Name of the Grant Type.
   */
  public readonly name: string = 'refresh_token';

  /**
   * Instantiates a new Refresh Token Grant Type.
   *
   * @param scopeHandler Scope Handler of the Authorization Server.
   * @param authorizationServerOptions Configuration Parameters of the Authorization Server.
   * @param accessTokenService Instance of the Access Token Service.
   * @param refreshTokenService Instance of the Refresh Token Service.
   */
  public constructor(
    private readonly scopeHandler: ScopeHandler,
    @Inject(AUTHORIZATION_SERVER_OPTIONS) private readonly authorizationServerOptions: AuthorizationServerOptions,
    @Inject(ACCESS_TOKEN_SERVICE) private readonly accessTokenService: OAuth2AccessTokenService,
    @Inject(REFRESH_TOKEN_SERVICE) private readonly refreshTokenService: OAuth2RefreshTokenService
  ) {}

  /**
   * Creates the Access Token Response with the Access Token issued to the Client.
   *
   * In this flow the Client uses the Refresh Token received by the Authorization Server as an Authorization Grant
   * to request a new Access Token without the need to trigger a new User Consent process.
   *
   * If the Refresh Token presented is valid, the Authorization Server issues a new Access Token.
   *
   * If **Refresh Token Rotation** is enabled, it issues a new Refresh Token and revokes the provided Refresh Token.
   *
   * @param parameters Parameters of the Token Request.
   * @param client Client of the Request.
   * @returns Access Token Response.
   */
  public async handle(parameters: RefreshTokenTokenRequest, client: OAuth2Client): Promise<TokenResponse> {
    this.checkParameters(parameters);

    let refreshToken = await this.getRefreshToken(parameters.refresh_token);

    this.checkRefreshToken(refreshToken, client);

    const scopes = this.getScopes(refreshToken, parameters.scope);

    const { user } = refreshToken;

    const accessToken = await this.accessTokenService.create(scopes, client, user);

    if (this.authorizationServerOptions.enableRefreshTokenRotation) {
      await this.refreshTokenService.revoke(refreshToken);
      refreshToken = await this.refreshTokenService.create(refreshToken.scopes, client, user, accessToken);
    }

    return createTokenResponse(accessToken, refreshToken);
  }

  /**
   * Checks if the Parameters of the Token Request are valid.
   *
   * @param parameters Parameters of the Token Request.
   */
  private checkParameters(parameters: RefreshTokenTokenRequest): void {
    const { refresh_token: refreshToken, scope } = parameters;

    if (typeof refreshToken !== 'string') {
      throw new InvalidRequestException({ error_description: 'Invalid parameter "refresh_token".' });
    }

    this.scopeHandler.checkRequestedScope(scope);
  }

  /**
   * Fetches the requested Refresh Token from the application's storage.
   *
   * @param token Token provided by the Client.
   * @returns Refresh Token based on the provided token.
   */
  private async getRefreshToken(token: string): Promise<OAuth2RefreshToken> {
    const refreshToken = await this.refreshTokenService.findOne(token);

    if (refreshToken === null) {
      throw new InvalidGrantException({ error_description: 'Invalid Refresh Token.' });
    }

    return refreshToken;
  }

  /**
   * Checks the Refresh Token against the Client of the Token Request.
   *
   * @param refreshToken Refresh Token to be checked.
   * @param client Client of the Request.
   */
  private checkRefreshToken(refreshToken: OAuth2RefreshToken, client: OAuth2Client): void {
    if (refreshToken.client.id !== client.id) {
      throw new InvalidGrantException({ error_description: 'Mismatching Client Identifier.' });
    }

    if (new Date() < refreshToken.validAfter) {
      throw new InvalidGrantException({ error_description: 'Refresh Token not yet valid.' });
    }

    if (new Date() > refreshToken.expiresAt) {
      throw new InvalidGrantException({ error_description: 'Expired Refresh Token.' });
    }

    if (refreshToken.isRevoked) {
      throw new InvalidGrantException({ error_description: 'Revoked Refresh Token.' });
    }
  }

  /**
   * Returns the original scopes of the provided Refresh Token or a subset thereof, as requested by the Client.
   *
   * @param refreshToken Refresh Token provided by the Client.
   * @param scope Subset of scopes requested by the Client.
   * @returns Scopes of the new Access Token.
   */
  private getScopes(refreshToken: OAuth2RefreshToken, scope?: string): string[] {
    if (scope === undefined) {
      return refreshToken.scopes;
    }

    const requestedScopes = scope.split(' ');

    requestedScopes.forEach((requestedScope) => {
      if (!refreshToken.scopes.includes(requestedScope)) {
        throw new InvalidGrantException({
          error_description: `The scope "${requestedScope}" was not previously granted.`,
        });
      }
    });

    return requestedScopes;
  }
}
