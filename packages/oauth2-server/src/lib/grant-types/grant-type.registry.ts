import { Constructor } from '@unvision/di';

import { AuthorizationCodeGrantType } from './authorization-code.grant-type';
import { ClientCredentialsGrantType } from './client-credentials.grant-type';
import { GrantType } from './grant-type';
import { JwtBearerGrantType } from './jwt-bearer.grant-type';
import { PasswordGrantType } from './password.grant-type';
import { RefreshTokenGrantType } from './refresh-token.grant-type';

export const grantTypeRegistry: Record<string, Constructor<GrantType>> = {
  authorization_code: AuthorizationCodeGrantType,
  client_credentials: ClientCredentialsGrantType,
  password: PasswordGrantType,
  refresh_token: RefreshTokenGrantType,
  'urn:ietf:params:oauth:grant-type:jwt-bearer': JwtBearerGrantType,
};
