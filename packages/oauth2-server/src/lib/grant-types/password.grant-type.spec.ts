import { DependencyInjectionContainer } from '@unvision/di';

import { AuthorizationServerOptions } from '../authorization-server/options/authorization-server.options';
import { OAuth2AccessToken } from '../entities/oauth2-access-token.entity';
import { OAuth2Client } from '../entities/oauth2-client.entity';
import { OAuth2RefreshToken } from '../entities/oauth2-refresh-token.entity';
import { InvalidGrantException } from '../exceptions/invalid-grant.exception';
import { InvalidRequestException } from '../exceptions/invalid-request.exception';
import { InvalidScopeException } from '../exceptions/invalid-scope.exception';
import { ScopeHandler } from '../handlers/scope.handler';
import {
  ACCESS_TOKEN_SERVICE,
  AUTHORIZATION_SERVER_OPTIONS,
  REFRESH_TOKEN_SERVICE,
  USER_SERVICE,
} from '../metadata/metadata.keys';
import { OAuth2AccessTokenService } from '../services/oauth2-access-token.service';
import { OAuth2RefreshTokenService } from '../services/oauth2-refresh-token.service';
import { OAuth2UserService } from '../services/oauth2-user.service';
import { PasswordTokenRequest } from '../types/password.token-request';
import { TokenResponse } from '../types/token-response';
import { PasswordGrantType } from './password.grant-type';

describe('Password Grant Type', () => {
  let grantType: PasswordGrantType;

  const accessTokenServiceMock = jest.mocked<OAuth2AccessTokenService>({
    create: jest.fn(async (scopes) => {
      return <OAuth2AccessToken>{ token: 'access_token', scopes, expiresAt: new Date(Date.now() + 86400000) };
    }),
    findOne: jest.fn(),
    revoke: jest.fn(),
  });

  const refreshTokenServiceMock = jest.mocked<OAuth2RefreshTokenService>({
    create: jest.fn(async () => <OAuth2RefreshToken>{ token: 'refresh_token' }),
    findOne: jest.fn(),
    revoke: jest.fn(),
  });

  const userServiceMock = jest.mocked<OAuth2UserService>(
    {
      findOne: jest.fn(),
      findByResourceOwnerCredentials: jest.fn(),
    },
    true
  );

  const authorizationServerOptions = <AuthorizationServerOptions>{ scopes: ['foo', 'bar', 'baz', 'qux'] };

  beforeEach(() => {
    const container = new DependencyInjectionContainer();

    container.bind<AuthorizationServerOptions>(AUTHORIZATION_SERVER_OPTIONS).toValue(authorizationServerOptions);
    container.bind<OAuth2AccessTokenService>(ACCESS_TOKEN_SERVICE).toValue(accessTokenServiceMock);
    container.bind<OAuth2RefreshTokenService>(REFRESH_TOKEN_SERVICE).toValue(refreshTokenServiceMock);
    container.bind<OAuth2UserService>(USER_SERVICE).toValue(userServiceMock);
    container.bind(ScopeHandler).toSelf().asSingleton();
    container.bind(PasswordGrantType).toSelf().asSingleton();

    grantType = container.resolve(PasswordGrantType);
  });

  describe('name', () => {
    it('should have "password" as its name.', () => {
      expect(grantType.name).toBe('password');
    });
  });

  describe('constructor', () => {
    it(`should reject not implementing user service's "findByResourceOwnerCredentials()".`, () => {
      expect(() => new PasswordGrantType(<any>{}, <any>{}, <any>{})).toThrow(TypeError);
    });
  });

  describe('handle()', () => {
    let parameters: PasswordTokenRequest;

    beforeEach(() => {
      parameters = { grant_type: 'password', username: 'username', password: 'password' };
    });

    it('should reject not providing a "username" parameter.', async () => {
      Reflect.deleteProperty(parameters, 'username');

      const client = <OAuth2Client>{
        id: 'client_id',
        grantTypes: ['password', 'refresh_token'],
        scopes: ['foo', 'bar'],
      };

      await expect(grantType.handle(parameters, client)).rejects.toThrow(
        new InvalidRequestException({ error_description: 'Invalid parameter "username".' })
      );
    });

    it('should reject not providing a "password" parameter.', async () => {
      Reflect.deleteProperty(parameters, 'password');

      const client = <OAuth2Client>{
        id: 'client_id',
        grantTypes: ['password', 'refresh_token'],
        scopes: ['foo', 'bar'],
      };

      await expect(grantType.handle(parameters, client)).rejects.toThrow(
        new InvalidRequestException({ error_description: 'Invalid parameter "password".' })
      );
    });

    it('should reject requesting an unsupported scope.', async () => {
      Reflect.set(parameters, 'scope', 'foo unknown bar');

      const client = <OAuth2Client>{
        id: 'client_id',
        grantTypes: ['password', 'refresh_token'],
        scopes: ['foo', 'bar'],
      };

      await expect(grantType.handle(parameters, client)).rejects.toThrow(
        new InvalidScopeException({ error_description: 'Unsupported scope "unknown".' })
      );
    });

    it('should reject when an end-user is not found.', async () => {
      const client = <OAuth2Client>{
        id: 'client_id',
        grantTypes: ['password', 'refresh_token'],
        scopes: ['foo', 'bar'],
      };

      userServiceMock.findByResourceOwnerCredentials!.mockResolvedValueOnce(null);

      await expect(grantType.handle(parameters, client)).rejects.toThrow(
        new InvalidGrantException({ error_description: 'Invalid Credentials.' })
      );
    });

    it("should create a token response with the client's default scope and without a refresh Token if its service is not provided.", async () => {
      Reflect.deleteProperty(grantType, 'refreshTokenService');

      const client = <OAuth2Client>{
        id: 'client_id',
        grantTypes: ['password', 'refresh_token'],
        scopes: ['foo', 'bar'],
      };

      userServiceMock.findByResourceOwnerCredentials!.mockResolvedValueOnce({ id: 'user_id' });

      await expect(grantType.handle(parameters, client)).resolves.toStrictEqual<TokenResponse>({
        access_token: 'access_token',
        token_type: 'Bearer',
        expires_in: 86400,
        scope: 'foo bar',
        refresh_token: undefined,
      });

      Reflect.set(grantType, 'refreshTokenService', refreshTokenServiceMock);
    });

    it('should create a token response with the requested scope and without a refresh token if its service is not provided.', async () => {
      Reflect.deleteProperty(grantType, 'refreshTokenService');
      Reflect.set(parameters, 'scope', 'bar foo');

      const client = <OAuth2Client>{
        id: 'client_id',
        grantTypes: ['password', 'refresh_token'],
        scopes: ['foo', 'bar'],
      };

      userServiceMock.findByResourceOwnerCredentials!.mockResolvedValueOnce({ id: 'user_id' });

      await expect(grantType.handle(parameters, client)).resolves.toStrictEqual<TokenResponse>({
        access_token: 'access_token',
        token_type: 'Bearer',
        expires_in: 86400,
        scope: 'bar foo',
        refresh_token: undefined,
      });

      Reflect.set(grantType, 'refreshTokenService', refreshTokenServiceMock);
    });

    it('should create a token response with a restricted scope and without a refresh token if its service is not provided.', async () => {
      Reflect.deleteProperty(grantType, 'refreshTokenService');
      Reflect.set(parameters, 'scope', 'bar qux foo');

      const client = <OAuth2Client>{
        id: 'client_id',
        grantTypes: ['password', 'refresh_token'],
        scopes: ['foo', 'bar'],
      };

      userServiceMock.findByResourceOwnerCredentials!.mockResolvedValueOnce({ id: 'user_id' });

      await expect(grantType.handle(parameters, client)).resolves.toStrictEqual<TokenResponse>({
        access_token: 'access_token',
        token_type: 'Bearer',
        expires_in: 86400,
        scope: 'bar foo',
        refresh_token: undefined,
      });

      Reflect.set(grantType, 'refreshTokenService', refreshTokenServiceMock);
    });

    it("should create a token response with the client's default scope and without a refresh token if the client does not use it.", async () => {
      const client = <OAuth2Client>{
        id: 'client_id',
        grantTypes: ['password'],
        scopes: ['bar', 'baz'],
      };

      userServiceMock.findByResourceOwnerCredentials!.mockResolvedValueOnce({ id: 'user_id' });

      await expect(grantType.handle(parameters, client)).resolves.toStrictEqual<TokenResponse>({
        access_token: 'access_token',
        token_type: 'Bearer',
        expires_in: 86400,
        scope: 'bar baz',
        refresh_token: undefined,
      });
    });

    it('should create a token response with the requested scope and without a refresh token if the client does not use it.', async () => {
      Reflect.set(parameters, 'scope', 'baz bar');

      const client = <OAuth2Client>{
        id: 'client_id',
        grantTypes: ['password'],
        scopes: ['bar', 'baz'],
      };

      userServiceMock.findByResourceOwnerCredentials!.mockResolvedValueOnce({ id: 'user_id' });

      await expect(grantType.handle(parameters, client)).resolves.toStrictEqual<TokenResponse>({
        access_token: 'access_token',
        token_type: 'Bearer',
        expires_in: 86400,
        scope: 'baz bar',
        refresh_token: undefined,
      });
    });

    it('should create a token response with a restricted scope and without a refresh token if the client does not use it.', async () => {
      Reflect.set(parameters, 'scope', 'baz bar qux');

      const client = <OAuth2Client>{
        id: 'client_id',
        grantTypes: ['password'],
        scopes: ['bar', 'baz'],
      };

      userServiceMock.findByResourceOwnerCredentials!.mockResolvedValueOnce({ id: 'user_id' });

      await expect(grantType.handle(parameters, client)).resolves.toStrictEqual<TokenResponse>({
        access_token: 'access_token',
        token_type: 'Bearer',
        expires_in: 86400,
        scope: 'baz bar',
        refresh_token: undefined,
      });
    });

    it("should create a token response with the client's default scope and with a refresh token.", async () => {
      const client = <OAuth2Client>{
        id: 'client_id',
        grantTypes: ['password', 'refresh_token'],
        scopes: ['foo', 'bar'],
      };

      userServiceMock.findByResourceOwnerCredentials!.mockResolvedValueOnce({ id: 'user_id' });

      await expect(grantType.handle(parameters, client)).resolves.toStrictEqual<TokenResponse>({
        access_token: 'access_token',
        token_type: 'Bearer',
        expires_in: 86400,
        scope: 'foo bar',
        refresh_token: 'refresh_token',
      });
    });

    it('should create a token response with the requested scope and with a refresh token.', async () => {
      Reflect.set(parameters, 'scope', 'bar foo');

      const client = <OAuth2Client>{
        id: 'client_id',
        grantTypes: ['password', 'refresh_token'],
        scopes: ['foo', 'bar'],
      };

      userServiceMock.findByResourceOwnerCredentials!.mockResolvedValueOnce({ id: 'user_id' });

      await expect(grantType.handle(parameters, client)).resolves.toStrictEqual<TokenResponse>({
        access_token: 'access_token',
        token_type: 'Bearer',
        expires_in: 86400,
        scope: 'bar foo',
        refresh_token: 'refresh_token',
      });
    });

    it('should create a token response with a restricted requested scope and with a refresh token.', async () => {
      Reflect.set(parameters, 'scope', 'baz bar foo');

      const client = <OAuth2Client>{
        id: 'client_id',
        grantTypes: ['password', 'refresh_token'],
        scopes: ['foo', 'bar'],
      };

      userServiceMock.findByResourceOwnerCredentials!.mockResolvedValueOnce({ id: 'user_id' });

      await expect(grantType.handle(parameters, client)).resolves.toStrictEqual<TokenResponse>({
        access_token: 'access_token',
        token_type: 'Bearer',
        expires_in: 86400,
        scope: 'bar foo',
        refresh_token: 'refresh_token',
      });
    });
  });
});
