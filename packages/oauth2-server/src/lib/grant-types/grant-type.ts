import { OAuth2Client } from '../entities/oauth2-client.entity';
import { TokenRequest } from '../types/token-request';
import { TokenResponse } from '../types/token-response';

/**
 * Interface of a Grant Type.
 *
 * @see https://www.rfc-editor.org/rfc/rfc6749.html#section-4
 */
export interface GrantType {
  /**
   * Name of the Grant Type.
   */
  readonly name: string;

  /**
   * Creates the Access Token Response with the Access Token issued to the Client.
   *
   * @param parameters Parameters of the Token Request.
   * @param client Client of the Request.
   * @returns Access Token Response.
   */
  handle(parameters: TokenRequest, client: OAuth2Client): Promise<TokenResponse>;
}
