import { DependencyInjectionContainer } from '@unvision/di';

import { AuthorizationServerOptions } from '../authorization-server/options/authorization-server.options';
import { OAuth2AccessToken } from '../entities/oauth2-access-token.entity';
import { OAuth2Client } from '../entities/oauth2-client.entity';
import { OAuth2RefreshToken } from '../entities/oauth2-refresh-token.entity';
import { InvalidGrantException } from '../exceptions/invalid-grant.exception';
import { InvalidRequestException } from '../exceptions/invalid-request.exception';
import { InvalidScopeException } from '../exceptions/invalid-scope.exception';
import { ScopeHandler } from '../handlers/scope.handler';
import { ACCESS_TOKEN_SERVICE, AUTHORIZATION_SERVER_OPTIONS, REFRESH_TOKEN_SERVICE } from '../metadata/metadata.keys';
import { OAuth2AccessTokenService } from '../services/oauth2-access-token.service';
import { OAuth2RefreshTokenService } from '../services/oauth2-refresh-token.service';
import { RefreshTokenTokenRequest } from '../types/refresh-token.token-request';
import { TokenResponse } from '../types/token-response';
import { RefreshTokenGrantType } from './refresh-token.grant-type';

describe('Refresh Token Grant Type', () => {
  let grantType: RefreshTokenGrantType;

  const accessTokenServiceMock = jest.mocked<OAuth2AccessTokenService>({
    create: jest.fn(async (scopes) => {
      return <OAuth2AccessToken>{ token: 'access_token', scopes, expiresAt: new Date(Date.now() + 86400000) };
    }),
    findOne: jest.fn(),
    revoke: jest.fn(),
  });

  const refreshTokenServiceMock = jest.mocked<OAuth2RefreshTokenService>({
    create: jest.fn(async () => <OAuth2RefreshToken>{ token: 'new_refresh_token' }),
    findOne: jest.fn(),
    revoke: jest.fn(),
  });

  const authorizationServerOptions = <AuthorizationServerOptions>{ scopes: ['foo', 'bar', 'baz', 'qux'] };

  beforeEach(() => {
    const container = new DependencyInjectionContainer();

    container.bind<AuthorizationServerOptions>(AUTHORIZATION_SERVER_OPTIONS).toValue(authorizationServerOptions);
    container.bind<OAuth2AccessTokenService>(ACCESS_TOKEN_SERVICE).toValue(accessTokenServiceMock);
    container.bind<OAuth2RefreshTokenService>(REFRESH_TOKEN_SERVICE).toValue(refreshTokenServiceMock);
    container.bind(ScopeHandler).toSelf().asSingleton();
    container.bind(RefreshTokenGrantType).toSelf().asSingleton();

    grantType = container.resolve(RefreshTokenGrantType);
  });

  describe('name', () => {
    it('should have "refresh_token" as its name.', () => {
      expect(grantType.name).toBe('refresh_token');
    });
  });

  describe('handle()', () => {
    let parameters: RefreshTokenTokenRequest;

    beforeEach(() => {
      parameters = { grant_type: 'refresh_token', refresh_token: 'refresh_token' };
    });

    it('should reject not providing a "refresh_token" parameter.', async () => {
      Reflect.deleteProperty(parameters, 'refresh_token');

      const client = <OAuth2Client>{ id: 'client_id' };

      await expect(grantType.handle(parameters, client)).rejects.toThrow(
        new InvalidRequestException({ error_description: 'Invalid parameter "refresh_token".' })
      );
    });

    it('should reject requesting an unsupported scope.', async () => {
      Reflect.set(parameters, 'scope', 'foo unknown bar');

      const client = <OAuth2Client>{ id: 'client_id' };

      await expect(grantType.handle(parameters, client)).rejects.toThrow(
        new InvalidScopeException({ error_description: 'Unsupported scope "unknown".' })
      );
    });

    it('should reject when a refresh token is not found.', async () => {
      const client = <OAuth2Client>{ id: 'client_id' };

      refreshTokenServiceMock.findOne.mockResolvedValueOnce(null);

      await expect(grantType.handle(parameters, client)).rejects.toThrow(
        new InvalidGrantException({ error_description: 'Invalid Refresh Token.' })
      );
    });

    it('should reject a mismathching client identifier.', async () => {
      const client = <OAuth2Client>{ id: 'client_id' };

      refreshTokenServiceMock.findOne.mockResolvedValueOnce(<OAuth2RefreshToken>{
        token: 'refresh_token',
        client: { id: 'another_client_id' },
      });

      await expect(grantType.handle(parameters, client)).rejects.toThrow(
        new InvalidGrantException({ error_description: 'Mismatching Client Identifier.' })
      );
    });

    it('should reject a refresh token not yet valid.', async () => {
      const client = <OAuth2Client>{ id: 'client_id' };

      refreshTokenServiceMock.findOne.mockResolvedValueOnce(<OAuth2RefreshToken>{
        token: 'refresh_token',
        validAfter: new Date(Date.now() + 3600000),
        client: { id: 'client_id' },
      });

      await expect(grantType.handle(parameters, client)).rejects.toThrow(
        new InvalidGrantException({ error_description: 'Refresh Token not yet valid.' })
      );
    });

    it('should reject an expired refresh token.', async () => {
      const client = <OAuth2Client>{ id: 'client_id' };

      refreshTokenServiceMock.findOne.mockResolvedValueOnce(<OAuth2RefreshToken>{
        token: 'refresh_token',
        expiresAt: new Date(Date.now() - 3600000),
        client: { id: 'client_id' },
      });

      await expect(grantType.handle(parameters, client)).rejects.toThrow(
        new InvalidGrantException({ error_description: 'Expired Refresh Token.' })
      );
    });

    it('should reject a revoked refresh token.', async () => {
      const client = <OAuth2Client>{ id: 'client_id' };

      refreshTokenServiceMock.findOne.mockResolvedValueOnce(<OAuth2RefreshToken>{
        token: 'refresh_token',
        isRevoked: true,
        expiresAt: new Date(Date.now() + 3600000),
        client: { id: 'client_id' },
      });

      await expect(grantType.handle(parameters, client)).rejects.toThrow(
        new InvalidGrantException({ error_description: 'Revoked Refresh Token.' })
      );
    });

    it('should reject requesting a scope not previously granted.', async () => {
      Reflect.set(parameters, 'scope', 'foo bar baz');

      const client = <OAuth2Client>{ id: 'client_id' };

      refreshTokenServiceMock.findOne.mockResolvedValueOnce(<OAuth2RefreshToken>{
        token: 'refresh_token',
        isRevoked: false,
        scopes: ['foo', 'bar'],
        expiresAt: new Date(Date.now() + 3600000),
        client: { id: 'client_id' },
      });

      await expect(grantType.handle(parameters, client)).rejects.toThrow(
        new InvalidGrantException({ error_description: 'The scope "baz" was not previously granted.' })
      );
    });

    it('should create a token response with the original scope and the same refresh token.', async () => {
      const client = <OAuth2Client>{ id: 'client_id' };

      refreshTokenServiceMock.findOne.mockResolvedValueOnce(<OAuth2RefreshToken>{
        token: 'refresh_token',
        isRevoked: false,
        scopes: ['foo', 'bar'],
        expiresAt: new Date(Date.now() + 3600000),
        client: { id: 'client_id' },
      });

      await expect(grantType.handle(parameters, client)).resolves.toStrictEqual<TokenResponse>({
        access_token: 'access_token',
        token_type: 'Bearer',
        expires_in: 86400,
        scope: 'foo bar',
        refresh_token: 'refresh_token',
      });
    });

    it('should create a token response with the requested scope and the same refresh token.', async () => {
      Reflect.set(parameters, 'scope', 'foo');

      const client = <OAuth2Client>{ id: 'client_id' };

      refreshTokenServiceMock.findOne.mockResolvedValueOnce(<OAuth2RefreshToken>{
        token: 'refresh_token',
        isRevoked: false,
        scopes: ['foo', 'bar'],
        expiresAt: new Date(Date.now() + 3600000),
        client: { id: 'client_id' },
      });

      await expect(grantType.handle(parameters, client)).resolves.toStrictEqual<TokenResponse>({
        access_token: 'access_token',
        token_type: 'Bearer',
        expires_in: 86400,
        scope: 'foo',
        refresh_token: 'refresh_token',
      });
    });

    it('should create a token response with the original scope and a new refresh token.', async () => {
      Reflect.set(authorizationServerOptions, 'enableRefreshTokenRotation', true);

      const client = <OAuth2Client>{ id: 'client_id' };

      refreshTokenServiceMock.findOne.mockResolvedValueOnce(<OAuth2RefreshToken>{
        token: 'refresh_token',
        isRevoked: false,
        scopes: ['foo', 'bar'],
        expiresAt: new Date(Date.now() + 3600000),
        client: { id: 'client_id' },
      });

      await expect(grantType.handle(parameters, client)).resolves.toStrictEqual<TokenResponse>({
        access_token: 'access_token',
        token_type: 'Bearer',
        expires_in: 86400,
        scope: 'foo bar',
        refresh_token: 'new_refresh_token',
      });

      expect(refreshTokenServiceMock.revoke).toHaveBeenCalledTimes(1);
      expect(refreshTokenServiceMock.create).toHaveBeenCalledTimes(1);

      const revokeOrder = refreshTokenServiceMock.revoke.mock.invocationCallOrder[0]!;
      const createOrder = refreshTokenServiceMock.create.mock.invocationCallOrder[0]!;

      expect(revokeOrder).toBeLessThan(createOrder);

      Reflect.deleteProperty(authorizationServerOptions, 'enableRefreshTokenRotation');
    });

    it('should create a token response with the requested scope and a new refresh token.', async () => {
      Reflect.set(authorizationServerOptions, 'enableRefreshTokenRotation', true);
      Reflect.set(parameters, 'scope', 'foo');

      const client = <OAuth2Client>{ id: 'client_id' };

      refreshTokenServiceMock.findOne.mockResolvedValueOnce(<OAuth2RefreshToken>{
        token: 'refresh_token',
        isRevoked: false,
        scopes: ['foo', 'bar'],
        expiresAt: new Date(Date.now() + 3600000),
        client: { id: 'client_id' },
      });

      await expect(grantType.handle(parameters, client)).resolves.toStrictEqual<TokenResponse>({
        access_token: 'access_token',
        token_type: 'Bearer',
        expires_in: 86400,
        scope: 'foo',
        refresh_token: 'new_refresh_token',
      });

      expect(refreshTokenServiceMock.revoke).toHaveBeenCalledTimes(1);
      expect(refreshTokenServiceMock.create).toHaveBeenCalledTimes(1);

      const revokeOrder = refreshTokenServiceMock.revoke.mock.invocationCallOrder[0]!;
      const createOrder = refreshTokenServiceMock.create.mock.invocationCallOrder[0]!;

      expect(revokeOrder).toBeLessThan(createOrder);

      Reflect.deleteProperty(authorizationServerOptions, 'enableRefreshTokenRotation');
    });
  });
});
