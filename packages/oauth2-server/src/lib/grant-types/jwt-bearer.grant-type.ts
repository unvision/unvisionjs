import { Inject, Injectable } from '@unvision/di';
import {
  InvalidJsonWebKeySetException,
  JsonWebKey,
  JsonWebKeySet,
  JsonWebSignature,
  JsonWebSignatureHeaderParameters,
  JsonWebTokenClaims,
  SupportedJsonWebSignatureAlgorithm,
} from '@unvision/jose';

import { Buffer } from 'buffer';
import https from 'https';

import { AuthorizationServerOptions } from '../authorization-server/options/authorization-server.options';
import { OAuth2Client } from '../entities/oauth2-client.entity';
import { OAuth2User } from '../entities/oauth2-user.entity';
import { InvalidGrantException } from '../exceptions/invalid-grant.exception';
import { InvalidRequestException } from '../exceptions/invalid-request.exception';
import { OAuth2Exception } from '../exceptions/oauth2.exception';
import { ScopeHandler } from '../handlers/scope.handler';
import { ACCESS_TOKEN_SERVICE, AUTHORIZATION_SERVER_OPTIONS, USER_SERVICE } from '../metadata/metadata.keys';
import { OAuth2AccessTokenService } from '../services/oauth2-access-token.service';
import { OAuth2UserService } from '../services/oauth2-user.service';
import { JwtBearerTokenRequest } from '../types/jwt-bearer.token-request';
import { TokenResponse } from '../types/token-response';
import { createTokenResponse } from '../utils/create-token-response';
import { GrantType } from './grant-type';

/**
 * Implementation of the **JWT Bearer** Grant Type.
 *
 * In this Grant Type the Client provides a JSON Web Token Assertion as its Authorization Grant
 * and exchanges it for an Access Token.
 *
 * @see https://www.rfc-editor.org/rfc/rfc7523.html
 */
@Injectable()
export class JwtBearerGrantType implements GrantType {
  /**
   * JSON Web Signature Algorithms.
   */
  private readonly algorithms: SupportedJsonWebSignatureAlgorithm[] = [
    'ES256',
    'ES384',
    'ES512',
    'HS256',
    'HS384',
    'HS512',
    'PS256',
    'PS384',
    'PS512',
    'RS256',
    'RS384',
    'RS512',
  ];

  /**
   * Name of the Grant Type.
   */
  public readonly name: string = 'urn:ietf:params:oauth:grant-type:jwt-bearer';

  /**
   * Instantiates a new JWT Bearer Grant Type.
   *
   * @param scopeHandler Scope Handler of the Authorization Server.
   * @param accessTokenService Instance of the Access Token Service.
   * @param userService Instance of the User Service.
   * @param authorizationServerOptions Configuration Parameters of the Authorization Server.
   */
  public constructor(
    private readonly scopeHandler: ScopeHandler,
    @Inject(ACCESS_TOKEN_SERVICE) private readonly accessTokenService: OAuth2AccessTokenService,
    @Inject(USER_SERVICE) private readonly userService: OAuth2UserService,
    @Inject(AUTHORIZATION_SERVER_OPTIONS) protected readonly authorizationServerOptions: AuthorizationServerOptions
  ) {}

  /**
   * Creates an Access Token Response with the Access Token issued to the Client.
   *
   * In this flow, the Client presents a JSON Web Token Assertion signed with one of its JSON Web Keys
   * as an Authorization Grant for the Token Endpoint. Once the signature of the JSON Web Token is verified,
   * the Authorization Server issues an Access Token to the Client.
   *
   * It does not issue a Refresh Token, since the Client can generate a new JSON Web Token Assertion
   * and present it to the Token Endpoint without the need to reauthenticate the End User.
   *
   * @param parameters Parameters of the Token Request.
   * @param client Client of the Request.
   * @returns Access Token Response.
   */
  public async handle(parameters: JwtBearerTokenRequest, client: OAuth2Client): Promise<TokenResponse> {
    this.checkParameters(parameters);

    const user = await this.getSubjectFromAssertion(parameters.assertion, client);
    const scopes = this.scopeHandler.getAllowedScopes(client, parameters.scope);
    const accessToken = await this.accessTokenService.create(scopes, client, user);

    return createTokenResponse(accessToken);
  }

  /**
   * Checks if the Parameters of the Token Request are valid.
   *
   * @param parameters Parameters of the Token Request.
   */
  private checkParameters(parameters: JwtBearerTokenRequest): void {
    const { assertion, scope } = parameters;

    if (typeof assertion !== 'string') {
      throw new InvalidRequestException({ error_description: 'Invalid parameter "assertion".' });
    }

    this.scopeHandler.checkRequestedScope(scope);
  }

  /**
   * Returns the End User represented by the Client in its Assertion.
   *
   * @param assertion JSON Web Token Assertion provided by the Client.
   * @param client Client requesting authorization.
   * @returns End User represented in the Assertion.
   */
  private async getSubjectFromAssertion(assertion: string, client: OAuth2Client): Promise<OAuth2User> {
    try {
      const [header, payload] = JsonWebSignature.decode(assertion);

      if (header.alg === 'none') {
        throw new InvalidGrantException({ error_description: 'Invalid JSON Web Signature Algorithm "none".' });
      }

      const claims = new JsonWebTokenClaims(JSON.parse(payload.toString('utf8')), {
        iss: { essential: true, value: client.id },
        sub: { essential: true },
        aud: { essential: true, value: new URL('/oauth/token', this.authorizationServerOptions.issuer).href },
        exp: { essential: true },
      });

      const key = await this.getClientKey(client, header);

      await JsonWebSignature.verify(assertion, key, this.algorithms);

      return await this.getUser(<string>claims.sub);
    } catch (exc: any) {
      throw exc instanceof OAuth2Exception
        ? exc
        : new InvalidGrantException({ error_description: 'The provided Assertion is invalid.' }, exc);
    }
  }

  /**
   * Returns the JSON Web Key of the Client used to validate the Assertion.
   *
   * @param client Client of the Request.
   * @param header JSON Web Signature Header of the Assertion.
   * @returns JSON Web Key of the Client based on the JSON Web Signature Header.
   */
  private async getClientKey(client: OAuth2Client, header: JsonWebSignatureHeaderParameters): Promise<JsonWebKey> {
    return (<SupportedJsonWebSignatureAlgorithm[]>['HS256', 'HS384', 'HS512']).includes(header.alg)
      ? await this.getClientSecretKey(client)
      : await this.getClientPublicKey(client, header);
  }

  /**
   * Returns the Client Secret as a JSON Web Key.
   *
   * @param client Client of the Request.
   * @returns JSON Web Key of the Client Secret.
   */
  private async getClientSecretKey(client: OAuth2Client): Promise<JsonWebKey> {
    if (client.secret == null || (client.secretExpiresAt != null && new Date() >= client.secretExpiresAt)) {
      throw new InvalidGrantException({ error_description: 'The provided Assertion is invalid.' });
    }

    return new JsonWebKey({ kty: 'oct', k: Buffer.from(client.secret, 'utf8').toString('base64url') });
  }

  /**
   * Returns the Public JSON Web Key of the Client used to validate the Assertion.
   *
   * @param client Client of the Request.
   * @param header JSON Web Signature Header of the Assertion.
   * @returns JSON Web Key of the Client based on the JSON Web Signature Header.
   */
  private async getClientPublicKey(
    client: OAuth2Client,
    header: JsonWebSignatureHeaderParameters
  ): Promise<JsonWebKey> {
    let clientJwks: JsonWebKeySet | null = null;

    if (client.jwksUri != null) {
      clientJwks = await this.getClientJwksFromUri(client.jwksUri);
    } else if (client.jwks != null) {
      clientJwks = JsonWebKeySet.load(client.jwks);
    }

    if (clientJwks === null) {
      throw new InvalidGrantException({ error_description: 'The provided Assertion is invalid.' });
    }

    const jwk = clientJwks.find((key) => {
      return (
        key.kid === header.kid &&
        (key.key_ops?.includes('verify') ?? true) &&
        (key.use !== undefined ? key.use === 'sig' : true)
      );
    });

    if (jwk === null) {
      throw new InvalidGrantException({ error_description: 'The provided Assertion is invalid.' });
    }

    return jwk;
  }

  /**
   * Fetches the JSON Web Key Set of the Client hosted at the provided URI.
   *
   * @param jwksUri URI of the JSON Web Key Set of the Client.
   * @returns JSON Web Key Set of the Client.
   */
  private getClientJwksFromUri(jwksUri: string): Promise<JsonWebKeySet> {
    return new Promise((resolve, reject) => {
      const request = https.request(jwksUri, (res) => {
        let responseBody = '';

        res.setEncoding('utf8');

        res.on('data', (chunk) => (responseBody += chunk));
        res.on('end', () => {
          try {
            resolve(JsonWebKeySet.parse(responseBody));
          } catch (exc: any) {
            reject(new InvalidJsonWebKeySetException(null, exc));
          }
        });
      });

      request.on('error', (error) => reject(new InvalidJsonWebKeySetException(null, error)));
      request.end();
    });
  }

  /**
   * Fetches an End User from the application's storage based on the provided Identifier.
   *
   * @param id Identifier of the End User.
   * @returns End User based on the provided Identifier.
   */
  private async getUser(id: string): Promise<OAuth2User> {
    const user = await this.userService.findOne(id);

    if (user === null) {
      throw new InvalidGrantException({ error_description: 'The provided Assertion is invalid.' });
    }

    return user;
  }
}
