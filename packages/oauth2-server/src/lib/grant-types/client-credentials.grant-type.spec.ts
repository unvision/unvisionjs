import { DependencyInjectionContainer } from '@unvision/di';

import { AuthorizationServerOptions } from '../authorization-server/options/authorization-server.options';
import { OAuth2AccessToken } from '../entities/oauth2-access-token.entity';
import { OAuth2Client } from '../entities/oauth2-client.entity';
import { InvalidScopeException } from '../exceptions/invalid-scope.exception';
import { ScopeHandler } from '../handlers/scope.handler';
import { ACCESS_TOKEN_SERVICE, AUTHORIZATION_SERVER_OPTIONS } from '../metadata/metadata.keys';
import { OAuth2AccessTokenService } from '../services/oauth2-access-token.service';
import { ClientCredentialsTokenRequest } from '../types/client-credentials.token-request';
import { TokenResponse } from '../types/token-response';
import { ClientCredentialsGrantType } from './client-credentials.grant-type';

describe('Client Credentials Grant Type', () => {
  let grantType: ClientCredentialsGrantType;

  const client = <OAuth2Client>{ scopes: ['foo', 'bar', 'baz'] };

  const accessTokenServiceMock = jest.mocked<OAuth2AccessTokenService>({
    create: jest.fn().mockImplementation(async (scopes) => {
      return <OAuth2AccessToken>{ token: 'access_token', scopes, expiresAt: new Date(Date.now() + 300000) };
    }),
    findOne: jest.fn(),
    revoke: jest.fn(),
  });

  const authorizationServerOptions = <AuthorizationServerOptions>{ scopes: ['foo', 'bar', 'baz', 'qux'] };

  beforeEach(() => {
    const container = new DependencyInjectionContainer();

    container.bind<AuthorizationServerOptions>(AUTHORIZATION_SERVER_OPTIONS).toValue(authorizationServerOptions);
    container.bind<OAuth2AccessTokenService>(ACCESS_TOKEN_SERVICE).toValue(accessTokenServiceMock);
    container.bind(ScopeHandler).toSelf().asSingleton();
    container.bind(ClientCredentialsGrantType).toSelf().asSingleton();

    grantType = container.resolve(ClientCredentialsGrantType);
  });

  describe('name', () => {
    it('should have "client_credentials" as its name.', () => {
      expect(grantType.name).toBe('client_credentials');
    });
  });

  describe('handle()', () => {
    let parameters: ClientCredentialsTokenRequest;

    beforeEach(() => {
      parameters = { grant_type: 'client_credentials' };
    });

    it('should reject requesting an unsupported scope.', async () => {
      Reflect.set(parameters, 'scope', 'foo unknown bar');

      await expect(grantType.handle(parameters, client)).rejects.toThrow(
        new InvalidScopeException({ error_description: 'Unsupported scope "unknown".' })
      );
    });

    it('should create a token response with a restricted scope.', async () => {
      Reflect.set(parameters, 'scope', 'foo qux baz');

      await expect(grantType.handle(parameters, client)).resolves.toMatchObject<TokenResponse>({
        access_token: 'access_token',
        token_type: 'Bearer',
        expires_in: 300,
        scope: 'foo baz',
      });
    });

    it('should create a token response with the requested scope.', async () => {
      Reflect.set(parameters, 'scope', 'baz foo');

      await expect(grantType.handle(parameters, client)).resolves.toMatchObject<TokenResponse>({
        access_token: 'access_token',
        token_type: 'Bearer',
        expires_in: 300,
        scope: 'baz foo',
      });
    });

    it('should create a token response with the default scope of the client.', async () => {
      await expect(grantType.handle(parameters, client)).resolves.toMatchObject<TokenResponse>({
        access_token: 'access_token',
        token_type: 'Bearer',
        expires_in: 300,
        scope: 'foo bar baz',
      });
    });
  });
});
