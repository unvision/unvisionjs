import { Inject, Injectable, Optional } from '@unvision/di';

import { OAuth2Client } from '../entities/oauth2-client.entity';
import { OAuth2User } from '../entities/oauth2-user.entity';
import { InvalidGrantException } from '../exceptions/invalid-grant.exception';
import { InvalidRequestException } from '../exceptions/invalid-request.exception';
import { ScopeHandler } from '../handlers/scope.handler';
import { ACCESS_TOKEN_SERVICE, REFRESH_TOKEN_SERVICE, USER_SERVICE } from '../metadata/metadata.keys';
import { OAuth2AccessTokenService } from '../services/oauth2-access-token.service';
import { OAuth2RefreshTokenService } from '../services/oauth2-refresh-token.service';
import { OAuth2UserService } from '../services/oauth2-user.service';
import { PasswordTokenRequest } from '../types/password.token-request';
import { TokenResponse } from '../types/token-response';
import { createTokenResponse } from '../utils/create-token-response';
import { GrantType } from './grant-type';

/**
 * Implementation of the **Password** Grant Type.
 *
 * In this Grant Type the Client must obtain the **username** and **password** of the End User
 * and present them as the Authorization Grant to the Authorization Server.
 *
 * @see https://www.rfc-editor.org/rfc/rfc6749.html#section-4.3
 */
@Injectable()
export class PasswordGrantType implements GrantType {
  /**
   * Name of the Grant Type.
   */
  public readonly name: string = 'password';

  /**
   * Instantiates a new Password Grant Type.
   *
   * @param scopeHandler Scope Handler of the Authorization Server.
   * @param accessTokenService Instance of the Access Token Service.
   * @param userService Instance of the User Service.
   * @param refreshTokenService Instance of the Refresh Token Service.
   */
  public constructor(
    private readonly scopeHandler: ScopeHandler,
    @Inject(ACCESS_TOKEN_SERVICE) private readonly accessTokenService: OAuth2AccessTokenService,
    @Inject(USER_SERVICE) private readonly userService: OAuth2UserService,
    @Optional() @Inject(REFRESH_TOKEN_SERVICE) private readonly refreshTokenService?: OAuth2RefreshTokenService
  ) {
    if (typeof this.userService.findByResourceOwnerCredentials !== 'function') {
      throw new TypeError(
        'Missing implementation of required method "OAuth2UserService.findByResourceOwnerCredentials".'
      );
    }
  }

  /**
   * Creates the Access Token Response with the Access Token issued to the Client.
   *
   * In this flow the Authorization Server checks the **username** and **password** provided by the Client
   * on behalf of the End User, checks if the credentials are valid, issues an Access Token and,
   * if allowed to the Client, a Refresh Token.
   *
   * @param parameters Parameters of the Toke Request.
   * @param client Client of the Request.
   * @returns Access Token Response.
   */
  public async handle(parameters: PasswordTokenRequest, client: OAuth2Client): Promise<TokenResponse> {
    this.checkParameters(parameters);

    const user = await this.getUser(parameters.username, parameters.password);
    const scopes = this.scopeHandler.getAllowedScopes(client, parameters.scope);

    const accessToken = await this.accessTokenService.create(scopes, client, user);

    const refreshToken = client.grantTypes.includes('refresh_token')
      ? await this.refreshTokenService?.create(scopes, client, user, accessToken)
      : undefined;

    return createTokenResponse(accessToken, refreshToken);
  }

  /**
   * Checks if the Parameters of the Token Request are valid.
   *
   * @param parameters Parameters of the Token Request.
   */
  private checkParameters(parameters: PasswordTokenRequest): void {
    const { username, password, scope } = parameters;

    if (typeof username !== 'string') {
      throw new InvalidRequestException({ error_description: 'Invalid parameter "username".' });
    }

    if (typeof password !== 'string') {
      throw new InvalidRequestException({ error_description: 'Invalid parameter "password".' });
    }

    this.scopeHandler.checkRequestedScope(scope);
  }

  /**
   * Searches a User from the application's storage based on the provided username and password.
   *
   * @param username Username of the User represented by the Client.
   * @param password Password of the User represented by the Client.
   * @returns User that matches the provided Credentials.
   */
  private async getUser(username: string, password: string): Promise<OAuth2User> {
    const user = await this.userService.findByResourceOwnerCredentials!(username, password);

    if (user === null) {
      throw new InvalidGrantException({ error_description: 'Invalid Credentials.' });
    }

    return user;
  }
}
