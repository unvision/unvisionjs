import { Inject, Injectable, InjectAll, Optional } from '@unvision/di';

import { OAuth2AuthorizationCode } from '../entities/oauth2-authorization-code.entity';
import { OAuth2Client } from '../entities/oauth2-client.entity';
import { InvalidGrantException } from '../exceptions/invalid-grant.exception';
import { InvalidRequestException } from '../exceptions/invalid-request.exception';
import {
  ACCESS_TOKEN_SERVICE,
  AUTHORIZATION_CODE_SERVICE,
  PKCE_METHOD,
  REFRESH_TOKEN_SERVICE,
} from '../metadata/metadata.keys';
import { PkceMethod } from '../pkce-methods/pkce-method';
import { OAuth2AccessTokenService } from '../services/oauth2-access-token.service';
import { OAuth2AuthorizationCodeService } from '../services/oauth2-authorization-code.service';
import { OAuth2RefreshTokenService } from '../services/oauth2-refresh-token.service';
import { AuthorizationCodeTokenRequest } from '../types/authorization-code.token-request';
import { TokenResponse } from '../types/token-response';
import { createTokenResponse } from '../utils/create-token-response';
import { GrantType } from './grant-type';

/**
 * Implementation of the **Authorization Code** Grant Type.
 *
 * In this Grant Type the Client obtains an Authorization Grant from the End User and exchanges it for an Access Token.
 *
 * This implementation uses PKCE by default, and enforces its use every time.
 *
 * @see https://www.rfc-editor.org/rfc/rfc6749.html#section-4.1
 */
@Injectable()
export class AuthorizationCodeGrantType implements GrantType {
  /**
   * Name of the Grant Type.
   */
  public readonly name: string = 'authorization_code';

  /**
   * Instantiates a new Authorization Code Grant Type.
   *
   * @param pkceMethods PKCE Methods.
   * @param authorizationCodeService Instance of the Authorization Code Service.
   * @param accessTokenService Instance of the Access Token Service.
   * @param refreshTokenService Instance of the Refresh Token Service.
   */
  public constructor(
    @InjectAll(PKCE_METHOD) private readonly pkceMethods: PkceMethod[],
    @Inject(AUTHORIZATION_CODE_SERVICE) private readonly authorizationCodeService: OAuth2AuthorizationCodeService,
    @Inject(ACCESS_TOKEN_SERVICE) private readonly accessTokenService: OAuth2AccessTokenService,
    @Optional() @Inject(REFRESH_TOKEN_SERVICE) private readonly refreshTokenService?: OAuth2RefreshTokenService
  ) {
    if (this.pkceMethods.length === 0) {
      throw new TypeError('Missing PKCE Methods for Authorization Code Grant Type.');
    }
  }

  /**
   * Creates the Access Token Response with the Access Token issued to the Client.
   *
   * In this part of the Authorization process the Authorization Server checks the validity of the Authorization Code
   * provided by the Client against the Authorization Code metadata saved at the application's storage.
   *
   * If the Client presented a valid Authorization Code that was granted to itself, and if it presented the correct PKCE
   * Code Verifier that matches the Code Challenge presented at the Authorization Endpoint, then the Provider issues
   * an Access Token and, if allowed to the Client, a Refresh Token.
   *
   * @param parameters Parameters of the Token Request.
   * @param client Client of the Request.
   * @returns Access Token Response.
   */
  public async handle(parameters: AuthorizationCodeTokenRequest, client: OAuth2Client): Promise<TokenResponse> {
    this.checkParameters(parameters);

    const authorizationCode = await this.getAuthorizationCode(parameters.code);

    try {
      this.checkAuthorizationCode(authorizationCode, parameters, client);

      const { scopes, user } = authorizationCode;

      const accessToken = await this.accessTokenService.create(scopes, client, user);

      const refreshToken = client.grantTypes.includes('refresh_token')
        ? await this.refreshTokenService?.create(scopes, client, user, accessToken)
        : undefined;

      return createTokenResponse(accessToken, refreshToken);
    } finally {
      await this.authorizationCodeService.revoke(authorizationCode);
    }
  }

  /**
   * Checks if the Parameters of the Token Request are valid.
   *
   * @param parameters Parameters of the Token Request.
   */
  private checkParameters(parameters: AuthorizationCodeTokenRequest): void {
    const { code, code_verifier: codeVerifier, redirect_uri: redirectUri } = parameters;

    if (typeof code !== 'string') {
      throw new InvalidRequestException({ error_description: 'Invalid parameter "code".' });
    }

    if (typeof codeVerifier !== 'string') {
      throw new InvalidRequestException({ error_description: 'Invalid parameter "code_verifier".' });
    }

    if (typeof redirectUri !== 'string') {
      throw new InvalidRequestException({ error_description: 'Invalid parameter "redirect_uri".' });
    }
  }

  /**
   * Fetches the requested Authorization Code from the application's storage.
   *
   * @param code Code provided by the Client.
   * @returns Authorization Code based on the provided Code.
   */
  private async getAuthorizationCode(code: string): Promise<OAuth2AuthorizationCode> {
    const authorizationCode = await this.authorizationCodeService.findOne(code);

    if (authorizationCode === null) {
      throw new InvalidGrantException({ error_description: 'Invalid Authorization Code.' });
    }

    return authorizationCode;
  }

  /**
   * Checks the Authorization Code against the provided data and against the Client of the Token Request.
   *
   * @param authorizationCode Authorization Code to be checked.
   * @param parameters Parameters of the Token Request.
   * @param client Client of the Request.
   */
  private checkAuthorizationCode(
    authorizationCode: OAuth2AuthorizationCode,
    parameters: AuthorizationCodeTokenRequest,
    client: OAuth2Client
  ): void {
    if (authorizationCode.client.id !== client.id) {
      throw new InvalidGrantException({ error_description: 'Mismatching Client Identifier.' });
    }

    if (new Date() < authorizationCode.validAfter) {
      throw new InvalidGrantException({ error_description: 'Authorization Code not yet valid.' });
    }

    if (new Date() > authorizationCode.expiresAt) {
      throw new InvalidGrantException({ error_description: 'Expired Authorization Code.' });
    }

    if (authorizationCode.isRevoked) {
      throw new InvalidGrantException({ error_description: 'Revoked Authorization Code.' });
    }

    if (authorizationCode.redirectUri !== parameters.redirect_uri) {
      throw new InvalidGrantException({ error_description: 'Mismatching Redirect URI.' });
    }

    const pkceMethod = this.getPkceMethod(authorizationCode.codeChallengeMethod ?? 'plain');

    if (!pkceMethod.verify(authorizationCode.codeChallenge, parameters.code_verifier)) {
      throw new InvalidGrantException({ error_description: 'Invalid PKCE Code Challenge.' });
    }
  }

  /**
   * Retrieves the requested PKCE Method.
   *
   * @param codeChallengeMethod PKCE Method to be retrieved.
   * @returns Instance of the requested PKCE Method.
   */
  private getPkceMethod(codeChallengeMethod: string): PkceMethod {
    const pkceMethod = this.pkceMethods.find((pkceMethod) => pkceMethod.name === codeChallengeMethod);

    if (pkceMethod === undefined) {
      throw new InvalidRequestException({ error_description: `Unsupported PKCE Method "${codeChallengeMethod}".` });
    }

    return pkceMethod;
  }
}
