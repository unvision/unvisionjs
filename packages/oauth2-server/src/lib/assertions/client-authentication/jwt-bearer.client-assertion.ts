import { Inject, Injectable } from '@unvision/di';
import {
  JsonWebKey,
  JsonWebSignature,
  JsonWebSignatureHeader,
  JsonWebSignatureHeaderParameters,
  JsonWebTokenClaims,
  SupportedJsonWebSignatureAlgorithm,
} from '@unvision/jose';

import { URL } from 'url';

import { AuthorizationServerOptions } from '../../authorization-server/options/authorization-server.options';
import { ClientAuthentication } from '../../client-authentication/client-authentication';
import { OAuth2Client } from '../../entities/oauth2-client.entity';
import { InvalidClientException } from '../../exceptions/invalid-client.exception';
import { OAuth2Exception } from '../../exceptions/oauth2.exception';
import { Request } from '../../http/request';
import { AUTHORIZATION_SERVER_OPTIONS, CLIENT_SERVICE } from '../../metadata/metadata.keys';
import { OAuth2ClientService } from '../../services/oauth2-client.service';

/**
 * Interface of the parameters expected from the POST body.
 */
interface AssertionParameters {
  /**
   * Client Assertion provided by the Client.
   */
  readonly client_assertion: string;

  /**
   * Client Assertion Type requested by the Client.
   */
  readonly client_assertion_type: string;
}

/**
 * Implementation of the JWT Bearer Client Assertion as described in RFC 7523.
 *
 * @see https://www.rfc-editor.org/rfc/rfc7523.html
 */
@Injectable()
export abstract class JwtBearerClientAssertion implements ClientAuthentication {
  /**
   * JSON Web Signature Algorithms.
   */
  protected abstract readonly algorithms: SupportedJsonWebSignatureAlgorithm[];

  /**
   * Name of the Client Authentication Method.
   */
  public abstract readonly name: string;

  /**
   * Name of the Client Assertion Type.
   */
  public readonly clientAssertionType: string = 'urn:ietf:params:oauth:client-assertion-type:jwt-bearer';

  /**
   * Instantiates a new JWT Bearer Client Authentication Method.
   *
   * @param authorizationServerOptions Configuration Parameters of the Authorization Server.
   * @param clientService Instance of the Client Service.
   */
  public constructor(
    @Inject(AUTHORIZATION_SERVER_OPTIONS) protected readonly authorizationServerOptions: AuthorizationServerOptions,
    @Inject(CLIENT_SERVICE) protected readonly clientService: OAuth2ClientService
  ) {}

  /**
   * Checks if the Client Authentication Method has been requested by the Client.
   *
   * @param request HTTP Request.
   */
  public hasBeenRequested(request: Request): boolean {
    const isValidClientAssertion =
      request.body.client_assertion_type === this.clientAssertionType &&
      typeof request.body.client_assertion === 'string';

    if (!isValidClientAssertion) {
      return false;
    }

    try {
      const [header] = JsonWebSignature.decode(request.body.client_assertion);

      return (
        this.algorithms.includes(header.alg) &&
        this.authorizationServerOptions.clientAuthenticationSignatureAlgorithms.includes(header.alg)
      );
    } catch (exc: any) {
      throw new InvalidClientException({ error_description: 'Invalid JSON Web Token Client Assertion.' }, exc);
    }
  }

  /**
   * Authenticates and returns the Client of the Request.
   *
   * @param request HTTP Request.
   * @returns Authenticated Client.
   */
  public async authenticate(request: Request): Promise<OAuth2Client> {
    const { client_assertion: clientAssertion } = <AssertionParameters>request.body;

    try {
      const [header, claims] = this.getClientAssertionComponents(clientAssertion, request);

      const client = await this.getClient(<string>claims.sub);

      // TODO: allow the application to validate the claims as it sees fit.

      if (client.authenticationMethod !== this.name) {
        throw new InvalidClientException({
          error_description: `This Client is not allowed to use the Authentication Method "${this.name}".`,
        });
      }

      if (!client.authenticationSigningAlgorithms?.includes(header.alg)) {
        throw new InvalidClientException({
          error_description: `This Client is not allowed to use the Authentication Method "${this.name}".`,
        });
      }

      const clientKey = await this.getClientKey(client, header);

      await JsonWebSignature.verify(clientAssertion, clientKey, this.algorithms);

      return client;
    } catch (exc: any) {
      throw exc instanceof OAuth2Exception
        ? exc
        : new InvalidClientException({ error_description: 'Invalid JSON Web Token Client Assertion.' }, exc);
    }
  }

  /**
   * Extracts, validates and returns the JSON Web Signature Header and JSON Web Token Claims of the Client Assertion.
   *
   * @param clientAssertion JSON Web Token Client Assertion provided by the Client.
   * @param request OAuth 2.0 Request.
   * @returns 2-tuple with the JSON Web Signature Header and the JSON Web Token Claims of the Client Assertion.
   */
  private getClientAssertionComponents(
    clientAssertion: string,
    request: Request
  ): [JsonWebSignatureHeader, JsonWebTokenClaims] {
    const [header, payload] = JsonWebSignature.decode(clientAssertion);

    if (header.alg === 'none') {
      throw new InvalidClientException({ error_description: 'Invalid JSON Web Signature Algorithm "none".' });
    }

    const claims = new JsonWebTokenClaims(JSON.parse(payload.toString('utf8')), {
      iss: { essential: true },
      sub: { essential: true },
      aud: { essential: true, value: new URL(request.path, this.authorizationServerOptions.issuer).href },
      exp: { essential: true },
      jti: { essential: true },
    });

    if (claims.iss !== claims.sub) {
      throw new InvalidClientException({ error_description: 'The values of "iss" and "sub" are different.' });
    }

    return [header, claims];
  }

  /**
   * Fetches the Client of the Client Assertion from the application's storage.
   *
   * @param id Identifier of the Client that issued the Client Assertion.
   * @returns Client of the Client Assertion.
   */
  private async getClient(id: string): Promise<OAuth2Client> {
    const client = await this.clientService.findOne(id);

    if (client === null) {
      throw new InvalidClientException({ error_description: 'Invalid Client.' });
    }

    return client;
  }

  /**
   * Returns the JSON Web Key of the Client used to validate the Client Assertion.
   *
   * @param client Client of the Request.
   * @param header JSON Web Signature Header of the Client Assertion.
   * @returns JSON Web Key of the Client based on the JSON Web Signature Header.
   */
  protected abstract getClientKey(client: OAuth2Client, header: JsonWebSignatureHeaderParameters): Promise<JsonWebKey>;
}
