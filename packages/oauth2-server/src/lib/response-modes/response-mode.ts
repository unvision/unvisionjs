import { Response } from '../http/response';
import { AuthorizationResponse } from '../types/authorization-response';

/**
 * Interface of a Response Mode.
 *
 * @see https://openid.net/specs/oauth-v2-multiple-response-types-1_0.html#ResponseModes
 */
export interface ResponseMode {
  /**
   * Name of the Response Mode.
   */
  readonly name: string;

  /**
   * Creates and returns an HTTP Response containing the Parameters of the Authorization Response.
   *
   * @param redirectUri Redirect URI that the User-Agent will be redirected to.
   * @param parameters Authorization Response Parameters that will be returned to the Client Application.
   * @returns HTTP Response containing the Authorization Response Parameters.
   */
  createHttpResponse(redirectUri: string, parameters: AuthorizationResponse): Response;
}
