import { Constructor } from '@unvision/di';

import { FormPostResponseMode } from './form-post.response-mode';
import { FragmentResponseMode } from './fragment.response-mode';
import { QueryResponseMode } from './query.response-mode';
import { ResponseMode } from './response-mode';

export const responseModeRegistry: Record<string, Constructor<ResponseMode>> = {
  form_post: FormPostResponseMode,
  fragment: FragmentResponseMode,
  query: QueryResponseMode,
};
