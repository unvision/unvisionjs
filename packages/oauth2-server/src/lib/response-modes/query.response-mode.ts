import { Injectable } from '@unvision/di';

import { URL, URLSearchParams } from 'url';

import { Response } from '../http/response';
import { AuthorizationResponse } from '../types/authorization-response';
import { ResponseMode } from './response-mode';

/**
 * Implementation of the **Query** Response Mode.
 *
 * @see https://openid.net/specs/oauth-v2-multiple-response-types-1_0.html#ResponseModes
 */
@Injectable()
export class QueryResponseMode implements ResponseMode {
  /**
   * Name of the Response Mode.
   */
  public readonly name: string = 'query';

  /**
   * Creates a Redirect Response to the provided Redirect URI with the provided Parameters at the Query of the URI.
   *
   * @param redirectUri Redirect URI that the User-Agent will be redirected to.
   * @param parameters Authorization Response Parameters that will be returned to the Client Application.
   * @returns HTTP Response containing the Authorization Response Parameters.
   */
  public createHttpResponse(redirectUri: string, parameters: AuthorizationResponse): Response {
    const url = new URL(redirectUri);
    const searchParams = new URLSearchParams(parameters);

    url.search = searchParams.toString();

    return new Response().redirect(url);
  }
}
