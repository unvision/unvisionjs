import { DependencyInjectionContainer } from '@unvision/di';

import { Buffer } from 'buffer';

import { Response } from '../http/response';
import { FragmentResponseMode } from './fragment.response-mode';

describe('Fragment Response Mode', () => {
  let responseMode: FragmentResponseMode;

  beforeEach(() => {
    const container = new DependencyInjectionContainer();

    container.bind(FragmentResponseMode).toSelf().asSingleton();

    responseMode = container.resolve(FragmentResponseMode);
  });

  it('should have "fragment" as its name.', () => {
    expect(responseMode.name).toBe('fragment');
  });

  it('should create a redirect http response with a populated uri fragment.', () => {
    expect(
      responseMode.createHttpResponse('https://example.com', { foo: 'foo', bar: 'bar', baz: 'baz' })
    ).toMatchObject<Partial<Response>>({
      body: Buffer.alloc(0),
      headers: { Location: 'https://example.com/#foo=foo&bar=bar&baz=baz' },
      statusCode: 303,
    });
  });
});
