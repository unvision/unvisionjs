/**
 * Access Token Service Metadata Token.
 */
export const ACCESS_TOKEN_SERVICE = Symbol('unvision:oauth2-server:access-token-service');

/**
 * Authorization Code Service Metadata Token.
 */
export const AUTHORIZATION_CODE_SERVICE = Symbol('unvision:oauth2-server:authorization-code-service');

/**
 * Authorization Server Options Metadata Token.
 */
export const AUTHORIZATION_SERVER_OPTIONS = Symbol('unvision:oauth2-server:authorization-server-options');

/**
 * Client Authentication Metadata Token.
 */
export const CLIENT_AUTHENTICATION = Symbol('unvision:oauth2-server:client-authentication');

/**
 * Client Service Metadata Token.
 */
export const CLIENT_SERVICE = Symbol('unvision:oauth2-server:client-service');

/**
 * Consent Service Metadata Token.
 */
export const CONSENT_SERVICE = Symbol('unvision:oauth2-server:consent-service');

/**
 * Endpoint Metadata Token.
 */
export const ENDPOINT = Symbol('unvision:oauth2-server:endpoint');

/**
 * Grant Type Metadata Token.
 */
export const GRANT_TYPE = Symbol('unvision:oauth2-server:grant-type');

/**
 * Interaction Type Metadata Token.
 */
export const INTERACTION_TYPE = Symbol('unvision:oauth2-server:interaction-type');

/**
 * PKCE Method Metadata Token.
 */
export const PKCE_METHOD = Symbol('unvision:oauth2-server:pkce-method');

/**
 * Refresh Token Service Metadata Token.
 */
export const REFRESH_TOKEN_SERVICE = Symbol('unvision:oauth2-server:refresh-token-service');

/**
 * Response Type Metadata Token.
 */
export const RESPONSE_TYPE = Symbol('unvision:oauth2-server:response-type');

/**
 * Response Mode Metadata Token.
 */
export const RESPONSE_MODE = Symbol('unvision:oauth2-server:response-mode');

/**
 * Session Service Metadata Token.
 */
export const SESSION_SERVICE = Symbol('unvision:oauth2-server:session-service');

/**
 * User Service Metadata Token.
 */
export const USER_SERVICE = Symbol('unvision:oauth2-server:user-service');
