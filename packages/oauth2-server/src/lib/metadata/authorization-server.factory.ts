import { Constructor, getContainer } from '@unvision/di';

import { AuthorizationServer } from '../authorization-server/authorization-server';
import { AuthorizationServerOptions } from '../authorization-server/options/authorization-server.options';
import { ClientAuthentication } from '../client-authentication/client-authentication';
import { clientAuthenticationRegistry } from '../client-authentication/client-authentication.registry';
import { AuthorizationEndpoint } from '../endpoints/authorization.endpoint';
import { DiscoveryEndpoint } from '../endpoints/discovery.endpoint';
import { Endpoint } from '../endpoints/endpoint';
import { InteractionEndpoint } from '../endpoints/interaction.endpoint';
import { IntrospectionEndpoint } from '../endpoints/introspection.endpoint';
import { RevocationEndpoint } from '../endpoints/revocation.endpoint';
import { TokenEndpoint } from '../endpoints/token.endpoint';
import { GrantType } from '../grant-types/grant-type';
import { grantTypeRegistry } from '../grant-types/grant-type.registry';
import { ClientAuthenticationHandler } from '../handlers/client-authentication.handler';
import { ScopeHandler } from '../handlers/scope.handler';
import { InteractionType } from '../interaction-types/interaction-type';
import { interactionTypeRegistry } from '../interaction-types/interaction-type.registry';
import { PkceMethod } from '../pkce-methods/pkce-method';
import { pkceMethodRegistry } from '../pkce-methods/pkce-method.registry';
import { ResponseMode } from '../response-modes/response-mode';
import { responseModeRegistry } from '../response-modes/response-mode.registry';
import { ResponseType } from '../response-types/response-type';
import { responseTypeRegistry } from '../response-types/response-type.registry';
import { DefaultAccessTokenService } from '../services/default/access-token.service';
import { DefaultAuthorizationCodeService } from '../services/default/authorization-code.service';
import { DefaultClientService } from '../services/default/client.service';
import { DefaultConsentService } from '../services/default/consent.service';
import { DefaultRefreshTokenService } from '../services/default/refresh-token.service';
import { DefaultSessionService } from '../services/default/session.service';
import { DefaultUserService } from '../services/default/user.service';
import { OAuth2AccessTokenService } from '../services/oauth2-access-token.service';
import { OAuth2AuthorizationCodeService } from '../services/oauth2-authorization-code.service';
import { OAuth2ClientService } from '../services/oauth2-client.service';
import { OAuth2ConsentService } from '../services/oauth2-consent.service';
import { OAuth2RefreshTokenService } from '../services/oauth2-refresh-token.service';
import { OAuth2SessionService } from '../services/oauth2-session.service';
import { OAuth2UserService } from '../services/oauth2-user.service';
import { AuthorizationServerMetadataOptions } from './authorization-server-metadata.options';
import {
  ACCESS_TOKEN_SERVICE,
  AUTHORIZATION_CODE_SERVICE,
  AUTHORIZATION_SERVER_OPTIONS,
  CLIENT_AUTHENTICATION,
  CLIENT_SERVICE,
  CONSENT_SERVICE,
  ENDPOINT,
  GRANT_TYPE,
  INTERACTION_TYPE,
  PKCE_METHOD,
  REFRESH_TOKEN_SERVICE,
  RESPONSE_MODE,
  RESPONSE_TYPE,
  SESSION_SERVICE,
  USER_SERVICE,
} from './metadata.keys';

/**
 * Factory class for configuring and instantiating an OAuth 2.0 Authorization Server.
 */
export class AuthorizationServerFactory {
  /**
   * Dependency Injection Container of the OAuth 2.0 Authorization Server.
   */
  private static readonly container = getContainer('oauth2');

  /**
   * Authorization Server Metadata Options for usage in the factory.
   */
  private static readonly metadataOptions: Partial<AuthorizationServerMetadataOptions>;

  /**
   * Configuration Parameters of the Authorization Server.
   */
  private static readonly authorizationServerOptions: AuthorizationServerOptions;

  /**
   * Fabricates a new instance of the provided OAuth 2.0 Authorization Server.
   *
   * @param server Authorization Server Constructor.
   * @returns Instance of the provided Authorization Server.
   */
  public static async create<T extends AuthorizationServer>(server: Constructor<T>): Promise<T> {
    Reflect.set(this, 'metadataOptions', Reflect.getMetadata('oauth2-server:options', server));

    this.configure();
    this.container.bind<AuthorizationServer>('AuthorizationServer').toClass(server).asSingleton();

    return this.container.resolve<T>('AuthorizationServer');
  }

  /**
   * Bootstraps the Configuration of the OAuth 2.0 Authorization Server.
   */
  private static configure(): void {
    this.setAuthorizationServerOptions();
    this.setClientAuthentication();
    this.setGrantTypes();
    this.setInteractionTypes();
    this.setResponseTypes();
    this.setResponseModes();
    this.setPkceMethods();
    this.setEndpoints();
    this.setHandlers();
    this.setAccessTokenService();
    this.setAuthorizationCodeService();
    this.setClientService();
    this.setConsentService();
    this.setRefreshTokenService();
    this.setSessionService();
    this.setUserService();
  }

  /**
   * Defines the Settings of the Authorization Server.
   */
  private static setAuthorizationServerOptions(): void {
    const options: AuthorizationServerOptions = {
      issuer: <string>this.metadataOptions.issuer,
      scopes: <string[]>this.metadataOptions.scopes,
      clientAuthenticationMethods: this.metadataOptions.clientAuthenticationMethods ?? ['client_secret_basic'],
      grantTypes: this.metadataOptions.grantTypes ?? ['authorization_code'],
      responseTypes: this.metadataOptions.responseTypes ?? ['code'],
      responseModes: this.metadataOptions.responseModes ?? ['query'],
      pkceMethods: this.metadataOptions.pkceMethods ?? ['S256'],
      clientAuthenticationSignatureAlgorithms: this.metadataOptions.clientAuthenticationSignatureAlgorithms ?? [],
      userInteraction: this.metadataOptions.userInteraction,
      enableRefreshTokenRotation: this.metadataOptions.enableRefreshTokenRotation ?? false,
      enableAccessTokenRevocation: this.metadataOptions.enableAccessTokenRevocation ?? true,
      enableRefreshTokenIntrospection: this.metadataOptions.enableRefreshTokenIntrospection ?? false,
    };

    this.container.bind<AuthorizationServerOptions>(AUTHORIZATION_SERVER_OPTIONS).toValue(options);

    Reflect.set(this, 'authorizationServerOptions', options);
  }

  /**
   * Defines the Client Authentication Methods supported by the Authorization Server.
   */
  private static setClientAuthentication(): void {
    const { clientAuthenticationMethods } = this.authorizationServerOptions;

    if (clientAuthenticationMethods.length === 0) {
      return;
    }

    clientAuthenticationMethods.forEach((clientAuthenticationMethod) => {
      const constructor = <Constructor<ClientAuthentication>>clientAuthenticationRegistry[clientAuthenticationMethod];
      this.container.bind<ClientAuthentication>(CLIENT_AUTHENTICATION).toClass(constructor).asSingleton();
    });
  }

  /**
   * Defines the Grant Types supported by the Authorization Server.
   */
  private static setGrantTypes(): void {
    const { grantTypes } = this.authorizationServerOptions;

    if (grantTypes.length === 0) {
      return;
    }

    grantTypes.forEach((grantType) => {
      const constructor = <Constructor<GrantType>>grantTypeRegistry[grantType];
      this.container.bind<GrantType>(GRANT_TYPE).toClass(constructor).asSingleton();
    });
  }

  /**
   * Defines the Interaction Types supported by the Authorization Server.
   */
  private static setInteractionTypes(): void {
    Object.values(interactionTypeRegistry).forEach((interactionType) => {
      this.container.bind<InteractionType>(INTERACTION_TYPE).toClass(interactionType).asSingleton();
    });
  }

  /**
   * Defines the Response Types supported by the Authorization Server.
   */
  private static setResponseTypes(): void {
    const { responseTypes } = this.authorizationServerOptions;

    if (responseTypes.length === 0) {
      return;
    }

    responseTypes.forEach((responseType) => {
      const constructor = <Constructor<ResponseType>>responseTypeRegistry[responseType];
      this.container.bind<ResponseType>(RESPONSE_TYPE).toClass(constructor).asSingleton();
    });
  }

  /**
   * Defines the Response Modes supported by the Authorization Server.
   */
  private static setResponseModes(): void {
    const { responseModes } = this.authorizationServerOptions;

    if (responseModes.length === 0) {
      return;
    }

    responseModes.forEach((responseMode) => {
      const constructor = <Constructor<ResponseMode>>responseModeRegistry[responseMode];
      this.container.bind<ResponseMode>(RESPONSE_MODE).toClass(constructor).asSingleton();
    });
  }

  /**
   * Defines the PKCE Methods supported by the Authorization Server.
   */
  private static setPkceMethods(): void {
    const { pkceMethods } = this.authorizationServerOptions;

    if (pkceMethods.length === 0) {
      return;
    }

    pkceMethods.forEach((pkceMethod) => {
      const constructor = <Constructor<PkceMethod>>pkceMethodRegistry[pkceMethod];
      this.container.bind<PkceMethod>(PKCE_METHOD).toClass(constructor).asSingleton();
    });
  }

  /**
   * Defines the Endpoints supported by the Authorization Server.
   */
  private static setEndpoints(): void {
    const hasAuthorizationEndpoint = this.container.isRegistered<ResponseType>(RESPONSE_TYPE);
    const hasTokenEndpoint = this.container.isRegistered<GrantType>(GRANT_TYPE);

    if (!hasAuthorizationEndpoint && !hasTokenEndpoint) {
      throw new Error('Cannot instantiate an Authorization Server without Grants.');
    }

    if (hasAuthorizationEndpoint) {
      this.container.bind<Endpoint>(ENDPOINT).toClass(AuthorizationEndpoint).asSingleton();
      this.container.bind<Endpoint>(ENDPOINT).toClass(InteractionEndpoint).asSingleton();
    }

    if (hasTokenEndpoint) {
      this.container.bind<Endpoint>(ENDPOINT).toClass(TokenEndpoint).asSingleton();
    }

    if (this.metadataOptions.enableRevocationEndpoint !== false) {
      this.container.bind<Endpoint>(ENDPOINT).toClass(RevocationEndpoint).asSingleton();
    }

    if (this.metadataOptions.enableIntrospectionEndpoint !== false) {
      this.container.bind<Endpoint>(ENDPOINT).toClass(IntrospectionEndpoint).asSingleton();
    }

    this.container.bind<Endpoint>(ENDPOINT).toClass(DiscoveryEndpoint).asSingleton();
  }

  /**
   * Defines the Handlers of the Authorization Server.
   */
  private static setHandlers(): void {
    this.container.bind(ClientAuthenticationHandler).toSelf().asSingleton();
    this.container.bind(ScopeHandler).toSelf().asSingleton();
  }

  /**
   * Defines the Access Token Service used by the Authorization Server.
   */
  private static setAccessTokenService(): void {
    const accessTokenService = this.metadataOptions.accessTokenService ?? DefaultAccessTokenService;

    const binding = this.container.bind<OAuth2AccessTokenService>(ACCESS_TOKEN_SERVICE);

    typeof accessTokenService === 'function'
      ? binding.toClass(accessTokenService).asSingleton()
      : binding.toValue(accessTokenService);
  }

  /**
   * Defines the Authorization Code Service used by the Authorization Server.
   */
  private static setAuthorizationCodeService(): void {
    const authorizationCodeService = this.metadataOptions.authorizationCodeService ?? DefaultAuthorizationCodeService;

    const binding = this.container.bind<OAuth2AuthorizationCodeService>(AUTHORIZATION_CODE_SERVICE);

    typeof authorizationCodeService === 'function'
      ? binding.toClass(authorizationCodeService).asSingleton()
      : binding.toValue(authorizationCodeService);
  }

  /**
   * Defines the Client Service used by the Authorization Server.
   */
  private static setClientService(): void {
    const clientService = this.metadataOptions.clientService ?? DefaultClientService;

    const binding = this.container.bind<OAuth2ClientService>(CLIENT_SERVICE);

    typeof clientService === 'function' ? binding.toClass(clientService).asSingleton() : binding.toValue(clientService);
  }

  /**
   * Defines the Consent Service used by the Authorization Server.
   */
  private static setConsentService(): void {
    const consentService = this.metadataOptions.consentService ?? DefaultConsentService;

    const binding = this.container.bind<OAuth2ConsentService>(CONSENT_SERVICE);

    typeof consentService === 'function'
      ? binding.toClass(consentService).asSingleton()
      : binding.toValue(consentService);
  }

  /**
   * Defines the Refresh Token Service used by the Authorization Server.
   */
  private static setRefreshTokenService(): void {
    const refreshTokenService = this.metadataOptions.refreshTokenService ?? DefaultRefreshTokenService;

    const binding = this.container.bind<OAuth2RefreshTokenService>(REFRESH_TOKEN_SERVICE);

    typeof refreshTokenService === 'function'
      ? binding.toClass(refreshTokenService).asSingleton()
      : binding.toValue(refreshTokenService);
  }

  /**
   * Defines the Session Service used by the Authorization Server.
   */
  private static setSessionService(): void {
    const sessionService = this.metadataOptions.sessionService ?? DefaultSessionService;

    const binding = this.container.bind<OAuth2SessionService>(SESSION_SERVICE);

    typeof sessionService === 'function'
      ? binding.toClass(sessionService).asSingleton()
      : binding.toValue(sessionService);
  }

  /**
   * Defines the User Service used by the Authorization Server.
   */
  private static setUserService(): void {
    const userService = this.metadataOptions.userService ?? DefaultUserService;

    const binding = this.container.bind<OAuth2UserService>(USER_SERVICE);

    typeof userService === 'function' ? binding.toClass(userService).asSingleton() : binding.toValue(userService);
  }
}
