import { Constructor } from '@unvision/di';
import { SupportedJsonWebSignatureAlgorithm } from '@unvision/jose';

import { UserInteractionOptions } from '../authorization-server/options/user-interaction.options';
import { OAuth2AccessTokenService } from '../services/oauth2-access-token.service';
import { OAuth2AuthorizationCodeService } from '../services/oauth2-authorization-code.service';
import { OAuth2ClientService } from '../services/oauth2-client.service';
import { OAuth2ConsentService } from '../services/oauth2-consent.service';
import { OAuth2RefreshTokenService } from '../services/oauth2-refresh-token.service';
import { OAuth2SessionService } from '../services/oauth2-session.service';
import { OAuth2UserService } from '../services/oauth2-user.service';

/**
 * Configuration Parameters of the Authorization Server Metadata Decorator.
 */
export interface AuthorizationServerMetadataOptions {
  /**
   * Identifier of the Authorization Server's Issuer.
   */
  readonly issuer: string;

  /**
   * Scopes to be registered at the Authorization Server.
   */
  readonly scopes: string[];

  /**
   * Client Authentication Methods to be registered at the Authorization Server.
   *
   * @default ['client_secret_basic']
   */
  readonly clientAuthenticationMethods?: string[];

  /**
   * Grant Types to be registered at the Authorization Server.
   *
   * @default ['authorization_code']
   */
  readonly grantTypes?: string[];

  /**
   * Response Types to be registered at the Authorization Server.
   *
   * @default ['code']
   */
  readonly responseTypes?: string[];

  /**
   * Response Modes to be registered at the Authorization Server.
   *
   * @default ['query']
   */
  readonly responseModes?: string[];

  /**
   * PKCE Methods to be registered at the Authorization Server.
   *
   * @default ['S256']
   */
  readonly pkceMethods?: string[];

  /**
   * JSON Web Signature Algoithms for Client Authentication to be registered at the Authorization Server.
   */
  readonly clientAuthenticationSignatureAlgorithms?: SupportedJsonWebSignatureAlgorithm[];

  /**
   * Defines the Parameters of the User Interaction.
   */
  readonly userInteraction?: UserInteractionOptions;

  /**
   * Enables Refresh Token Rotation on the Authorization Server.
   *
   * @default false
   */
  readonly enableRefreshTokenRotation?: boolean;

  /**
   * Enables the Revocation Endpoint on the Authorization Server.
   *
   * @default true
   */
  readonly enableRevocationEndpoint?: boolean;

  /**
   * Enables the Introspection Endpoint on the Authorization Server.
   *
   * @default true
   */
  readonly enableIntrospectionEndpoint?: boolean;

  /**
   * Enables Access Token Revocation on the Authorization Server.
   *
   * @default true
   */
  readonly enableAccessTokenRevocation?: boolean;

  /**
   * Enables Refresh Token Introspection on the Authorization Server.
   *
   * @default false
   */
  readonly enableRefreshTokenIntrospection?: boolean;

  /**
   * Access Token Service.
   */
  readonly accessTokenService?: OAuth2AccessTokenService | Constructor<OAuth2AccessTokenService>;

  /**
   * Authorization Code Service.
   */
  readonly authorizationCodeService?: OAuth2AuthorizationCodeService | Constructor<OAuth2AuthorizationCodeService>;

  /**
   * Client Service.
   */
  readonly clientService?: OAuth2ClientService | Constructor<OAuth2ClientService>;

  /**
   * Consent Service.
   */
  readonly consentService?: OAuth2ConsentService | Constructor<OAuth2ConsentService>;

  /**
   * Session Service.
   */
  readonly sessionService?: OAuth2SessionService | Constructor<OAuth2SessionService>;

  /**
   * Refresh Token Service.
   */
  readonly refreshTokenService?: OAuth2RefreshTokenService | Constructor<OAuth2RefreshTokenService>;

  /**
   * User Service.
   */
  readonly userService?: OAuth2UserService | Constructor<OAuth2UserService>;
}
