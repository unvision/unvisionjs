import { AuthorizationServerMetadataOptions } from './authorization-server-metadata.options';

/**
 * Defines the Configuration options of the Authorization Server.
 *
 * @param options Configuration options of the Authorization Server.
 */
export function AuthorizationServerMetadata(options: AuthorizationServerMetadataOptions): ClassDecorator {
  return function (target): void {
    Reflect.defineMetadata('oauth2-server:options', options, target);
  };
}
