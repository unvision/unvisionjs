import { DependencyInjectionContainer } from '@unvision/di';

import { Buffer } from 'buffer';
import { URL, URLSearchParams } from 'url';

import { AuthorizationServerOptions } from '../authorization-server/options/authorization-server.options';
import { OAuth2Client } from '../entities/oauth2-client.entity';
import { OAuth2Consent } from '../entities/oauth2-consent.entity';
import { OAuth2Session } from '../entities/oauth2-session.entity';
import { AccessDeniedException } from '../exceptions/access-denied.exception';
import { InvalidClientException } from '../exceptions/invalid-client.exception';
import { InvalidRequestException } from '../exceptions/invalid-request.exception';
import { InvalidScopeException } from '../exceptions/invalid-scope.exception';
import { UnauthorizedClientException } from '../exceptions/unauthorized-client.exception';
import { UnsupportedResponseTypeException } from '../exceptions/unsupported-response-type.exception';
import { ScopeHandler } from '../handlers/scope.handler';
import { Request } from '../http/request';
import { Response } from '../http/response';
import {
  AUTHORIZATION_SERVER_OPTIONS,
  CLIENT_SERVICE,
  CONSENT_SERVICE,
  RESPONSE_MODE,
  RESPONSE_TYPE,
  SESSION_SERVICE,
} from '../metadata/metadata.keys';
import { ResponseMode } from '../response-modes/response-mode';
import { ResponseType } from '../response-types/response-type';
import { OAuth2ClientService } from '../services/oauth2-client.service';
import { OAuth2ConsentService } from '../services/oauth2-consent.service';
import { OAuth2SessionService } from '../services/oauth2-session.service';
import { AuthorizationRequest } from '../types/authorization-request';
import { AuthorizationEndpoint } from './authorization.endpoint';

describe('Authorization Endpoint.', () => {
  let endpoint: AuthorizationEndpoint;

  const clientServiceMock = jest.mocked<OAuth2ClientService>({
    findOne: jest.fn(),
  });

  const sessionServiceMock = jest.mocked<OAuth2SessionService>({
    create: jest.fn(),
    findOne: jest.fn(),
    remove: jest.fn(),
    save: jest.fn(),
  });

  const consentServiceMock = jest.mocked<OAuth2ConsentService>({
    create: jest.fn(),
    findOne: jest.fn(),
    remove: jest.fn(),
    save: jest.fn(),
  });

  const responseTypesMocks = [
    jest.mocked<ResponseType>({ name: 'code', defaultResponseMode: 'query', handle: jest.fn() }),
    jest.mocked<ResponseType>({ name: 'token', defaultResponseMode: 'fragment', handle: jest.fn() }),
  ];

  const responseModesMocks = [
    jest.mocked<ResponseMode>({ name: 'query', createHttpResponse: jest.fn() }),
    jest.mocked<ResponseMode>({ name: 'fragment', createHttpResponse: jest.fn() }),
  ];

  const authorizationServerOptions = <AuthorizationServerOptions>{
    issuer: 'https://server.example.com',
    scopes: ['foo', 'bar', 'baz', 'qux'],
    userInteraction: { consentUrl: '/auth/consent', errorUrl: '/oauth/error', loginUrl: '/auth/login' },
  };

  beforeEach(() => {
    const container = new DependencyInjectionContainer();

    container.bind<AuthorizationServerOptions>(AUTHORIZATION_SERVER_OPTIONS).toValue(authorizationServerOptions);
    container.bind<OAuth2ClientService>(CLIENT_SERVICE).toValue(clientServiceMock);
    container.bind<OAuth2SessionService>(SESSION_SERVICE).toValue(sessionServiceMock);
    container.bind<OAuth2ConsentService>(CONSENT_SERVICE).toValue(consentServiceMock);
    container.bind(ScopeHandler).toSelf().asSingleton();
    container.bind(AuthorizationEndpoint).toSelf().asSingleton();

    responseTypesMocks.forEach((responseType) => container.bind<ResponseType>(RESPONSE_TYPE).toValue(responseType));
    responseModesMocks.forEach((responseMode) => container.bind<ResponseMode>(RESPONSE_MODE).toValue(responseMode));

    endpoint = container.resolve(AuthorizationEndpoint);
  });

  describe('name', () => {
    it('should have "authorization" as its name.', () => {
      expect(endpoint.name).toBe('authorization');
    });
  });

  describe('path', () => {
    it('should have "/oauth/authorize" as its default path.', () => {
      expect(endpoint.path).toBe('/oauth/authorize');
    });
  });

  describe('httpMethods', () => {
    it('should have \'["get"]\' as its default path.', () => {
      expect(endpoint.httpMethods).toStrictEqual(['get']);
    });
  });

  describe('consentUrl', () => {
    it('should be defined based on the provided user interaction.', () => {
      expect(endpoint['consentUrl']).toBe('https://server.example.com/auth/consent');
    });
  });

  describe('errorUrl', () => {
    it('should be defined based on the provided user interaction.', () => {
      expect(endpoint['errorUrl']).toBe('https://server.example.com/oauth/error');
    });
  });

  describe('loginUrl', () => {
    it('should be defined based on the provided user interaction.', () => {
      expect(endpoint['loginUrl']).toBe('https://server.example.com/auth/login');
    });
  });

  describe('constructor', () => {
    it('should throw when not providing a user interaction object.', () => {
      expect(() => new AuthorizationEndpoint(<any>{}, [], [], <any>{}, <any>{}, <any>{}, <any>{})).toThrow(TypeError);
    });
  });

  describe('handle()', () => {
    let request: Request;

    beforeEach(() => {
      request = {
        body: {},
        cookies: { 'unvision:session': 'session_id', 'unvision:consent': 'consent_id' },
        headers: {},
        method: 'GET',
        path: '/oauth/authorize',
        query: {
          response_type: 'code',
          client_id: 'client_id',
          redirect_uri: 'https://example.com/callback',
          scope: 'foo bar',
          state: 'client_state',
        },
      };
    });

    it('should return an error response when not providing a "response_type" parameter.', async () => {
      delete request.query.response_type;

      const error = new InvalidRequestException({ error_description: 'Invalid parameter "response_type".' });
      const params = new URLSearchParams(error.toJSON());

      await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
        body: Buffer.alloc(0),
        headers: { Location: `https://server.example.com/oauth/error?${params.toString()}` },
        statusCode: 303,
      });
    });

    it('should return an error response when not providing a "client_id" parameter.', async () => {
      delete request.query.client_id;

      const error = new InvalidRequestException({ error_description: 'Invalid parameter "client_id".' });
      const params = new URLSearchParams(error.toJSON());

      await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
        body: Buffer.alloc(0),
        headers: { Location: `https://server.example.com/oauth/error?${params.toString()}` },
        statusCode: 303,
      });
    });

    it('should return an error response when not providing a "redirect_uri" parameter.', async () => {
      delete request.query.redirect_uri;

      const error = new InvalidRequestException({ error_description: 'Invalid parameter "redirect_uri".' });
      const params = new URLSearchParams(error.toJSON());

      await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
        body: Buffer.alloc(0),
        headers: { Location: `https://server.example.com/oauth/error?${params.toString()}` },
        statusCode: 303,
      });
    });

    it('should return an error response when not providing a "scope" parameter.', async () => {
      delete request.query.scope;

      const error = new InvalidRequestException({ error_description: 'Invalid parameter "scope".' });
      const params = new URLSearchParams(error.toJSON());

      await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
        body: Buffer.alloc(0),
        headers: { Location: `https://server.example.com/oauth/error?${params.toString()}` },
        statusCode: 303,
      });
    });

    it('should return an error response when a client is not found.', async () => {
      clientServiceMock.findOne.mockResolvedValueOnce(null);

      const error = new InvalidClientException({ error_description: 'Invalid Client.' });
      const params = new URLSearchParams(error.toJSON());

      await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
        body: Buffer.alloc(0),
        headers: { Location: `https://server.example.com/oauth/error?${params.toString()}` },
        statusCode: 303,
      });
    });

    it('should return an error response when requesting an unsupported response type.', async () => {
      request.query.response_type = 'unknown';

      clientServiceMock.findOne.mockResolvedValueOnce(<OAuth2Client>{ id: 'client_id' });

      const error = new UnsupportedResponseTypeException({ error_description: 'Unsupported response_type "unknown".' });
      const params = new URLSearchParams(error.toJSON());

      await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
        body: Buffer.alloc(0),
        headers: { Location: `https://server.example.com/oauth/error?${params.toString()}` },
        statusCode: 303,
      });
    });

    it('should return an error response when the client requests a response type that it is not allowed to request.', async () => {
      clientServiceMock.findOne.mockResolvedValueOnce(<OAuth2Client>{ id: 'client_id', responseTypes: ['token'] });

      const error = new UnauthorizedClientException({
        error_description: 'This Client is not allowed to request the response_type "code".',
      });

      const params = new URLSearchParams(error.toJSON());

      await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
        body: Buffer.alloc(0),
        headers: { Location: `https://server.example.com/oauth/error?${params.toString()}` },
        statusCode: 303,
      });
    });

    it('should return an error response when the client provides a redirect uri that it is not allowed to use.', async () => {
      clientServiceMock.findOne.mockResolvedValueOnce(<OAuth2Client>{
        id: 'client_id',
        redirectUris: ['https://bad.example.com/callback'],
        responseTypes: ['code'],
      });

      const error = new AccessDeniedException({ error_description: 'Invalid Redirect URI.' });
      const params = new URLSearchParams(error.toJSON());

      await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
        body: Buffer.alloc(0),
        headers: { Location: `https://server.example.com/oauth/error?${params.toString()}` },
        statusCode: 303,
      });
    });

    it('should return an error response when the client requests an unsupported scope.', async () => {
      request.query.scope = 'foo unknown bar';

      clientServiceMock.findOne.mockResolvedValueOnce(<OAuth2Client>{
        id: 'client_id',
        redirectUris: ['https://example.com/callback'],
        responseTypes: ['code'],
        scopes: ['foo', 'bar'],
      });

      const error = new InvalidScopeException({ error_description: 'Unsupported scope "unknown".' });
      const params = new URLSearchParams(error.toJSON());

      await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
        body: Buffer.alloc(0),
        headers: { Location: `https://server.example.com/oauth/error?${params.toString()}` },
        statusCode: 303,
      });
    });

    it('should return an error response when requesting an unsupported response mode.', async () => {
      request.query.response_mode = 'unknown';

      clientServiceMock.findOne.mockResolvedValueOnce(<OAuth2Client>{
        id: 'client_id',
        redirectUris: ['https://example.com/callback'],
        responseTypes: ['code'],
        scopes: ['foo', 'bar'],
      });

      const error = new InvalidRequestException({ error_description: 'Unsupported response_mode "unknown".' });
      const params = new URLSearchParams(error.toJSON());

      await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
        body: Buffer.alloc(0),
        headers: { Location: `https://server.example.com/oauth/error?${params.toString()}` },
        statusCode: 303,
      });
    });

    it('should return a redirect response to the login page when no session is found at the cookies.', async () => {
      delete request.cookies['unvision:session'];

      clientServiceMock.findOne.mockResolvedValueOnce(<OAuth2Client>{
        id: 'client_id',
        redirectUris: ['https://example.com/callback'],
        responseTypes: ['code'],
        scopes: ['foo', 'bar'],
      });

      sessionServiceMock.create.mockResolvedValueOnce(<OAuth2Session>{ id: 'session_id' });

      const params = new URLSearchParams({ login_challenge: 'session_id' });

      await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
        body: Buffer.alloc(0),
        cookies: { 'unvision:session': 'session_id' },
        headers: { Location: `https://server.example.com/auth/login?${params.toString()}` },
        statusCode: 303,
      });
    });

    it('should return a redirect response to the login page when no session is found with the id at the cookies.', async () => {
      clientServiceMock.findOne.mockResolvedValueOnce(<OAuth2Client>{
        id: 'client_id',
        redirectUris: ['https://example.com/callback'],
        responseTypes: ['code'],
        scopes: ['foo', 'bar'],
      });

      sessionServiceMock.findOne.mockResolvedValueOnce(null);
      sessionServiceMock.create.mockResolvedValueOnce(<OAuth2Session>{ id: 'session_id' });

      const params = new URLSearchParams({ login_challenge: 'session_id' });

      await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
        body: Buffer.alloc(0),
        cookies: { 'unvision:session': 'session_id' },
        headers: { Location: `https://server.example.com/auth/login?${params.toString()}` },
        statusCode: 303,
      });
    });

    it('should return a redirect response to the login page when the session is expired.', async () => {
      clientServiceMock.findOne.mockResolvedValueOnce(<OAuth2Client>{
        id: 'client_id',
        redirectUris: ['https://example.com/callback'],
        responseTypes: ['code'],
        scopes: ['foo', 'bar'],
      });

      sessionServiceMock.findOne.mockResolvedValueOnce(<OAuth2Session>{
        id: 'session_id',
        expiresAt: new Date(Date.now() - 3600000),
      });

      sessionServiceMock.create.mockResolvedValueOnce(<OAuth2Session>{ id: 'session_id' });

      const params = new URLSearchParams({ login_challenge: 'session_id' });

      await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
        body: Buffer.alloc(0),
        cookies: { 'unvision:session': 'session_id' },
        headers: { Location: `https://server.example.com/auth/login?${params.toString()}` },
        statusCode: 303,
      });

      expect(sessionServiceMock.remove).toHaveBeenCalledTimes(1);
    });

    it('should return a redirect response to the login page when the session does not have a user.', async () => {
      clientServiceMock.findOne.mockResolvedValueOnce(<OAuth2Client>{
        id: 'client_id',
        redirectUris: ['https://example.com/callback'],
        responseTypes: ['code'],
        scopes: ['foo', 'bar'],
      });

      sessionServiceMock.findOne.mockResolvedValueOnce(<OAuth2Session>{ id: 'session_id', user: null });
      sessionServiceMock.create.mockResolvedValueOnce(<OAuth2Session>{ id: 'session_id' });

      const params = new URLSearchParams({ login_challenge: 'session_id' });

      await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
        body: Buffer.alloc(0),
        cookies: { 'unvision:session': 'session_id' },
        headers: { Location: `https://server.example.com/auth/login?${params.toString()}` },
        statusCode: 303,
      });

      expect(sessionServiceMock.remove).toHaveBeenCalledTimes(1);
    });

    it('should return an error response when the parameters of the request and session do not match.', async () => {
      request.query.state = 'bad_client_state';

      clientServiceMock.findOne.mockResolvedValueOnce(<OAuth2Client>{
        id: 'client_id',
        redirectUris: ['https://example.com/callback'],
        responseTypes: ['code'],
        scopes: ['foo', 'bar'],
      });

      sessionServiceMock.findOne.mockResolvedValueOnce(<OAuth2Session>{
        id: 'session_id',
        parameters: { ...request.query, state: 'client_state' },
        user: { id: 'user_id' },
      });

      responseModesMocks[0]!.createHttpResponse.mockImplementationOnce((redirectUri, parameters) => {
        const url = new URL(redirectUri);
        url.search = new URLSearchParams(parameters).toString();
        return new Response().redirect(url);
      });

      const error = new InvalidRequestException({
        error_description: 'One or more parameters changed since the initial request.',
      });

      const params = new URLSearchParams(error.toJSON());

      await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
        body: Buffer.alloc(0),
        headers: { Location: `https://example.com/callback?${params.toString()}` },
        statusCode: 303,
      });
    });

    it('should return a redirect response to the consent page when no consent is found at the cookies.', async () => {
      delete request.cookies['unvision:consent'];

      clientServiceMock.findOne.mockResolvedValueOnce(<OAuth2Client>{
        id: 'client_id',
        redirectUris: ['https://example.com/callback'],
        responseTypes: ['code'],
        scopes: ['foo', 'bar'],
      });

      sessionServiceMock.findOne.mockResolvedValueOnce(<OAuth2Session>{
        id: 'session_id',
        parameters: request.query,
        user: { id: 'user_id' },
      });

      consentServiceMock.create.mockResolvedValueOnce(<OAuth2Consent<AuthorizationRequest>>{ id: 'consent_id' });

      const params = new URLSearchParams({ consent_challenge: 'consent_id' });

      await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
        body: Buffer.alloc(0),
        cookies: { 'unvision:consent': 'consent_id' },
        headers: { Location: `https://server.example.com/auth/consent?${params.toString()}` },
        statusCode: 303,
      });
    });

    it('should return a redirect response to the consent page when no consent is found with the id at the cookies.', async () => {
      clientServiceMock.findOne.mockResolvedValueOnce(<OAuth2Client>{
        id: 'client_id',
        redirectUris: ['https://example.com/callback'],
        responseTypes: ['code'],
        scopes: ['foo', 'bar'],
      });

      sessionServiceMock.findOne.mockResolvedValueOnce(<OAuth2Session>{
        id: 'session_id',
        parameters: request.query,
        user: { id: 'user_id' },
      });

      consentServiceMock.findOne.mockResolvedValueOnce(null);
      consentServiceMock.create.mockResolvedValueOnce(<OAuth2Consent<AuthorizationRequest>>{ id: 'consent_id' });

      const params = new URLSearchParams({ consent_challenge: 'consent_id' });

      await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
        body: Buffer.alloc(0),
        cookies: { 'unvision:consent': 'consent_id' },
        headers: { Location: `https://server.example.com/auth/consent?${params.toString()}` },
        statusCode: 303,
      });
    });

    it('should return a redirect response to the consent page when the consent is expired.', async () => {
      clientServiceMock.findOne.mockResolvedValueOnce(<OAuth2Client>{
        id: 'client_id',
        redirectUris: ['https://example.com/callback'],
        responseTypes: ['code'],
        scopes: ['foo', 'bar'],
      });

      sessionServiceMock.findOne.mockResolvedValueOnce(<OAuth2Session>{
        id: 'session_id',
        parameters: request.query,
        user: { id: 'user_id' },
      });

      consentServiceMock.findOne.mockResolvedValueOnce(<OAuth2Consent<AuthorizationRequest>>{
        id: 'consent_id',
        expiresAt: new Date(Date.now() - 3600000),
      });

      consentServiceMock.create.mockResolvedValueOnce(<OAuth2Consent<AuthorizationRequest>>{ id: 'consent_id' });

      const params = new URLSearchParams({ consent_challenge: 'consent_id' });

      await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
        body: Buffer.alloc(0),
        cookies: { 'unvision:consent': 'consent_id' },
        headers: { Location: `https://server.example.com/auth/consent?${params.toString()}` },
        statusCode: 303,
      });

      expect(consentServiceMock.remove).toHaveBeenCalledTimes(1);
    });

    it('should return an error response when the parameters of the request and consent do not match.', async () => {
      request.query.state = 'bad_client_state';

      clientServiceMock.findOne.mockResolvedValueOnce(<OAuth2Client>{
        id: 'client_id',
        redirectUris: ['https://example.com/callback'],
        responseTypes: ['code'],
        scopes: ['foo', 'bar'],
      });

      sessionServiceMock.findOne.mockResolvedValueOnce(<OAuth2Session>{ id: 'session_id', user: { id: 'user_id' } });

      consentServiceMock.findOne.mockResolvedValueOnce(<OAuth2Consent<AuthorizationRequest>>{
        id: 'consent_id',
        parameters: { ...request.query, state: 'client_state' },
      });

      responseModesMocks[0]!.createHttpResponse.mockImplementationOnce((redirectUri, parameters) => {
        const url = new URL(redirectUri);
        url.search = new URLSearchParams(parameters).toString();
        return new Response().redirect(url);
      });

      const error = new InvalidRequestException({
        error_description: 'One or more parameters changed since the initial request.',
      });

      const params = new URLSearchParams(error.toJSON());

      await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
        body: Buffer.alloc(0),
        headers: { Location: `https://example.com/callback?${params.toString()}` },
        statusCode: 303,
      });
    });

    it('should return a valid authorization response.', async () => {
      clientServiceMock.findOne.mockResolvedValueOnce(<OAuth2Client>{
        id: 'client_id',
        redirectUris: ['https://example.com/callback'],
        responseTypes: ['code'],
        scopes: ['foo', 'bar'],
      });

      sessionServiceMock.findOne.mockResolvedValueOnce(<OAuth2Session>{
        id: 'session_id',
        parameters: request.query,
        user: { id: 'user_id' },
      });

      consentServiceMock.findOne.mockResolvedValueOnce(<OAuth2Consent<AuthorizationRequest>>{ id: 'consent_id' });

      responseTypesMocks[0]!.handle.mockResolvedValueOnce({ code: 'code', state: request.query.state });

      responseModesMocks[0]!.createHttpResponse.mockImplementationOnce((redirectUri, parameters) => {
        const url = new URL(redirectUri);
        url.search = new URLSearchParams(parameters).toString();
        return new Response().redirect(url);
      });

      await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
        body: Buffer.alloc(0),
        headers: { Location: 'https://example.com/callback?code=code&state=client_state' },
        statusCode: 303,
      });
    });
  });
});
