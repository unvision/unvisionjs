import { Inject, Injectable, Optional } from '@unvision/di';

import { Buffer } from 'buffer';
import { timingSafeEqual } from 'crypto';
import { OutgoingHttpHeaders } from 'http';

import { AuthorizationServerOptions } from '../authorization-server/options/authorization-server.options';
import { OAuth2AccessToken } from '../entities/oauth2-access-token.entity';
import { OAuth2Client } from '../entities/oauth2-client.entity';
import { OAuth2RefreshToken } from '../entities/oauth2-refresh-token.entity';
import { InvalidRequestException } from '../exceptions/invalid-request.exception';
import { OAuth2Exception } from '../exceptions/oauth2.exception';
import { ServerErrorException } from '../exceptions/server-error.exception';
import { UnsupportedTokenTypeException } from '../exceptions/unsupported-token-type.exception';
import { ClientAuthenticationHandler } from '../handlers/client-authentication.handler';
import { Request } from '../http/request';
import { Response } from '../http/response';
import { ACCESS_TOKEN_SERVICE, AUTHORIZATION_SERVER_OPTIONS, REFRESH_TOKEN_SERVICE } from '../metadata/metadata.keys';
import { OAuth2AccessTokenService } from '../services/oauth2-access-token.service';
import { OAuth2RefreshTokenService } from '../services/oauth2-refresh-token.service';
import { IntrospectionRequest } from '../types/introspection-request';
import { IntrospectionResponse } from '../types/introspection-response';
import { Endpoint } from './endpoint';

interface FindTokenResult {
  readonly entity: OAuth2AccessToken | OAuth2RefreshToken;
  readonly tokenType: string;
}

/**
 * Implementation of the **Introspection** Endpoint.
 *
 * This endpoint is used by the Client to obtain information about a Token in its possession.
 *
 * If the Client succeeds to authenticate but provides a Token that was not issued to itself, is invalid,
 * does not exist within the Authorization Server, or is otherwise unknown or invalid, it will return
 * a standard response of the format `{"active": false}`.
 *
 * If every verification step passes, then the Authorization Server returns the information
 * associated to the Token back to the Client.
 *
 * @see https://www.rfc-editor.org/rfc/rfc7662.html
 */
@Injectable()
export class IntrospectionEndpoint implements Endpoint {
  /**
   * Inactive Token Standard Response.
   */
  private static readonly INACTIVE_TOKEN: IntrospectionResponse = { active: false };

  /**
   * Name of the Endpoint.
   */
  public readonly name: string = 'introspection';

  /**
   * Path of the Endpoint.
   */
  public readonly path: string = '/oauth/introspect';

  /**
   * Http Methods supported by the Endpoint.
   */
  public readonly httpMethods: string[] = ['post'];

  /**
   * Default HTTP Headers to be included in the Response.
   */
  private readonly headers: OutgoingHttpHeaders = { 'Cache-Control': 'no-store', Pragma: 'no-cache' };

  /**
   * Token Type Hints supported by the Introspection Endpoint.
   */
  private readonly supportedTokenTypeHints: string[] = ['access_token'];

  /**
   * Instantiates a new Introspection Endpoint.
   *
   * @param clientAuthenticationHandler Instance of the Client Authentication Handler.
   * @param authorizationServerOptions Configuration Parameters of the Authorization Server.
   * @param accessTokenService Instance of the Access Token Service.
   * @param refreshTokenService Instance of the Refresh Token Service.
   */
  public constructor(
    private readonly clientAuthenticationHandler: ClientAuthenticationHandler,
    @Inject(AUTHORIZATION_SERVER_OPTIONS) private readonly authorizationServerOptions: AuthorizationServerOptions,
    @Inject(ACCESS_TOKEN_SERVICE) private readonly accessTokenService: OAuth2AccessTokenService,
    @Optional() @Inject(REFRESH_TOKEN_SERVICE) private readonly refreshTokenService?: OAuth2RefreshTokenService
  ) {
    if (this.authorizationServerOptions.enableRefreshTokenIntrospection) {
      if (this.refreshTokenService === undefined) {
        throw new Error('Cannot enable Refresh Token Introspection without a Refresh Token Service.');
      }

      this.supportedTokenTypeHints.push('refresh_token');
    }
  }

  /**
   * Introspects the provided Token about its metadata and state within the Authorization Server.
   *
   * First it validates the  Request of the Client by making sure the required parameter **token** is present,
   * and that the Client can authenticate within the Endpoint.
   *
   * It then tries to lookup the information about the Token from the application's storage.
   *
   * If the Client passes the authentication, the token is still valid, and the Client is the owner of the token,
   * this method will return the Token's metadata back to the Client.
   *
   * If it is determined that the Client should not have access to the Token's metadata, or if the Token
   * is not valid anymore, this method will return an Introspection Response in the format `{"active": false}`.
   *
   * This is done in order to prevent a Client from fishing any information that it should not have access to.
   *
   * @param request HTTP Request.
   * @returns HTTP Response.
   */
  public async handle(request: Request): Promise<Response> {
    const parameters = <IntrospectionRequest>request.body;

    try {
      this.checkParameters(parameters);

      const client = await this.clientAuthenticationHandler.authenticate(request);
      const introspectionResponse = await this.introspectToken(parameters, client);

      return new Response().setHeaders(this.headers).json(introspectionResponse);
    } catch (exc: any) {
      const error =
        exc instanceof OAuth2Exception
          ? exc
          : new ServerErrorException({ error_description: 'An unexpected error occurred.' }, exc);

      return new Response()
        .setStatus(error.statusCode)
        .setHeaders(error.headers)
        .setHeaders(this.headers)
        .json(error.toJSON());
    }
  }

  /**
   * Checks if the Parameters of the Introspection Request are valid.
   *
   * @param parameters Parameters of the Introspection Request.
   */
  protected checkParameters(parameters: IntrospectionRequest): void {
    const { token, token_type_hint: tokenTypeHint } = parameters;

    if (typeof token !== 'string') {
      throw new InvalidRequestException({ error_description: 'Invalid parameter "token".' });
    }

    if (tokenTypeHint !== undefined && !this.supportedTokenTypeHints.includes(tokenTypeHint)) {
      throw new UnsupportedTokenTypeException({ error_description: `Unsupported token_type_hint "${tokenTypeHint}".` });
    }
  }

  /**
   * Introspects the provide Token for its metadata.
   *
   * @param parameters Parameters of the Introspection Request.
   * @param client Authenticated Client.
   * @returns Metadata of the Token.
   */
  private async introspectToken(
    parameters: IntrospectionRequest,
    client: OAuth2Client
  ): Promise<IntrospectionResponse> {
    const { token, token_type_hint: tokenTypeHint } = parameters;

    const { entity, tokenType } = (await this.findTokenEntity(token, tokenTypeHint)) ?? {};

    if (entity === undefined || tokenType === undefined) {
      return IntrospectionEndpoint.INACTIVE_TOKEN;
    }

    const clientId = Buffer.from(client.id, 'utf8');
    const tokenClientId = Buffer.from(entity.client.id, 'utf8');

    if (clientId.length !== tokenClientId.length || !timingSafeEqual(clientId, tokenClientId)) {
      return IntrospectionEndpoint.INACTIVE_TOKEN;
    }

    return this.getTokenMetadata(entity);
  }

  /**
   * Searches the application's storage for a Token Entity that satisfies the Token provided by the Client.
   *
   * @param token Token provided by the Client.
   * @param tokenTypeHint Optional hint about the type of the Token.
   * @returns Resulting Token Entity and its type.
   */
  private async findTokenEntity(token: string, tokenTypeHint?: string): Promise<FindTokenResult | null> {
    switch (tokenTypeHint) {
      case 'access_token':
        return (await this.findAccessToken(token)) ?? (await this.findRefreshToken(token));

      case 'refresh_token':
      default:
        return (await this.findRefreshToken(token)) ?? (await this.findAccessToken(token));
    }
  }

  /**
   * Searches the application's storage for an Access Token.
   *
   * @param token Token provided by the Client.
   * @returns Result of the search.
   */
  private async findAccessToken(token: string): Promise<FindTokenResult | null> {
    const entity = await this.accessTokenService.findOne(token);
    return entity !== null ? { entity, tokenType: 'access_token' } : null;
  }

  /**
   * Searches the application's storage for a Refresh Token.
   *
   * @param token Token provided by the Client.
   * @returns Result of the search.
   */
  private async findRefreshToken(token: string): Promise<FindTokenResult | null> {
    if (!this.authorizationServerOptions.enableRefreshTokenIntrospection) {
      return null;
    }

    const entity = await this.refreshTokenService!.findOne(token);

    return entity !== null ? { entity, tokenType: 'refresh_token' } : null;
  }

  /**
   * Returns the metadata of the provided Token Entity.
   *
   * @param token Token Entity to be introspected.
   * @returns Metadata of the provided Token Entity.
   */
  private getTokenMetadata(token: OAuth2AccessToken | OAuth2RefreshToken): IntrospectionResponse {
    if (token.isRevoked || new Date() < token.validAfter || new Date() >= token.expiresAt) {
      return IntrospectionEndpoint.INACTIVE_TOKEN;
    }

    // TODO: Add check for username and jti.
    // TODO: Add policy to restrict or add parameters.
    return {
      active: true,
      scope: token.scopes.join(' '),
      client_id: token.client.id,
      username: undefined,
      token_type: 'Bearer',
      exp: Math.ceil(token.expiresAt.getTime() / 1000),
      iat: Math.ceil(token.issuedAt.getTime() / 1000),
      nbf: Math.ceil(token.validAfter.getTime() / 1000),
      sub: token.user?.id,
      aud: token.client.id,
      iss: this.authorizationServerOptions.issuer,
      jti: undefined,
    };
  }
}
