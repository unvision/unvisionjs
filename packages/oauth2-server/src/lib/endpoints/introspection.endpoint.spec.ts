import { DependencyInjectionContainer } from '@unvision/di';

import { Buffer } from 'buffer';
import { OutgoingHttpHeaders } from 'http';

import { AuthorizationServerOptions } from '../authorization-server/options/authorization-server.options';
import { OAuth2AccessToken } from '../entities/oauth2-access-token.entity';
import { OAuth2Client } from '../entities/oauth2-client.entity';
import { InvalidClientException } from '../exceptions/invalid-client.exception';
import { InvalidRequestException } from '../exceptions/invalid-request.exception';
import { UnsupportedTokenTypeException } from '../exceptions/unsupported-token-type.exception';
import { ClientAuthenticationHandler } from '../handlers/client-authentication.handler';
import { Request } from '../http/request';
import { Response } from '../http/response';
import { ACCESS_TOKEN_SERVICE, AUTHORIZATION_SERVER_OPTIONS, REFRESH_TOKEN_SERVICE } from '../metadata/metadata.keys';
import { OAuth2AccessTokenService } from '../services/oauth2-access-token.service';
import { OAuth2RefreshTokenService } from '../services/oauth2-refresh-token.service';
import { IntrospectionResponse } from '../types/introspection-response';
import { IntrospectionEndpoint } from './introspection.endpoint';

jest.mock('../handlers/client-authentication.handler');

describe('Introspection Endpoint', () => {
  let endpoint: IntrospectionEndpoint;

  const accessTokenServiceMock = jest.mocked<OAuth2AccessTokenService>({
    create: jest.fn(),
    findOne: jest.fn(),
    revoke: jest.fn(),
  });

  const refreshTokenServiceMock = jest.mocked<OAuth2RefreshTokenService>({
    create: jest.fn(),
    findOne: jest.fn(),
    revoke: jest.fn(),
  });

  const clientAuthenticationHandlerMock = jest.mocked(ClientAuthenticationHandler.prototype, true);

  const authorizationServerOptions = <AuthorizationServerOptions>{
    issuer: 'https://server.example.com',
    enableRefreshTokenIntrospection: true,
  };

  beforeEach(() => {
    const container = new DependencyInjectionContainer();

    container.bind<AuthorizationServerOptions>(AUTHORIZATION_SERVER_OPTIONS).toValue(authorizationServerOptions);
    container.bind<OAuth2AccessTokenService>(ACCESS_TOKEN_SERVICE).toValue(accessTokenServiceMock);
    container.bind<OAuth2RefreshTokenService>(REFRESH_TOKEN_SERVICE).toValue(refreshTokenServiceMock);
    container.bind(ClientAuthenticationHandler).toValue(clientAuthenticationHandlerMock);
    container.bind(IntrospectionEndpoint).toSelf().asSingleton();

    endpoint = container.resolve(IntrospectionEndpoint);
  });

  describe('name', () => {
    it('should have "introspection" as its name.', () => {
      expect(endpoint.name).toBe('introspection');
    });
  });

  describe('path', () => {
    it('should have "/oauth/introspect" as its default path.', () => {
      expect(endpoint.path).toBe('/oauth/introspect');
    });
  });

  describe('httpMethods', () => {
    it('should have \'["post"]\' as its default path.', () => {
      expect(endpoint.httpMethods).toStrictEqual(['post']);
    });
  });

  describe('headers', () => {
    it('should have a default "headers" object for the http response.', () => {
      expect(endpoint['headers']).toMatchObject<OutgoingHttpHeaders>({
        'Cache-Control': 'no-store',
        Pragma: 'no-cache',
      });
    });
  });

  describe('supportedTokenTypeHints', () => {
    it('should have only the type "access_token" when not supporting refresh token introspection.', () => {
      const opts = <AuthorizationServerOptions>{ enableRefreshTokenIntrospection: false };
      const endpoint = new IntrospectionEndpoint(<any>{}, opts, <any>{}, <any>{});

      expect(endpoint['supportedTokenTypeHints']).toEqual(['access_token']);
    });

    it('should have the types ["access_token", "refresh_token"] when supporting refresh token introspection.', () => {
      const opts = <AuthorizationServerOptions>{ enableRefreshTokenIntrospection: true };
      const endpoint = new IntrospectionEndpoint(<any>{}, opts, <any>{}, <any>{});

      expect(endpoint['supportedTokenTypeHints']).toEqual(['access_token', 'refresh_token']);
    });
  });

  describe('constructor', () => {
    it('should reject when enabling refresh token introspection without a refresh token service.', () => {
      const opts = <AuthorizationServerOptions>{ enableRefreshTokenIntrospection: true };
      expect(() => new IntrospectionEndpoint(<any>{}, opts, <any>{})).toThrow();
    });
  });

  describe('handle()', () => {
    let request: Request;

    const defaultResponse = new Response()
      .setHeaders({ 'Content-Type': 'application/json', 'Cache-Control': 'no-store', Pragma: 'no-cache' })
      .json({ active: false });

    beforeEach(() => {
      request = {
        body: { token: 'access_token' },
        cookies: {},
        headers: { authorization: 'Basic ' + Buffer.from('client_id:client_secret', 'utf8').toString('base64') },
        method: 'post',
        path: '/oauth/introspect',
        query: {},
      };
    });

    it('should reject not providing a "token" parameter.', async () => {
      delete request.body.token;

      const error = new InvalidRequestException({ error_description: 'Invalid parameter "token".' });

      await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
        body: Buffer.from(JSON.stringify(error.toJSON()), 'utf8'),
        headers: { 'Content-Type': 'application/json', ...endpoint['headers'] },
        statusCode: 400,
      });
    });

    it('should reject providing an unsupported "token_type_hint".', async () => {
      request.body.token_type_hint = 'unknown';

      const error = new UnsupportedTokenTypeException({ error_description: 'Unsupported token_type_hint "unknown".' });

      await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
        body: Buffer.from(JSON.stringify(error.toJSON()), 'utf8'),
        headers: { 'Content-Type': 'application/json', ...endpoint['headers'] },
        statusCode: 400,
      });
    });

    it('should reject not using a client authentication method.', async () => {
      delete request.headers.authorization;

      const error = new InvalidClientException({ error_description: 'No Client Authentication Method detected.' });

      clientAuthenticationHandlerMock.authenticate.mockRejectedValueOnce(error);

      await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
        body: Buffer.from(JSON.stringify(error.toJSON()), 'utf8'),
        headers: { 'Content-Type': 'application/json', ...endpoint['headers'] },
        statusCode: error.statusCode,
      });
    });

    it('should reject using multiple client authentication methods.', async () => {
      Object.assign(request.body, { client_id: 'client_id', client_secret: 'client_secret' });

      const error = new InvalidClientException({
        error_description: 'Multiple Client Authentication Methods detected.',
      });

      clientAuthenticationHandlerMock.authenticate.mockRejectedValueOnce(error);

      await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
        body: Buffer.from(JSON.stringify(error.toJSON()), 'utf8'),
        headers: { 'Content-Type': 'application/json', ...endpoint['headers'] },
        statusCode: error.statusCode,
      });
    });

    it("should reject when the provided secret does not match the client's one.", async () => {
      const error = new InvalidClientException({ error_description: 'Invalid Credentials.' }).setHeaders({
        'WWW-Authenticate': 'Basic',
      });

      clientAuthenticationHandlerMock.authenticate.mockRejectedValueOnce(error);

      await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
        body: Buffer.from(JSON.stringify(error.toJSON()), 'utf8'),
        headers: { 'Content-Type': 'application/json', ...endpoint['headers'], ...error.headers },
        statusCode: error.statusCode,
      });
    });

    it('should search for an access token and then a refresh token when providing an "access_token" token_type_hint.', async () => {
      request.body.token_type_hint = 'access_token';

      clientAuthenticationHandlerMock.authenticate.mockResolvedValueOnce(<OAuth2Client>{ id: 'client_id' });

      accessTokenServiceMock.findOne.mockResolvedValueOnce(null);
      refreshTokenServiceMock.findOne.mockResolvedValueOnce(null);

      await expect(endpoint.handle(request)).resolves.toMatchObject(defaultResponse);

      expect(accessTokenServiceMock.findOne).toHaveBeenCalledTimes(1);
      expect(refreshTokenServiceMock.findOne).toHaveBeenCalledTimes(1);

      const findAccessTokenOrder = accessTokenServiceMock.findOne.mock.invocationCallOrder[0];
      const findRefreshTokenOrder = refreshTokenServiceMock.findOne.mock.invocationCallOrder[0];

      expect(findAccessTokenOrder).toBeLessThan(<number>findRefreshTokenOrder);
    });

    it('should search for a refresh token and then an access token when providing a "refresh_token" token_type_hint.', async () => {
      request.body.token_type_hint = 'refresh_token';

      clientAuthenticationHandlerMock.authenticate.mockResolvedValueOnce(<OAuth2Client>{ id: 'client_id' });

      accessTokenServiceMock.findOne.mockResolvedValueOnce(null);
      refreshTokenServiceMock.findOne.mockResolvedValueOnce(null);

      await expect(endpoint.handle(request)).resolves.toMatchObject(defaultResponse);

      expect(accessTokenServiceMock.findOne).toHaveBeenCalledTimes(1);
      expect(refreshTokenServiceMock.findOne).toHaveBeenCalledTimes(1);

      const findAccessTokenOrder = accessTokenServiceMock.findOne.mock.invocationCallOrder[0];
      const findRefreshTokenOrder = refreshTokenServiceMock.findOne.mock.invocationCallOrder[0];

      expect(findRefreshTokenOrder).toBeLessThan(<number>findAccessTokenOrder);
    });

    it('should search for a refresh token and then an access token when not providing a token_type_hint.', async () => {
      clientAuthenticationHandlerMock.authenticate.mockResolvedValueOnce(<OAuth2Client>{ id: 'client_id' });

      accessTokenServiceMock.findOne.mockResolvedValueOnce(null);
      refreshTokenServiceMock.findOne.mockResolvedValueOnce(null);

      await expect(endpoint.handle(request)).resolves.toMatchObject(defaultResponse);

      expect(accessTokenServiceMock.findOne).toHaveBeenCalledTimes(1);
      expect(refreshTokenServiceMock.findOne).toHaveBeenCalledTimes(1);

      const findAccessTokenOrder = accessTokenServiceMock.findOne.mock.invocationCallOrder[0];
      const findRefreshTokenOrder = refreshTokenServiceMock.findOne.mock.invocationCallOrder[0];

      expect(findRefreshTokenOrder).toBeLessThan(<number>findAccessTokenOrder);
    });

    it('should return an inactive token response when the authorization server does not support refresh token introspection.', async () => {
      Reflect.set(authorizationServerOptions, 'enableRefreshTokenIntrospection', false);

      request.body.token = 'refresh_token';

      clientAuthenticationHandlerMock.authenticate.mockResolvedValueOnce(<OAuth2Client>{ id: 'client_id' });

      accessTokenServiceMock.findOne.mockResolvedValueOnce(null);
      refreshTokenServiceMock.findOne.mockResolvedValueOnce(null);

      await expect(endpoint.handle(request)).resolves.toMatchObject(defaultResponse);

      Reflect.set(authorizationServerOptions, 'enableRefreshTokenIntrospection', true);
    });

    it('should return an inactive token response when the client is not the owner of the token.', async () => {
      clientAuthenticationHandlerMock.authenticate.mockResolvedValueOnce(<OAuth2Client>{ id: 'client_id' });

      accessTokenServiceMock.findOne.mockResolvedValueOnce(<OAuth2AccessToken>{
        token: 'access_token',
        isRevoked: true,
        client: { id: 'another_client_id' },
      });

      refreshTokenServiceMock.findOne.mockResolvedValueOnce(null);

      await expect(endpoint.handle(request)).resolves.toMatchObject(defaultResponse);
    });

    it('should return an inactive token response when the token is revoked.', async () => {
      clientAuthenticationHandlerMock.authenticate.mockResolvedValueOnce(<OAuth2Client>{ id: 'client_id' });

      accessTokenServiceMock.findOne.mockResolvedValueOnce(<OAuth2AccessToken>{
        token: 'access_token',
        isRevoked: true,
        client: { id: 'client_id' },
      });

      refreshTokenServiceMock.findOne.mockResolvedValueOnce(null);

      await expect(endpoint.handle(request)).resolves.toMatchObject(defaultResponse);
    });

    it('should return an inactive token response when the token is not yet valid.', async () => {
      clientAuthenticationHandlerMock.authenticate.mockResolvedValueOnce(<OAuth2Client>{ id: 'client_id' });

      accessTokenServiceMock.findOne.mockResolvedValueOnce(<OAuth2AccessToken>{
        token: 'access_token',
        isRevoked: false,
        validAfter: new Date(Date.now() + 3600000),
        client: { id: 'client_id' },
      });

      refreshTokenServiceMock.findOne.mockResolvedValueOnce(null);

      await expect(endpoint.handle(request)).resolves.toMatchObject(defaultResponse);
    });

    it('should return an inactive token response when the token is expired.', async () => {
      clientAuthenticationHandlerMock.authenticate.mockResolvedValueOnce(<OAuth2Client>{ id: 'client_id' });

      accessTokenServiceMock.findOne.mockResolvedValueOnce(<OAuth2AccessToken>{
        token: 'access_token',
        isRevoked: false,
        validAfter: new Date(Date.now() - 7200000),
        expiresAt: new Date(Date.now() - 3600000),
        client: { id: 'client_id' },
      });

      refreshTokenServiceMock.findOne.mockResolvedValueOnce(null);

      await expect(endpoint.handle(request)).resolves.toMatchObject(defaultResponse);
    });

    it('should return the metadata of the requested token.', async () => {
      const now = Date.now();

      clientAuthenticationHandlerMock.authenticate.mockResolvedValueOnce(<OAuth2Client>{ id: 'client_id' });

      accessTokenServiceMock.findOne.mockResolvedValueOnce(<OAuth2AccessToken>{
        token: 'access_token',
        scopes: ['foo', 'bar'],
        isRevoked: false,
        issuedAt: new Date(now - 3600000),
        validAfter: new Date(now - 3600000),
        expiresAt: new Date(now + 3600000),
        client: { id: 'client_id' },
        user: { id: 'user_id' },
      });

      refreshTokenServiceMock.findOne.mockResolvedValueOnce(null);

      const introspectionResponse = <IntrospectionResponse>{
        active: true,
        scope: 'foo bar',
        client_id: 'client_id',
        username: undefined,
        token_type: 'Bearer',
        exp: Math.ceil((now + 3600000) / 1000),
        iat: Math.ceil((now - 3600000) / 1000),
        nbf: Math.ceil((now - 3600000) / 1000),
        sub: 'user_id',
        aud: 'client_id',
        iss: authorizationServerOptions.issuer,
        jti: undefined,
      };

      const response = await endpoint.handle(request);

      expect(response).toMatchObject<Partial<Response>>({
        body: Buffer.from(JSON.stringify(introspectionResponse), 'utf8'),
        headers: { 'Content-Type': 'application/json', ...endpoint['headers'] },
        statusCode: 200,
      });
    });
  });
});
