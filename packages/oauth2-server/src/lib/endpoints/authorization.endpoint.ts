import { Inject, Injectable, InjectAll } from '@unvision/di';

import { URL, URLSearchParams } from 'url';
import { isDeepStrictEqual } from 'util';

import { AuthorizationServerOptions } from '../authorization-server/options/authorization-server.options';
import { OAuth2Client } from '../entities/oauth2-client.entity';
import { OAuth2Consent } from '../entities/oauth2-consent.entity';
import { OAuth2Session } from '../entities/oauth2-session.entity';
import { AccessDeniedException } from '../exceptions/access-denied.exception';
import { InvalidClientException } from '../exceptions/invalid-client.exception';
import { InvalidRequestException } from '../exceptions/invalid-request.exception';
import { OAuth2Exception } from '../exceptions/oauth2.exception';
import { ServerErrorException } from '../exceptions/server-error.exception';
import { UnauthorizedClientException } from '../exceptions/unauthorized-client.exception';
import { UnsupportedResponseTypeException } from '../exceptions/unsupported-response-type.exception';
import { ScopeHandler } from '../handlers/scope.handler';
import { Request } from '../http/request';
import { Response } from '../http/response';
import {
  AUTHORIZATION_SERVER_OPTIONS,
  CLIENT_SERVICE,
  CONSENT_SERVICE,
  RESPONSE_MODE,
  RESPONSE_TYPE,
  SESSION_SERVICE,
} from '../metadata/metadata.keys';
import { ResponseMode } from '../response-modes/response-mode';
import { ResponseType } from '../response-types/response-type';
import { OAuth2ClientService } from '../services/oauth2-client.service';
import { OAuth2ConsentService } from '../services/oauth2-consent.service';
import { OAuth2SessionService } from '../services/oauth2-session.service';
import { AuthorizationRequest } from '../types/authorization-request';
import { Endpoint } from './endpoint';

/**
 * Implementation of the **Authorization** Endpoint.
 *
 * This endpoint is used to provide an Authorization Grant for the requesting Client on behalf of the End User.
 *
 * Since the OAuth 2.0 Spec does not define the need for authentication when using this endpoint, it is left omitted.
 *
 * @see https://www.rfc-editor.org/rfc/rfc6749.html#section-3.1
 */
@Injectable()
export class AuthorizationEndpoint implements Endpoint {
  /**
   * Name of the Endpoint.
   */
  public readonly name: string = 'authorization';

  /**
   * Path of the Endpoint.
   */
  public readonly path: string = '/oauth/authorize';

  /**
   * Http methods supported by the Endpoint.
   */
  public readonly httpMethods: string[] = ['get'];

  /**
   * Url of the Error Page.
   */
  private readonly consentUrl: string;

  /**
   * URL of the Error Page.
   */
  private readonly errorUrl: string;

  /**
   * URL of the Error Page.
   */
  private readonly loginUrl: string;

  /**
   * Instantiates a new Authorization Endpoint.
   *
   * @param scopeHandler Instance of the Scope Handler.
   * @param responseTypes Response Types registered at the Authorization Server.
   * @param responseModes Response Modes registered at the Authorization Server.
   * @param authorizationServerOptions Configuration Parameters of the Authorization Server.
   * @param clientService Instance of the Client Service.
   * @param sessionService Instance of the Session Service.
   * @param consentService Instance of the Consent Service.
   */
  public constructor(
    private readonly scopeHandler: ScopeHandler,
    @InjectAll(RESPONSE_TYPE) private readonly responseTypes: ResponseType[],
    @InjectAll(RESPONSE_MODE) private readonly responseModes: ResponseMode[],
    @Inject(AUTHORIZATION_SERVER_OPTIONS) private readonly authorizationServerOptions: AuthorizationServerOptions,
    @Inject(CLIENT_SERVICE) private readonly clientService: OAuth2ClientService,
    @Inject(SESSION_SERVICE) private readonly sessionService: OAuth2SessionService,
    @Inject(CONSENT_SERVICE) private readonly consentService: OAuth2ConsentService
  ) {
    if (this.authorizationServerOptions.userInteraction === undefined) {
      throw new TypeError('Missing User Interaction options.');
    }

    const {
      issuer,
      userInteraction: { consentUrl, errorUrl, loginUrl },
    } = this.authorizationServerOptions;

    this.consentUrl = new URL(consentUrl, issuer).href;
    this.errorUrl = new URL(errorUrl, issuer).href;
    this.loginUrl = new URL(loginUrl, issuer).href;
  }

  /**
   * Creates a Http Redirect Authorization Response.
   *
   * Any error is safely redirected to the Redirect URI provided by the Client in the Authorization Request,
   * or to the Authorization Server's Error Endpoint, should the error not be returned to the Client's Redirect URI.
   *
   * If the authorization flow of the grant results in a successful response, it will redirect the User-Agent
   * to the Redirect URI provided by the Client.
   *
   * This method **REQUIRES** consent given by the User, be it implicit or explicit.
   *
   * @param request Http Request.
   * @returns Http Response.
   */
  public async handle(request: Request): Promise<Response> {
    const parameters = <AuthorizationRequest>request.query;

    let client: OAuth2Client;
    let responseType: ResponseType;
    let responseMode: ResponseMode;

    try {
      this.checkParameters(parameters);

      client = await this.getClient(parameters.client_id);
      responseType = this.getResponseType(parameters.response_type);

      this.checkClientResponseType(client, responseType);
      this.checkClientRedirectUri(client, parameters.redirect_uri);
      this.checkClientScope(client, parameters.scope);

      responseMode = this.getResponseMode(parameters.response_mode ?? responseType.defaultResponseMode);
    } catch (exc: any) {
      const error =
        exc instanceof OAuth2Exception
          ? exc
          : new ServerErrorException({ error_description: 'An unexpected error occurred.' }, exc);

      return this.handleFatalAuthorizationError(error);
    }

    try {
      const { cookies } = request;

      const session = await this.findSession(cookies);

      if (session === null) {
        return this.redirectToLoginPage(parameters, client);
      }

      this.checkSessionParameters(parameters, session.parameters);

      const consent = await this.findConsent(cookies);

      if (consent === null) {
        return this.redirectToConsentPage(parameters, session);
      }

      this.checkConsentParameters(parameters, session.parameters);

      const authorizationResponse = await responseType.handle(consent);

      return responseMode.createHttpResponse(parameters.redirect_uri, authorizationResponse);
    } catch (exc: any) {
      const error =
        exc instanceof OAuth2Exception
          ? exc
          : new ServerErrorException({ error_description: 'An unexpected error occurred.' }, exc);

      return responseMode.createHttpResponse(parameters.redirect_uri, error.toJSON());
    }
  }

  /**
   * Checks if the Parameters of the Authorization Request are valid.
   *
   * @param parameters Parameters of the Authorization Request.
   */
  private checkParameters(parameters: AuthorizationRequest): void {
    const { response_type: responseType, client_id: clientId, redirect_uri: redirectUri, scope } = parameters;

    if (typeof responseType !== 'string') {
      throw new InvalidRequestException({ error_description: 'Invalid parameter "response_type".' });
    }

    if (typeof clientId !== 'string') {
      throw new InvalidRequestException({ error_description: 'Invalid parameter "client_id".' });
    }

    if (typeof redirectUri !== 'string') {
      throw new InvalidRequestException({ error_description: 'Invalid parameter "redirect_uri".' });
    }

    if (typeof scope !== 'string') {
      throw new InvalidRequestException({ error_description: 'Invalid parameter "scope".' });
    }
  }

  /**
   * Fetches a Client from the application's storage based on the provided Client Identifier.
   *
   * @param clientId Identifier of the Client.
   * @returns Client based on the provided Client Identifier.
   */
  private async getClient(clientId: string): Promise<OAuth2Client> {
    const client = await this.clientService.findOne(clientId);

    if (client === null) {
      throw new InvalidClientException({ error_description: 'Invalid Client.' });
    }

    return client;
  }

  /**
   * Retrieves the Response Type based on the **response_type** requested by the Client.
   *
   * @param name Response Type requested by the Client.
   * @returns Response Type.
   */
  private getResponseType(name: string): ResponseType {
    const responseType = this.responseTypes.find((responseType) => responseType.name === name);

    if (responseType === undefined) {
      throw new UnsupportedResponseTypeException({ error_description: `Unsupported response_type "${name}".` });
    }

    return responseType;
  }

  /**
   * Checks if the Client is allowed to use the requested Response Type.
   *
   * @param client Client of the Request.
   * @param responseType Response Type requested by the Client.
   */
  private checkClientResponseType(client: OAuth2Client, responseType: ResponseType): void {
    if (!client.responseTypes.includes(responseType.name)) {
      throw new UnauthorizedClientException({
        error_description: `This Client is not allowed to request the response_type "${responseType.name}".`,
      });
    }
  }

  /**
   * Checks the provided Redirect URI against the registered Redirect URIs of the Client.
   *
   * @param client Client of the Request.
   * @param redirectUri Redirect URI provided by the Client.
   */
  private checkClientRedirectUri(client: OAuth2Client, redirectUri: string): void {
    if (!client.redirectUris.includes(redirectUri)) {
      throw new AccessDeniedException({ error_description: 'Invalid Redirect URI.' });
    }
  }

  /**
   * Checks if the provided scope is supported by the Authorization Server and if the Client is allowed to request it.
   *
   * @param client Client of the Request.
   * @param scope Scope requested by the Client.
   */
  private checkClientScope(client: OAuth2Client, scope: string): void {
    this.scopeHandler.checkRequestedScope(scope);

    scope.split(' ').forEach((requestedScope) => {
      if (!client.scopes.includes(requestedScope)) {
        throw new AccessDeniedException({
          error_description: `The Client is not allowed to request the scope "${requestedScope}".`,
        });
      }
    });
  }

  /**
   * Retrieves the Response Mode based on the **response_mode** requested by the Client.
   *
   * @param name Response Mode requested by the Client.
   * @returns Response Mode.
   */
  private getResponseMode(name: string): ResponseMode {
    const responseMode = this.responseModes.find((responseMode) => responseMode.name === name);

    if (responseMode === undefined) {
      throw new InvalidRequestException({ error_description: `Unsupported response_mode "${name}".` });
    }

    return responseMode;
  }

  /**
   * Handles a fatal OAuth 2.0 Authorization Error - that is, an error that has to be redirected
   * to the Authorization Server's Error Page instead of the Client's Redirect URI.
   *
   * @param error OAuth 2.0 Exception.
   * @returns Http Response.
   */
  private handleFatalAuthorizationError(error: OAuth2Exception): Response {
    const url = new URL(this.errorUrl);
    const parameters = new URLSearchParams(error.toJSON());

    url.search = parameters.toString();

    return new Response().redirect(url.href);
  }

  /**
   * Searches the application's storage for a Session based on the Identifier in the Cookies of the Http Request.
   *
   * @param cookies Cookies of the Http Request.
   * @returns Session based on the Cookies.
   */
  private async findSession(cookies: Record<string, any>): Promise<OAuth2Session | null> {
    const sessionId: string | undefined = cookies['unvision:session'];

    if (sessionId === undefined) {
      return null;
    }

    const session = await this.sessionService.findOne(sessionId);

    if (session === null) {
      return null;
    }

    if (session.expiresAt != null && new Date() > session.expiresAt) {
      await this.sessionService.remove(session);
      return null;
    }

    if (session.user === null) {
      await this.sessionService.remove(session);
      return null;
    }

    return session;
  }

  /**
   * Redirects the User-Agent to the Authorization Server's Login Page for it to authenticate the User.
   *
   * @param parameters Parameters of the Authorization Request.
   * @param client Client of the Request.
   * @returns Http Redirect Response to the Login Page.
   */
  private async redirectToLoginPage(parameters: AuthorizationRequest, client: OAuth2Client): Promise<Response> {
    const session = await this.sessionService.create(parameters, client);

    const redirectUrl = new URL(this.loginUrl);
    const searchParams = new URLSearchParams({ login_challenge: session.id });

    redirectUrl.search = searchParams.toString();

    return new Response().setCookie('unvision:session', session.id).redirect(redirectUrl);
  }

  /**
   * Checks if the parameters of the current authorization request matches the parameters
   * of the original authorization request.
   *
   * @param requestParameters Parameters of the current Authorization Request.
   * @param sessionParameters Parameters of the original Authorization Request.
   */
  private checkSessionParameters(
    requestParameters: AuthorizationRequest,
    sessionParameters: AuthorizationRequest
  ): void {
    if (!isDeepStrictEqual(requestParameters, sessionParameters)) {
      throw new InvalidRequestException({
        error_description: 'One or more parameters changed since the initial request.',
      });
    }
  }

  /**
   * Searches the application's storage for a Consent based on the Identifier in the Cookies of the Http Request.
   *
   * @param cookies Cookies of the Http Request.
   * @returns Consent based on the Cookies.
   */
  private async findConsent(cookies: Record<string, any>): Promise<OAuth2Consent<AuthorizationRequest> | null> {
    const consentId: string | undefined = cookies['unvision:consent'];

    if (consentId === undefined) {
      return null;
    }

    const consent = await this.consentService.findOne(consentId);

    if (consent === null) {
      return null;
    }

    if (consent.expiresAt != null && new Date() > consent.expiresAt) {
      await this.consentService.remove(consent);
      return null;
    }

    return consent;
  }

  /**
   * Redirects the User-Agent to the Authorization Server's Consent Page for it to authenticate the User.
   *
   * @param parameters Parameters of the Authorization Request.
   * @param session Session of the Request.
   * @returns Http Redirect Response to the Consent Page.
   */
  private async redirectToConsentPage(parameters: AuthorizationRequest, session: OAuth2Session): Promise<Response> {
    const consent = await this.consentService.create(parameters, session);

    const redirectUrl = new URL(this.consentUrl);
    const searchParams = new URLSearchParams({ consent_challenge: consent.id });

    redirectUrl.search = searchParams.toString();

    return new Response().setCookie('unvision:consent', consent.id).redirect(redirectUrl);
  }

  /**
   * Checks if the parameters of the current authorization request matches the parameters
   * of the original authorization request.
   *
   * @param requestParameters Parameters of the current Authorization Request.
   * @param consentParameters Parameters of the original Authorization Request.
   */
  private checkConsentParameters(
    requestParameters: AuthorizationRequest,
    consentParameters: AuthorizationRequest
  ): void {
    if (!isDeepStrictEqual(requestParameters, consentParameters)) {
      throw new InvalidRequestException({
        error_description: 'One or more parameters changed since the initial request.',
      });
    }
  }
}
