import { DependencyInjectionContainer } from '@unvision/di';

import { Buffer } from 'buffer';
import { OutgoingHttpHeaders } from 'http';

import { OAuth2Client } from '../entities/oauth2-client.entity';
import { InvalidClientException } from '../exceptions/invalid-client.exception';
import { InvalidRequestException } from '../exceptions/invalid-request.exception';
import { UnauthorizedClientException } from '../exceptions/unauthorized-client.exception';
import { UnsupportedGrantTypeException } from '../exceptions/unsupported-grant-type.exception';
import { GrantType } from '../grant-types/grant-type';
import { ClientAuthenticationHandler } from '../handlers/client-authentication.handler';
import { Request } from '../http/request';
import { Response } from '../http/response';
import { GRANT_TYPE } from '../metadata/metadata.keys';
import { TokenResponse } from '../types/token-response';
import { TokenEndpoint } from './token.endpoint';

jest.mock('../handlers/client-authentication.handler');

describe('Token Endpoint', () => {
  let endpoint: TokenEndpoint;

  const grantTypesMocks = [
    jest.mocked<GrantType>({ name: 'authorization_code', handle: jest.fn() }),
    jest.mocked<GrantType>({ name: 'client_credentials', handle: jest.fn() }),
  ];

  const clientAuthenticationHandlerMock = jest.mocked(ClientAuthenticationHandler.prototype, true);

  beforeEach(() => {
    const container = new DependencyInjectionContainer();

    grantTypesMocks.forEach((grantType) => container.bind<GrantType>(GRANT_TYPE).toValue(grantType));

    container.bind(ClientAuthenticationHandler).toSelf().asSingleton();
    container.bind(TokenEndpoint).toSelf().asSingleton();

    endpoint = container.resolve(TokenEndpoint);
  });

  describe('name', () => {
    it('should have "token" as its name.', () => {
      expect(endpoint.name).toBe('token');
    });
  });

  describe('path', () => {
    it('should have "/oauth/token" as its default path.', () => {
      expect(endpoint.path).toBe('/oauth/token');
    });
  });

  describe('httpMethods', () => {
    it('should have \'["post"]\' as its default path.', () => {
      expect(endpoint.httpMethods).toStrictEqual(['post']);
    });
  });

  describe('headers', () => {
    it('should have a default "headers" object for the http response.', () => {
      expect(endpoint['headers']).toMatchObject<OutgoingHttpHeaders>({
        'Cache-Control': 'no-store',
        Pragma: 'no-cache',
      });
    });
  });

  describe('handle()', () => {
    let request: Request;

    beforeEach(() => {
      request = {
        body: { grant_type: 'authorization_code' },
        cookies: {},
        headers: {},
        method: 'post',
        path: '/oauth/token',
        query: {},
      };
    });

    it('should throw when not providing a "grant_type" parameter.', async () => {
      delete request.body.grant_type;

      const error = new InvalidRequestException({ error_description: 'Invalid parameter "grant_type".' });

      await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
        body: Buffer.from(JSON.stringify(error.toJSON()), 'utf8'),
        headers: { 'Content-Type': 'application/json', ...endpoint['headers'] },
        statusCode: error.statusCode,
      });
    });

    it('should throw when requesting an unsupported grant type.', async () => {
      request.body.grant_type = 'unknown';

      const error = new UnsupportedGrantTypeException({ error_description: 'Unsupported grant_type "unknown".' });

      await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
        body: Buffer.from(JSON.stringify(error.toJSON()), 'utf8'),
        headers: { 'Content-Type': 'application/json', ...endpoint['headers'] },
        statusCode: error.statusCode,
      });
    });

    it('should throw when not using a client authentication method.', async () => {
      const error = new InvalidClientException({ error_description: 'No Client Authentication Method detected.' });

      clientAuthenticationHandlerMock.authenticate.mockRejectedValue(error);

      await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
        body: Buffer.from(JSON.stringify(error.toJSON()), 'utf8'),
        headers: { 'Content-Type': 'application/json', ...endpoint['headers'] },
        statusCode: error.statusCode,
      });
    });

    it('should throw when using multiple client authentication methods.', async () => {
      const error = new InvalidClientException({
        error_description: 'Multiple Client Authentication Methods detected.',
      });

      clientAuthenticationHandlerMock.authenticate.mockRejectedValue(error);

      await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
        body: Buffer.from(JSON.stringify(error.toJSON()), 'utf8'),
        headers: { 'Content-Type': 'application/json', ...endpoint['headers'] },
        statusCode: error.statusCode,
      });
    });

    it("should throw when the provided secret does not match the client's one.", async () => {
      const error = new InvalidClientException({ error_description: 'Invalid Credentials.' }).setHeaders({
        'WWW-Authenticate': 'Basic',
      });

      clientAuthenticationHandlerMock.authenticate.mockRejectedValue(error);

      await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
        body: Buffer.from(JSON.stringify(error.toJSON()), 'utf8'),
        headers: { 'Content-Type': 'application/json', ...endpoint['headers'], ...error.headers },
        statusCode: error.statusCode,
      });
    });

    it('should throw when a client requests a grant type not allowed to itself.', async () => {
      const error = new UnauthorizedClientException({
        error_description: 'This Client is not allowed to request the grant_type "authorization_code".',
      });

      clientAuthenticationHandlerMock.authenticate.mockResolvedValue(<OAuth2Client>{
        id: 'client_id',
        grantTypes: ['password'],
      });

      await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
        body: Buffer.from(JSON.stringify(error.toJSON()), 'utf8'),
        headers: { 'Content-Type': 'application/json', ...endpoint['headers'] },
        statusCode: error.statusCode,
      });
    });

    it('should return a token response.', async () => {
      const accessTokenResponse: TokenResponse = {
        access_token: 'access_token',
        token_type: 'Bearer',
        expires_in: 3600,
        scope: 'foo bar',
        refresh_token: 'refresh_token',
      };

      clientAuthenticationHandlerMock.authenticate.mockResolvedValue(<OAuth2Client>{
        id: 'client_id',
        grantTypes: ['authorization_code'],
      });

      grantTypesMocks[0]!.handle.mockResolvedValue(accessTokenResponse);

      await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
        body: Buffer.from(JSON.stringify(accessTokenResponse), 'utf8'),
        headers: { 'Content-Type': 'application/json', ...endpoint['headers'] },
        statusCode: 200,
      });
    });
  });
});
