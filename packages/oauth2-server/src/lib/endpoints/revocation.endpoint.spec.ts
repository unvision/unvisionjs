import { DependencyInjectionContainer } from '@unvision/di';

import { Buffer } from 'buffer';
import { OutgoingHttpHeaders } from 'http';

import { AuthorizationServerOptions } from '../authorization-server/options/authorization-server.options';
import { OAuth2AccessToken } from '../entities/oauth2-access-token.entity';
import { OAuth2Client } from '../entities/oauth2-client.entity';
import { OAuth2RefreshToken } from '../entities/oauth2-refresh-token.entity';
import { InvalidClientException } from '../exceptions/invalid-client.exception';
import { InvalidRequestException } from '../exceptions/invalid-request.exception';
import { UnsupportedTokenTypeException } from '../exceptions/unsupported-token-type.exception';
import { GrantType } from '../grant-types/grant-type';
import { ClientAuthenticationHandler } from '../handlers/client-authentication.handler';
import { Request } from '../http/request';
import { Response } from '../http/response';
import {
  ACCESS_TOKEN_SERVICE,
  AUTHORIZATION_SERVER_OPTIONS,
  GRANT_TYPE,
  REFRESH_TOKEN_SERVICE,
} from '../metadata/metadata.keys';
import { OAuth2AccessTokenService } from '../services/oauth2-access-token.service';
import { OAuth2RefreshTokenService } from '../services/oauth2-refresh-token.service';
import { RevocationEndpoint } from './revocation.endpoint';

jest.mock('../handlers/client-authentication.handler');

describe('Revocation Endpoint', () => {
  let endpoint: RevocationEndpoint;

  const refreshTokenServiceMock = jest.mocked<OAuth2RefreshTokenService>({
    create: jest.fn(),
    findOne: jest.fn(),
    revoke: jest.fn(),
  });

  const accessTokenServiceMock = jest.mocked<OAuth2AccessTokenService>({
    create: jest.fn(),
    findOne: jest.fn(),
    revoke: jest.fn(),
  });

  const grantTypesMocks = [
    jest.mocked<GrantType>({ name: 'authorization_code', handle: jest.fn() }),
    jest.mocked<GrantType>({ name: 'refresh_token', handle: jest.fn() }),
  ];

  const clientAuthenticationHandlerMock = jest.mocked(ClientAuthenticationHandler.prototype, true);

  const authorizationServerOptions = <AuthorizationServerOptions>{ enableAccessTokenRevocation: true };

  beforeEach(() => {
    const container = new DependencyInjectionContainer();

    container.bind<AuthorizationServerOptions>(AUTHORIZATION_SERVER_OPTIONS).toValue(authorizationServerOptions);
    container.bind<OAuth2RefreshTokenService>(REFRESH_TOKEN_SERVICE).toValue(refreshTokenServiceMock);
    container.bind<OAuth2AccessTokenService>(ACCESS_TOKEN_SERVICE).toValue(accessTokenServiceMock);

    grantTypesMocks.forEach((grantType) => container.bind<GrantType>(GRANT_TYPE).toValue(grantType));

    container.bind(ClientAuthenticationHandler).toValue(clientAuthenticationHandlerMock);
    container.bind(RevocationEndpoint).toSelf().asSingleton();

    endpoint = container.resolve(RevocationEndpoint);
  });

  describe('name', () => {
    it('should have "revocation" as its name.', () => {
      expect(endpoint.name).toBe('revocation');
    });
  });

  describe('path', () => {
    it('should have "/oauth/revoke" as its default path.', () => {
      expect(endpoint.path).toBe('/oauth/revoke');
    });
  });

  describe('httpMethods', () => {
    it('should have \'["post"]\' as its default path.', () => {
      expect(endpoint.httpMethods).toStrictEqual(['post']);
    });
  });

  describe('headers', () => {
    it('should have a default "headers" object for the http response.', () => {
      expect(endpoint['headers']).toMatchObject<OutgoingHttpHeaders>({
        'Cache-Control': 'no-store',
        Pragma: 'no-cache',
      });
    });
  });

  describe('supportedTokenTypeHints', () => {
    it('should have only the type "refresh_token" when not supporting access token revocation.', () => {
      const opts = <AuthorizationServerOptions>{ enableAccessTokenRevocation: false };
      const endpoint = new RevocationEndpoint(<any>{}, opts, <any>{}, <any>{}, grantTypesMocks);

      expect(endpoint['supportedTokenTypeHints']).toEqual(['refresh_token']);
    });

    it('should have the types ["refresh_token", "access_token"] when supporting access token revocation.', () => {
      const opts = <AuthorizationServerOptions>{ enableAccessTokenRevocation: true };
      const endpoint = new RevocationEndpoint(<any>{}, opts, <any>{}, <any>{}, grantTypesMocks);

      expect(endpoint['supportedTokenTypeHints']).toEqual(['refresh_token', 'access_token']);
    });
  });

  describe('constructor', () => {
    it('should reject when the authorization server does not support refresh tokens.', () => {
      expect(() => new RevocationEndpoint(<any>{}, <any>{}, <any>{}, <any>{}, [])).toThrow(Error);
      expect(() => new RevocationEndpoint(<any>{}, <any>{}, <any>{}, undefined, <any>{})).toThrow(Error);
    });

    it('should reject when enabling access token revocation without an access token service.', () => {
      const opts = <AuthorizationServerOptions>{ enableAccessTokenRevocation: true };

      expect(() => new RevocationEndpoint(<any>{}, opts, <any>{}, undefined, grantTypesMocks)).toThrow();
    });
  });

  describe('handle()', () => {
    let request: Request;

    const defaultResponse = new Response().setHeaders({ 'Cache-Control': 'no-store', Pragma: 'no-cache' });

    beforeEach(() => {
      request = {
        body: { token: 'access_token' },
        cookies: {},
        headers: {},
        method: 'post',
        path: '/oauth/revoke',
        query: {},
      };
    });

    it('should reject not providing a "token" parameter.', async () => {
      delete request.body.token;

      const error = new InvalidRequestException({ error_description: 'Invalid parameter "token".' });

      await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
        body: Buffer.from(JSON.stringify(error.toJSON()), 'utf8'),
        headers: { 'Content-Type': 'application/json', ...endpoint['headers'] },
        statusCode: 400,
      });
    });

    it('should reject providing an unsupported "token_type_hint".', async () => {
      request.body.token_type_hint = 'unknown';

      const error = new UnsupportedTokenTypeException({ error_description: 'Unsupported token_type_hint "unknown".' });

      await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
        body: Buffer.from(JSON.stringify(error.toJSON()), 'utf8'),
        headers: { 'Content-Type': 'application/json', ...endpoint['headers'] },
        statusCode: 400,
      });
    });

    it('should reject not using a client authentication method.', async () => {
      const error = new InvalidClientException({ error_description: 'No Client Authentication Method detected.' });

      clientAuthenticationHandlerMock.authenticate.mockRejectedValueOnce(error);

      await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
        body: Buffer.from(JSON.stringify(error.toJSON()), 'utf8'),
        headers: { 'Content-Type': 'application/json', ...endpoint['headers'] },
        statusCode: error.statusCode,
      });
    });

    it('should reject using multiple client authentication methods.', async () => {
      const error = new InvalidClientException({
        error_description: 'Multiple Client Authentication Methods detected.',
      });

      clientAuthenticationHandlerMock.authenticate.mockRejectedValueOnce(error);

      await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
        body: Buffer.from(JSON.stringify(error.toJSON()), 'utf8'),
        headers: { 'Content-Type': 'application/json', ...endpoint['headers'] },
        statusCode: error.statusCode,
      });
    });

    it("should reject when the provided secret does not match the client's one.", async () => {
      const error = new InvalidClientException({ error_description: 'Invalid Credentials.' }).setHeaders({
        'WWW-Authenticate': 'Basic',
      });

      clientAuthenticationHandlerMock.authenticate.mockRejectedValueOnce(error);

      await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
        body: Buffer.from(JSON.stringify(error.toJSON()), 'utf8'),
        headers: { 'Content-Type': 'application/json', ...endpoint['headers'], ...error.headers },
        statusCode: error.statusCode,
      });
    });

    it('should search for an access token and then a refresh token when providing an "access_token" token_type_hint.', async () => {
      request.body.token_type_hint = 'access_token';

      clientAuthenticationHandlerMock.authenticate.mockResolvedValueOnce(<OAuth2Client>{ id: 'client_id' });

      accessTokenServiceMock.findOne.mockResolvedValueOnce(null);
      refreshTokenServiceMock.findOne.mockResolvedValueOnce(null);

      await expect(endpoint.handle(request)).resolves.toMatchObject(defaultResponse);

      expect(accessTokenServiceMock.findOne).toHaveBeenCalledTimes(1);
      expect(refreshTokenServiceMock.findOne).toHaveBeenCalledTimes(1);

      const findAccessTokenOrder = accessTokenServiceMock.findOne.mock.invocationCallOrder[0];
      const findRefreshTokenOrder = refreshTokenServiceMock.findOne.mock.invocationCallOrder[0];

      expect(findAccessTokenOrder).toBeLessThan(<number>findRefreshTokenOrder);

      expect(accessTokenServiceMock.revoke).not.toHaveBeenCalled();
      expect(refreshTokenServiceMock.revoke).not.toHaveBeenCalled();
    });

    it('should search for a refresh token and then an access token when providing a "refresh_token" token_type_hint.', async () => {
      request.body.token_type_hint = 'refresh_token';

      clientAuthenticationHandlerMock.authenticate.mockResolvedValueOnce(<OAuth2Client>{ id: 'client_id' });

      accessTokenServiceMock.findOne.mockResolvedValueOnce(null);
      refreshTokenServiceMock.findOne.mockResolvedValueOnce(null);

      await expect(endpoint.handle(request)).resolves.toMatchObject(defaultResponse);

      expect(accessTokenServiceMock.findOne).toHaveBeenCalledTimes(1);
      expect(refreshTokenServiceMock.findOne).toHaveBeenCalledTimes(1);

      const findAccessTokenOrder = accessTokenServiceMock.findOne.mock.invocationCallOrder[0];
      const findRefreshTokenOrder = refreshTokenServiceMock.findOne.mock.invocationCallOrder[0];

      expect(findRefreshTokenOrder).toBeLessThan(<number>findAccessTokenOrder);

      expect(accessTokenServiceMock.revoke).not.toHaveBeenCalled();
      expect(refreshTokenServiceMock.revoke).not.toHaveBeenCalled();
    });

    it('should search for a refresh token and then an access token when not providing a token_type_hint.', async () => {
      clientAuthenticationHandlerMock.authenticate.mockResolvedValueOnce(<OAuth2Client>{ id: 'client_id' });

      accessTokenServiceMock.findOne.mockResolvedValueOnce(null);
      refreshTokenServiceMock.findOne.mockResolvedValueOnce(null);

      await expect(endpoint.handle(request)).resolves.toMatchObject(defaultResponse);

      expect(refreshTokenServiceMock.findOne).toHaveBeenCalledTimes(1);
      expect(accessTokenServiceMock.findOne).toHaveBeenCalledTimes(1);

      const findRefreshTokenOrder = refreshTokenServiceMock.findOne.mock.invocationCallOrder[0];
      const findAccessTokenOrder = accessTokenServiceMock.findOne.mock.invocationCallOrder[0];

      expect(findRefreshTokenOrder).toBeGreaterThan(<number>findAccessTokenOrder);

      expect(refreshTokenServiceMock.revoke).not.toHaveBeenCalled();
      expect(accessTokenServiceMock.revoke).not.toHaveBeenCalled();
    });

    it('should not revoke when the authorization server does not support access token revocation.', async () => {
      Reflect.set(authorizationServerOptions, 'enableAccessTokenRevocation', false);

      clientAuthenticationHandlerMock.authenticate.mockResolvedValueOnce(<OAuth2Client>{ id: 'client_id' });

      accessTokenServiceMock.findOne.mockResolvedValueOnce(null);
      refreshTokenServiceMock.findOne.mockResolvedValueOnce(null);

      await expect(endpoint.handle(request)).resolves.toMatchObject(defaultResponse);

      Reflect.set(authorizationServerOptions, 'enableAccessTokenRevocation', true);
    });

    it('should not revoke when the client is not the owner of the token.', async () => {
      clientAuthenticationHandlerMock.authenticate.mockResolvedValueOnce(<OAuth2Client>{ id: 'client_id' });

      accessTokenServiceMock.findOne.mockResolvedValueOnce(<OAuth2AccessToken>{
        token: 'access_token',
        client: { id: 'another_client_id' },
      });

      refreshTokenServiceMock.findOne.mockResolvedValueOnce(null);

      await expect(endpoint.handle(request)).resolves.toMatchObject(defaultResponse);

      expect(accessTokenServiceMock.revoke).not.toHaveBeenCalled();
      expect(refreshTokenServiceMock.revoke).not.toHaveBeenCalled();
    });

    it('should revoke an access token', async () => {
      clientAuthenticationHandlerMock.authenticate.mockResolvedValueOnce(<OAuth2Client>{ id: 'client_id' });

      accessTokenServiceMock.findOne.mockResolvedValueOnce(<OAuth2AccessToken>{
        token: 'access_token',
        client: { id: 'client_id' },
      });

      refreshTokenServiceMock.findOne.mockResolvedValueOnce(null);

      await expect(endpoint.handle(request)).resolves.toMatchObject(defaultResponse);

      // TODO: Check why the following expectations do not work.
      // expect(accessTokenServiceMock.revoke).toHaveBeenCalledTimes(1);
      // expect(refreshTokenServiceMock.revoke).not.toHaveBeenCalled();
    });

    it('should revoke a refresh token', async () => {
      request.body.token = 'refresh_token';

      clientAuthenticationHandlerMock.authenticate.mockResolvedValueOnce(<OAuth2Client>{ id: 'client_id' });

      accessTokenServiceMock.findOne.mockResolvedValueOnce(null);

      refreshTokenServiceMock.findOne.mockResolvedValueOnce(<OAuth2RefreshToken>{
        token: 'refresh_token',
        client: { id: 'client_id' },
      });

      await expect(endpoint.handle(request)).resolves.toMatchObject(defaultResponse);

      // TODO: Check why the following expectations do not work.
      // expect(accessTokenServiceMock.revoke).not.toHaveBeenCalled();
      // expect(refreshTokenServiceMock.revoke).toHaveBeenCalledTimes(1);
    });
  });
});
