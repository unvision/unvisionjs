import { getContainer, Inject, Injectable } from '@unvision/di';

import { URL } from 'url';

import { AuthorizationServerOptions } from '../authorization-server/options/authorization-server.options';
import { Request } from '../http/request';
import { Response } from '../http/response';
import { AUTHORIZATION_SERVER_OPTIONS, ENDPOINT } from '../metadata/metadata.keys';
import { DiscoveryResponse } from '../types/discovery-response';
import { Endpoint } from './endpoint';

/**
 * Implementation of the **Discovery** Endpoint.
 *
 * This endpoint is used to provide the Metadata of the Authorization Server to the Client.
 *
 * @see https://www.rfc-editor.org/rfc/rfc8414.html
 * @see https://openid.net/specs/openid-connect-discovery-1_0.html
 */
@Injectable()
export class DiscoveryEndpoint implements Endpoint {
  /**
   * Name of the Endpoint.
   */
  public readonly name: string = 'discovery';

  /**
   * Path of the Endpoint.
   */
  public readonly path: string = '/.well-known/openid-configuration';

  /**
   * Http Methods supported by the Endpoint.
   */
  public readonly httpMethods: string[] = ['get'];

  /**
   * Indicates if the Endpoint will be published at the Discovery Document.
   */
  public readonly publish: boolean = false;

  public constructor(
    @Inject(AUTHORIZATION_SERVER_OPTIONS) private readonly authorizationServerOptions: AuthorizationServerOptions
  ) {}

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  public async handle(_request: Request): Promise<Response> {
    const discoveryResponse = <DiscoveryResponse>{
      issuer: this.authorizationServerOptions.issuer,
      authorization_endpoint: this.getEndpointPath('authorization'),
      token_endpoint: this.getEndpointPath('token'),
      jwks_uri: undefined,
      scopes_supported: this.authorizationServerOptions.scopes,
      response_types_supported: this.authorizationServerOptions.responseTypes,
      response_modes_supported: this.authorizationServerOptions.responseModes,
      grant_types_supported: this.authorizationServerOptions.grantTypes,
      token_endpoint_auth_methods_supported: this.authorizationServerOptions.clientAuthenticationMethods,
      token_endpoint_auth_signing_alg_values_supported:
        this.authorizationServerOptions.clientAuthenticationSignatureAlgorithms,
      service_documentation: undefined,
      ui_locales_supported: undefined,
      op_policy_uri: undefined,
      op_tos_uri: undefined,
      revocation_endpoint: this.getEndpointPath('revocation'),
      revocation_endpoint_auth_methods_supported: this.authorizationServerOptions.clientAuthenticationMethods,
      revocation_endpoint_auth_signing_alg_values_supported:
        this.authorizationServerOptions.clientAuthenticationSignatureAlgorithms,
      introspection_endpoint: this.getEndpointPath('introspection'),
      introspection_endpoint_auth_methods_supported: this.authorizationServerOptions.clientAuthenticationMethods,
      introspection_endpoint_auth_signing_alg_values_supported:
        this.authorizationServerOptions.clientAuthenticationSignatureAlgorithms,
      code_challenge_methods_supported: this.authorizationServerOptions.pkceMethods,
    };

    return new Response().json(discoveryResponse);
  }

  private getEndpointPath(name: string): string | undefined {
    const endpoints = getContainer('oauth2').resolveAll<Endpoint>(ENDPOINT);
    const path = endpoints.find((endpoint) => endpoint.name === name)?.path;

    if (path === undefined) {
      return undefined;
    }

    return new URL(path, this.authorizationServerOptions.issuer).href;
  }
}
