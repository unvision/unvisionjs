import { DependencyInjectionContainer } from '@unvision/di';

import { Buffer } from 'buffer';
import { OutgoingHttpHeaders } from 'http';

import { InvalidRequestException } from '../exceptions/invalid-request.exception';
import { ServerErrorException } from '../exceptions/server-error.exception';
import { UnsupportedInteractionTypeException } from '../exceptions/unsupported-interaction-type.exception';
import { Request } from '../http/request';
import { Response } from '../http/response';
import { InteractionType } from '../interaction-types/interaction-type';
import { INTERACTION_TYPE } from '../metadata/metadata.keys';
import { InteractionEndpoint } from './interaction.endpoint';

describe('Interaction Endpoint', () => {
  let endpoint: InteractionEndpoint;

  const interactionTypesMock = [
    jest.mocked<InteractionType>({ name: 'login', handleContext: jest.fn(), handleDecision: jest.fn() }),
    jest.mocked<InteractionType>({ name: 'consent', handleContext: jest.fn(), handleDecision: jest.fn() }),
  ];

  beforeEach(() => {
    const container = new DependencyInjectionContainer();

    interactionTypesMock.forEach((interactionType) => {
      container.bind<InteractionType>(INTERACTION_TYPE).toValue(interactionType);
    });

    container.bind(InteractionEndpoint).toSelf().asSingleton();

    endpoint = container.resolve(InteractionEndpoint);
  });

  describe('name', () => {
    it('should have "interaction" as its name.', () => {
      expect(endpoint.name).toBe('interaction');
    });
  });

  describe('path', () => {
    it('should have "/oauth/interaction" as its default path.', () => {
      expect(endpoint.path).toBe('/oauth/interaction');
    });
  });

  describe('httpMethods', () => {
    it('should have \'["get", "post"]\' as its default path.', () => {
      expect(endpoint.httpMethods).toStrictEqual(['get', 'post']);
    });
  });

  describe('headers', () => {
    it('should have a default "headers" object for the http response.', () => {
      expect(endpoint['headers']).toMatchObject<OutgoingHttpHeaders>({
        'Cache-Control': 'no-store',
        Pragma: 'no-cache',
      });
    });
  });

  describe('handle()', () => {
    let request: Request;

    beforeEach(() => {
      request = { body: {}, cookies: {}, headers: {}, method: '', path: '/oauth/interaction', query: {} };
    });

    it('should return an error response when providing an unsupported http method.', async () => {
      Reflect.set(request, 'method', 'put');

      const error = new ServerErrorException({ error_description: 'An unexpected error occurred.' });

      await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
        body: Buffer.from(JSON.stringify(error.toJSON()), 'utf8'),
        headers: { 'Content-Type': 'application/json', ...endpoint['headers'] },
        statusCode: error.statusCode,
      });
    });

    it.each(['get', 'post'])(
      'should return an error response when not providing an "interaction_type" parameter.',
      async (method) => {
        Reflect.set(request, 'method', method);

        const error = new InvalidRequestException({ error_description: 'Invalid parameter "interaction_type".' });

        await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
          body: Buffer.from(JSON.stringify(error.toJSON()), 'utf8'),
          headers: { 'Content-Type': 'application/json', ...endpoint['headers'] },
          statusCode: error.statusCode,
        });
      }
    );

    it.each([
      ['get', 'query'],
      ['post', 'body'],
    ])('should return an error response when requesting an unsupported interaction type.', async (method, data) => {
      Reflect.set(request, 'method', method);
      Reflect.set(request, data, { interaction_type: 'unknown' });

      const error = new UnsupportedInteractionTypeException({
        error_description: 'Unsupported interaction_type "unknown".',
      });

      await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
        body: Buffer.from(JSON.stringify(error.toJSON()), 'utf8'),
        headers: { 'Content-Type': 'application/json', ...endpoint['headers'] },
        statusCode: error.statusCode,
      });
    });

    it('should return an interaction context response.', async () => {
      Reflect.set(request, 'method', 'get');

      request.query.interaction_type = 'login';

      const interactionTypeResponse: Record<string, any> = { skip: true, client: { id: 'client_id' } };

      interactionTypesMock[0]!.handleContext.mockResolvedValueOnce(interactionTypeResponse);

      await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
        body: Buffer.from(JSON.stringify(interactionTypeResponse), 'utf8'),
        headers: { 'Content-Type': 'application/json', ...endpoint['headers'] },
        statusCode: 200,
      });
    });

    it('should return an interaction decision response.', async () => {
      Reflect.set(request, 'method', 'post');

      request.body.interaction_type = 'login';

      const interactionTypeResponse: Record<string, any> = { skip: true, client: { id: 'client_id' } };

      interactionTypesMock[0]!.handleDecision.mockResolvedValueOnce(interactionTypeResponse);

      await expect(endpoint.handle(request)).resolves.toMatchObject<Partial<Response>>({
        body: Buffer.from(JSON.stringify(interactionTypeResponse), 'utf8'),
        headers: { 'Content-Type': 'application/json', ...endpoint['headers'] },
        statusCode: 200,
      });
    });
  });
});
