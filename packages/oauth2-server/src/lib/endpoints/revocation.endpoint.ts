import { Inject, Injectable, InjectAll, Optional } from '@unvision/di';

import { Buffer } from 'buffer';
import { timingSafeEqual } from 'crypto';
import { OutgoingHttpHeaders } from 'http';

import { AuthorizationServerOptions } from '../authorization-server/options/authorization-server.options';
import { OAuth2AccessToken } from '../entities/oauth2-access-token.entity';
import { OAuth2Client } from '../entities/oauth2-client.entity';
import { OAuth2RefreshToken } from '../entities/oauth2-refresh-token.entity';
import { InvalidRequestException } from '../exceptions/invalid-request.exception';
import { OAuth2Exception } from '../exceptions/oauth2.exception';
import { ServerErrorException } from '../exceptions/server-error.exception';
import { UnsupportedTokenTypeException } from '../exceptions/unsupported-token-type.exception';
import { GrantType } from '../grant-types/grant-type';
import { ClientAuthenticationHandler } from '../handlers/client-authentication.handler';
import { Request } from '../http/request';
import { Response } from '../http/response';
import {
  ACCESS_TOKEN_SERVICE,
  AUTHORIZATION_SERVER_OPTIONS,
  GRANT_TYPE,
  REFRESH_TOKEN_SERVICE,
} from '../metadata/metadata.keys';
import { OAuth2AccessTokenService } from '../services/oauth2-access-token.service';
import { OAuth2RefreshTokenService } from '../services/oauth2-refresh-token.service';
import { RevocationRequest } from '../types/revocation-request';
import { Endpoint } from './endpoint';

interface FindTokenResult {
  readonly entity: OAuth2AccessToken | OAuth2RefreshToken;
  readonly tokenType: string;
}

/**
 * Implementation of the **Revocation** Endpoint.
 *
 * This endpoint is used by the Client to revoke a Token in its possession.
 *
 * If the Client succeeds to authenticate but provides a token that was not issued to itself, the Authorization server
 * does not revoke the token, since the Client is not authorized to operate it.
 *
 * If the token is already invalid, does not exist within the Authorization Server or is otherwise unknown or invalid,
 * it is already considered ***revoked*** and, therefore, no further operation occurs.
 *
 * @see https://www.rfc-editor.org/rfc/rfc7009.html
 */
@Injectable()
export class RevocationEndpoint implements Endpoint {
  /**
   * Name of the Endpoint.
   */
  public readonly name: string = 'revocation';

  /**
   * Path of the Endpoint.
   */
  public readonly path: string = '/oauth/revoke';

  /**
   * Http Methods supported by the Endpoint.
   */
  public readonly httpMethods: string[] = ['post'];

  /**
   * Default HTTP Headers to be included in the Response.
   */
  private readonly headers: OutgoingHttpHeaders = { 'Cache-Control': 'no-store', Pragma: 'no-cache' };

  /**
   * Token Type Hints supported by the Revocation Endpoint.
   */
  private readonly supportedTokenTypeHints: string[] = ['refresh_token'];

  /**
   * Instantiates a new Revocation Endpoint.
   *
   * @param clientAuthenticationHandler Instance of the Client Authenticator.
   * @param authorizationServerOptions Configuration Parameters of the Authorization Server.
   * @param refreshTokenService Instance of the Refresh Token Service.
   * @param accessTokenService Instance of the Access Token Service.
   * @param grantTypes Grant Types supported by the Authorization Server.
   */
  public constructor(
    private readonly clientAuthenticationHandler: ClientAuthenticationHandler,
    @Inject(AUTHORIZATION_SERVER_OPTIONS) private readonly authorizationServerOptions: AuthorizationServerOptions,
    @Inject(REFRESH_TOKEN_SERVICE) private readonly refreshTokenService: OAuth2RefreshTokenService,
    @Optional() @Inject(ACCESS_TOKEN_SERVICE) private readonly accessTokenService?: OAuth2AccessTokenService,
    @Optional() @InjectAll(GRANT_TYPE) grantTypes?: GrantType[]
  ) {
    if (grantTypes?.find((grantType) => grantType.name === 'refresh_token') === undefined) {
      throw new Error('The Authorization Server does not support using Refresh Tokens.');
    }

    if (this.authorizationServerOptions.enableAccessTokenRevocation) {
      if (this.accessTokenService === undefined) {
        throw new Error('Cannot enable Access Token Revocation without an Access Token Service.');
      }

      this.supportedTokenTypeHints.push('access_token');
    }
  }

  /**
   * Revokes a previously issued Token.
   *
   * First it validates the Revocation Request of the Client by making sure the required parameter **token** is present,
   * and that the Client can authenticate within the Revocation Endpoint.
   *
   * It then tries to revoke the provided Token from the application's storage.
   *
   * Unless the Client presents an unsupported token_type_hint, fails to authenticate or does not present a token,
   * this endpoint will **ALWAYS** return a ***success*** response.
   *
   * This is done in order to prevent a Client from fishing any information that it should not have access to.
   *
   * @param request HTTP Request.
   * @returns HTTP Response.
   */
  public async handle(request: Request): Promise<Response> {
    const parameters = <RevocationRequest>request.body;

    try {
      this.checkParameters(parameters);

      const client = await this.clientAuthenticationHandler.authenticate(request);

      await this.revokeToken(parameters, client);

      return new Response().setHeaders(this.headers);
    } catch (exc: any) {
      const error =
        exc instanceof OAuth2Exception
          ? exc
          : new ServerErrorException({ error_description: 'An unexpected error occurred.' }, exc);

      return new Response()
        .setStatus(error.statusCode)
        .setHeaders(error.headers)
        .setHeaders(this.headers)
        .json(error.toJSON());
    }
  }

  /**
   * Checks if the Parameters of the Revocation Request are valid.
   *
   * @param parameters Parameters of the Revocation Request.
   */
  protected checkParameters(parameters: RevocationRequest): void {
    const { token, token_type_hint: tokenTypeHint } = parameters;

    if (typeof token !== 'string') {
      throw new InvalidRequestException({ error_description: 'Invalid parameter "token".' });
    }

    if (tokenTypeHint !== undefined && !this.supportedTokenTypeHints.includes(tokenTypeHint)) {
      throw new UnsupportedTokenTypeException({ error_description: `Unsupported token_type_hint "${tokenTypeHint}".` });
    }
  }

  /**
   * Revokes the provided Token from the application's storage.
   *
   * @param parameters Parameters of the Revocation Request.
   * @param client Client of the Request.
   */
  private async revokeToken(parameters: RevocationRequest, client: OAuth2Client): Promise<void> {
    const { token, token_type_hint: tokenTypeHint } = parameters;

    const { entity, tokenType } = (await this.findTokenEntity(token, tokenTypeHint)) ?? {};

    if (entity === undefined || tokenType === undefined) {
      return;
    }

    const clientId = Buffer.from(client.id, 'utf8');
    const tokenClientId = Buffer.from(entity.client.id, 'utf8');

    if (clientId.length !== tokenClientId.length || !timingSafeEqual(clientId, tokenClientId)) {
      return;
    }

    switch (tokenType) {
      case 'access_token':
        return await this.accessTokenService!.revoke(<OAuth2AccessToken>entity);

      case 'refresh_token':
        return await this.refreshTokenService.revoke(<OAuth2RefreshToken>entity);
    }
  }

  /**
   * Searches the application's storage for a Token Entity that satisfies the Token provided by the Client.
   *
   * @param token Token provided by the Client.
   * @param tokenTypeHint Optional hint about the type of the Token.
   * @returns Resulting Token Entity and its type.
   */
  private async findTokenEntity(token: string, tokenTypeHint?: string): Promise<FindTokenResult | null> {
    switch (tokenTypeHint) {
      case 'refresh_token':
        return (await this.findRefreshToken(token)) ?? (await this.findAccessToken(token));

      case 'access_token':
      default:
        return (await this.findAccessToken(token)) ?? (await this.findRefreshToken(token));
    }
  }

  /**
   * Searches the application's storage for an Access Token.
   *
   * @param token Token provided by the Client.
   * @returns Result of the search.
   */
  private async findAccessToken(token: string): Promise<FindTokenResult | null> {
    if (!this.authorizationServerOptions.enableAccessTokenRevocation) {
      return null;
    }

    const entity = await this.accessTokenService!.findOne(token);

    return entity !== null ? { entity, tokenType: 'access_token' } : null;
  }

  /**
   * Searches the application's storage for a Refresh Token.
   *
   * @param token Token provided by the Client.
   * @returns Result of the search.
   */
  private async findRefreshToken(token: string): Promise<FindTokenResult | null> {
    const entity = await this.refreshTokenService.findOne(token);
    return entity !== null ? { entity, tokenType: 'refresh_token' } : null;
  }
}
