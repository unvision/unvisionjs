import { Injectable, InjectAll } from '@unvision/di';

import { OutgoingHttpHeaders } from 'http';

import { InvalidRequestException } from '../exceptions/invalid-request.exception';
import { OAuth2Exception } from '../exceptions/oauth2.exception';
import { ServerErrorException } from '../exceptions/server-error.exception';
import { UnsupportedInteractionTypeException } from '../exceptions/unsupported-interaction-type.exception';
import { Request } from '../http/request';
import { Response } from '../http/response';
import { InteractionType } from '../interaction-types/interaction-type';
import { INTERACTION_TYPE } from '../metadata/metadata.keys';
import { InteractionRequest } from '../types/interaction-request';
import { Endpoint } from './endpoint';

/**
 * Implementation of the custom **Interaction** Endpoint.
 *
 * This endpoint is used by the application to fetch the context of, or to inform the decision of a User Interaction.
 *
 * Since there is no standard OAuth 2.0 specification regarding the Interactions with an End User,
 * this package implements a custom protocol for them.
 *
 * Further instructions on each Interaction should be consulted at the respective Interaction Type's documentation.
 */
@Injectable()
export class InteractionEndpoint implements Endpoint {
  /**
   * Name of the Endpoint.
   */
  public readonly name: string = 'interaction';

  /**
   * Path of the Endpoint.
   */
  public readonly path: string = '/oauth/interaction';

  /**
   * Http Methods supported by the Endpoint.
   */
  public readonly httpMethods: string[] = ['get', 'post'];

  /**
   * Default Http Headers to be included in the Response.
   */
  private readonly headers: OutgoingHttpHeaders = { 'Cache-Control': 'no-store', Pragma: 'no-cache' };

  /**
   * Instantiates a new Interaction Endpoint.
   *
   * @param interactionTypes Interaction Types registered at the Authorization Server.
   */
  public constructor(@InjectAll(INTERACTION_TYPE) private readonly interactionTypes: InteractionType[]) {}

  /**
   * Creates an Http JSON Interaction Response.
   *
   * This method is a dispatcher for either the Interaction Context Request or the Interaction Decision Request,
   * represented by the Http Methods GET and POST, respectively.
   *
   * @param request Http Request.
   * @returns Http Response.
   */
  public async handle(request: Request): Promise<Response> {
    try {
      switch (request.method.toLowerCase()) {
        case 'get':
          return await this.handleContext(request);

        case 'post':
          return await this.handleDecision(request);

        default:
          throw new TypeError(`Unsupported Http Method "${request.method.toUpperCase()}" for Interaction Endpoint.`);
      }
    } catch (exc: any) {
      const error =
        exc instanceof OAuth2Exception
          ? exc
          : new ServerErrorException({ error_description: 'An unexpected error occurred.' }, exc);

      return new Response()
        .setStatus(error.statusCode)
        .setHeaders(this.headers)
        .setHeaders(error.headers)
        .json(error.toJSON());
    }
  }

  /**
   * Handles the Context Flow of the Interaction.
   *
   * @param request Http Request.
   * @returns Http Response.
   */
  private async handleContext(request: Request): Promise<Response> {
    const parameters = <InteractionRequest>request.query;

    this.checkParameters(parameters);

    const interactionType = this.getInteractionType(parameters.interaction_type);
    const interactionResponse = await interactionType.handleContext(parameters);

    return new Response().setHeaders(this.headers).json(interactionResponse);
  }

  /**
   * Handles the Decision Flow of the Interaction.
   *
   * @param request Http Request.
   * @returns Http Response.
   */
  private async handleDecision(request: Request): Promise<Response> {
    const parameters = <InteractionRequest>request.body;

    this.checkParameters(parameters);

    const interactionType = this.getInteractionType(parameters.interaction_type);
    const interactionResponse = await interactionType.handleDecision(parameters);

    return new Response().setHeaders(this.headers).json(interactionResponse);
  }

  /**
   * Checks if the Parameters of the Interaction Request are valid.
   *
   * @param parameters Parameters of the Interaction Request.
   */
  private checkParameters(parameters: InteractionRequest): void {
    const { interaction_type: interactionType } = parameters;

    if (typeof interactionType !== 'string') {
      throw new InvalidRequestException({ error_description: 'Invalid parameter "interaction_type".' });
    }
  }

  /**
   * Retrieves the Interaction Type based on the **interaction_type** requested by the Client.
   *
   * @param name Interaction Type requested by the Client.
   * @returns Interaction Type.
   */
  private getInteractionType(name: string): InteractionType {
    const interactionType = this.interactionTypes.find((interactionType) => interactionType.name === name);

    if (interactionType === undefined) {
      throw new UnsupportedInteractionTypeException({ error_description: `Unsupported interaction_type "${name}".` });
    }

    return interactionType;
  }
}
