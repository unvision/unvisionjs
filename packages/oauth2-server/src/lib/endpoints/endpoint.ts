import { Request } from '../http/request';
import { Response } from '../http/response';

/**
 * Interface for the Endpoints of the OAuth 2.0 Authorization Server.
 *
 * @see https://www.rfc-editor.org/rfc/rfc6749.html#section-3
 */
export interface Endpoint {
  /**
   * Name of the Endpoint.
   */
  readonly name: string;

  /**
   * Path of the Endpoint.
   */
  readonly path: string;

  /**
   * Http Methods supported by the Endpoint.
   */
  readonly httpMethods: string[];

  /**
   * All Endpoints are required to implement this method, since it **MUST** return a Response back to the Client.
   *
   * The Type, Status, Headers and Body of the Response it returns, as well as its meaning and formatting,
   * have to be documented by the respective implementation.
   *
   * This method **MUST NOT** raise **ANY** exception.
   *
   * If an error occurred during the processing of the Request, it **MUST** be treated and its appropriate Status,
   * Headers and Body **MUST** be correctly set to denote the type of exception that occured.
   *
   * Other than the previous requirements, the endpoint is free to use whatever tools, methods and workflows it wishes.
   *
   * It is recommended to split the logic of the Endpoint into small single-responsibility methods.
   *
   * @param request HTTP Request.
   * @returns HTTP Response.
   */
  handle(request: Request): Promise<Response>;
}
