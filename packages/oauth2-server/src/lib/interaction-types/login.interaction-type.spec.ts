import { Container } from '@unvision/di';

import { URLSearchParams } from 'url';

import { AuthorizationServerOptions } from '../authorization-server/options/authorization-server.options';
import { OAuth2Client } from '../entities/oauth2-client.entity';
import { OAuth2Session } from '../entities/oauth2-session.entity';
import { AccessDeniedException } from '../exceptions/access-denied.exception';
import { InvalidRequestException } from '../exceptions/invalid-request.exception';
import { AUTHORIZATION_SERVER_OPTIONS, SESSION_SERVICE, USER_SERVICE } from '../metadata/metadata.keys';
import { OAuth2SessionService } from '../services/oauth2-session.service';
import { OAuth2UserService } from '../services/oauth2-user.service';
import { AuthorizationRequest } from '../types/authorization-request';
import { LoginContextInteractionRequest } from '../types/login-context.interaction-request';
import { LoginContextInteractionResponse } from '../types/login-context.interaction-response';
import { LoginDecisionInteractionRequest } from '../types/login-decision.interaction-request';
import { LoginDecisionInteractionResponse } from '../types/login-decision.interaction-response';
import { LoginInteractionType } from './login.interaction-type';

describe('Login Interaction Type', () => {
  let interactionType: LoginInteractionType;

  const sessionServiceMock = jest.mocked<OAuth2SessionService>({
    create: jest.fn(),
    findOne: jest.fn(),
    remove: jest.fn(),
    save: jest.fn(),
  });

  const userServiceMock = jest.mocked<OAuth2UserService>({
    findOne: jest.fn(),
  });

  const authorizationServerOptions = <AuthorizationServerOptions>{ issuer: 'https://server.example.com' };

  beforeEach(() => {
    Container.bind<AuthorizationServerOptions>(AUTHORIZATION_SERVER_OPTIONS).toValue(authorizationServerOptions);
    Container.bind<OAuth2SessionService>(SESSION_SERVICE).toValue(sessionServiceMock);
    Container.bind<OAuth2UserService>(USER_SERVICE).toValue(userServiceMock);
    Container.bind(LoginInteractionType).toSelf().asSingleton();

    interactionType = Container.resolve(LoginInteractionType);
  });

  afterEach(() => {
    Container.clear();
  });

  describe('name', () => {
    it('should have "login" as its name.', () => {
      expect(interactionType.name).toBe('login');
    });
  });

  describe('handleContext()', () => {
    let parameters: LoginContextInteractionRequest;

    beforeEach(() => {
      parameters = { interaction_type: 'login', login_challenge: '' };
    });

    it('should throw when the parameter "login_challenge" is not provided.', async () => {
      Reflect.deleteProperty(parameters, 'login_challenge');

      await expect(interactionType.handleContext(parameters)).rejects.toThrow(
        new InvalidRequestException({ error_description: 'Invalid parameter "login_challenge".' })
      );
    });

    it('should throw when no session is found.', async () => {
      sessionServiceMock.findOne.mockResolvedValueOnce(null);

      await expect(interactionType.handleContext(parameters)).rejects.toThrow(
        new AccessDeniedException({ error_description: 'Invalid Session.' })
      );
    });

    it('should throw when the session is expired.', async () => {
      sessionServiceMock.findOne.mockResolvedValueOnce(<OAuth2Session>{
        id: 'session_id',
        expiresAt: new Date(Date.now() - 3600000),
      });

      await expect(interactionType.handleContext(parameters)).rejects.toThrow(
        new AccessDeniedException({ error_description: 'Expired Session.' })
      );
    });

    it('should return a valid first time login context response.', async () => {
      const sessionParams = <AuthorizationRequest>{
        response_type: 'code',
        client_id: 'client_id',
        redirect_uri: 'https://client.example.com/callback',
        scope: 'foo bar baz',
        state: 'client_state',
        response_mode: 'query',
      };

      sessionServiceMock.findOne.mockResolvedValueOnce(<OAuth2Session>{
        id: 'session_id',
        parameters: sessionParams,
        client: { id: 'client_id' },
        user: null,
      });

      const params = new URLSearchParams(sessionParams);

      await expect(interactionType.handleContext(parameters)).resolves.toMatchObject<LoginContextInteractionResponse>({
        skip: false,
        request_url: `https://server.example.com/oauth/authorize?${params.toString()}`,
        client: <OAuth2Client>{ id: 'client_id' },
        context: {},
      });
    });

    it('should return a valid subsequent login context response.', async () => {
      const sessionParams = <AuthorizationRequest>{
        response_type: 'code',
        client_id: 'client_id',
        redirect_uri: 'https://client.example.com/callback',
        scope: 'foo bar baz',
        state: 'client_state',
        response_mode: 'query',
      };

      sessionServiceMock.findOne.mockResolvedValueOnce(<OAuth2Session>{
        id: 'session_id',
        parameters: sessionParams,
        client: { id: 'client_id' },
        user: { id: 'user_id' },
      });

      const params = new URLSearchParams(sessionParams);

      await expect(interactionType.handleContext(parameters)).resolves.toMatchObject<LoginContextInteractionResponse>({
        skip: true,
        request_url: `https://server.example.com/oauth/authorize?${params.toString()}`,
        client: <OAuth2Client>{ id: 'client_id' },
        context: {},
      });
    });
  });

  describe('handleDecision()', () => {
    let parameters: LoginDecisionInteractionRequest;

    beforeEach(() => {
      parameters = { interaction_type: 'login', login_challenge: '', decision: '' };
    });

    it('should throw when the parameter "login_challenge" is not provided.', async () => {
      Reflect.deleteProperty(parameters, 'login_challenge');

      await expect(interactionType.handleDecision(parameters)).rejects.toThrow(
        new InvalidRequestException({ error_description: 'Invalid parameter "login_challenge".' })
      );
    });

    it('should throw when the parameter "decision" is not provided.', async () => {
      Reflect.deleteProperty(parameters, 'decision');

      await expect(interactionType.handleDecision(parameters)).rejects.toThrow(
        new InvalidRequestException({ error_description: 'Invalid parameter "decision".' })
      );
    });

    it('should throw when no session is found.', async () => {
      sessionServiceMock.findOne.mockResolvedValueOnce(null);

      await expect(interactionType.handleDecision(parameters)).rejects.toThrow(
        new AccessDeniedException({ error_description: 'Invalid Session.' })
      );
    });

    it('should throw when the session is expired.', async () => {
      sessionServiceMock.findOne.mockResolvedValueOnce(<OAuth2Session>{
        id: 'session_id',
        expiresAt: new Date(Date.now() - 3600000),
      });

      await expect(interactionType.handleDecision(parameters)).rejects.toThrow(
        new AccessDeniedException({ error_description: 'Expired Session.' })
      );
    });

    it('should throw when providing an invalid decision.', async () => {
      Reflect.set(parameters, 'decision', 'unknown');

      sessionServiceMock.findOne.mockResolvedValueOnce(<OAuth2Session>{ id: 'session_id' });

      await expect(interactionType.handleDecision(parameters)).rejects.toThrow(
        new InvalidRequestException({ error_description: 'Unsupported decision "unknown".' })
      );
    });

    it('should throw when the parameter "subject" is not provided.', async () => {
      Reflect.set(parameters, 'decision', 'accept');

      sessionServiceMock.findOne.mockResolvedValueOnce(<OAuth2Session>{ id: 'session_id' });

      await expect(interactionType.handleDecision(parameters)).rejects.toThrow(
        new InvalidRequestException({ error_description: 'Invalid parameter "subject".' })
      );
    });

    it('should throw when no user is found.', async () => {
      Reflect.set(parameters, 'decision', 'accept');
      Reflect.set(parameters, 'subject', 'user_id');

      sessionServiceMock.findOne.mockResolvedValueOnce(<OAuth2Session>{ id: 'session_id' });
      userServiceMock.findOne.mockResolvedValueOnce(null);

      await expect(interactionType.handleDecision(parameters)).rejects.toThrow(
        new AccessDeniedException({ error_description: 'Invalid User.' })
      );
    });

    it('should return a valid login accept decision interaction response.', async () => {
      Reflect.set(parameters, 'decision', 'accept');
      Reflect.set(parameters, 'subject', 'user_id');

      const sessionParams = <AuthorizationRequest>{
        response_type: 'code',
        client_id: 'client_id',
        redirect_uri: 'https://client.example.com/callback',
        scope: 'foo bar baz',
        state: 'client_state',
        response_mode: 'query',
      };

      const session = <OAuth2Session>{ id: 'session_id', parameters: sessionParams };

      sessionServiceMock.findOne.mockResolvedValueOnce(session);

      userServiceMock.findOne.mockResolvedValueOnce({ id: 'user_id' });

      const params = new URLSearchParams(sessionParams);

      await expect(interactionType.handleDecision(parameters)).resolves.toMatchObject<LoginDecisionInteractionResponse>(
        { redirect_to: `https://server.example.com/oauth/authorize?${params.toString()}` }
      );

      expect(session.user).toStrictEqual({ id: 'user_id' });
      expect(sessionServiceMock.save).toHaveBeenCalledTimes(1);
    });

    it('should throw when the parameter "error" is not provided.', async () => {
      Reflect.set(parameters, 'decision', 'deny');

      sessionServiceMock.findOne.mockResolvedValueOnce(<OAuth2Session>{ id: 'session_id' });

      await expect(interactionType.handleDecision(parameters)).rejects.toThrow(
        new InvalidRequestException({ error_description: 'Invalid parameter "error".' })
      );
    });

    it('should throw when the parameter "error_description" is not provided.', async () => {
      Reflect.set(parameters, 'decision', 'deny');
      Reflect.set(parameters, 'error', 'custom_error');

      Reflect.deleteProperty(parameters, 'error_description');

      sessionServiceMock.findOne.mockResolvedValueOnce(<OAuth2Session>{ id: 'session_id' });

      await expect(interactionType.handleDecision(parameters)).rejects.toThrow(
        new InvalidRequestException({ error_description: 'Invalid parameter "error_description".' })
      );
    });

    it('should return a valid login deny decision interaction response.', async () => {
      Reflect.set(parameters, 'decision', 'deny');
      Reflect.set(parameters, 'error', 'custom_error');
      Reflect.set(parameters, 'error_description', 'Custom error description.');

      sessionServiceMock.findOne.mockResolvedValueOnce(<OAuth2Session>{ id: 'session_id' });

      userServiceMock.findOne.mockResolvedValueOnce({ id: 'user_id' });

      const params = new URLSearchParams({ error: parameters.error, error_description: parameters.error_description });

      await expect(interactionType.handleDecision(parameters)).resolves.toMatchObject<LoginDecisionInteractionResponse>(
        { redirect_to: `https://server.example.com/oauth/error?${params.toString()}` }
      );

      expect(sessionServiceMock.remove).toHaveBeenCalledTimes(1);
    });
  });
});
