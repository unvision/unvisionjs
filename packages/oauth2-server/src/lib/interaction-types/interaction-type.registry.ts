import { Constructor } from '@unvision/di';

import { ConsentInteractionType } from './consent.interaction-type';
import { InteractionType } from './interaction-type';
import { LoginInteractionType } from './login.interaction-type';

export const interactionTypeRegistry: Record<string, Constructor<InteractionType>> = {
  consent: ConsentInteractionType,
  login: LoginInteractionType,
};
