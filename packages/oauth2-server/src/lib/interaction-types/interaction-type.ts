import { InteractionRequest } from '../types/interaction-request';

/**
 * Interface of an Interaction Type.
 */
export interface InteractionType {
  /**
   * Name of the Interaction Type.
   */
  readonly name: string;

  /**
   * Handles the Context Flow of the Interaction.
   *
   * @param parameters Parameters of the Interaction Context Request.
   * @returns Parameters of the Interaction Context Response.
   */
  handleContext(parameters: InteractionRequest): Promise<Record<string, any>>;

  /**
   * Handles the Decision Flow of the Interaction.
   *
   * @param parameters Parameters of the Interaction Decision Request.
   * @returns Parameters of the Interaction Decision Response.
   */
  handleDecision(parameters: InteractionRequest): Promise<Record<string, any>>;
}
