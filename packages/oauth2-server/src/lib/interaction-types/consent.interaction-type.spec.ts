import { DependencyInjectionContainer } from '@unvision/di';

import { AuthorizationServerOptions } from '../authorization-server/options/authorization-server.options';
import { OAuth2Client } from '../entities/oauth2-client.entity';
import { OAuth2Consent } from '../entities/oauth2-consent.entity';
import { AccessDeniedException } from '../exceptions/access-denied.exception';
import { InvalidRequestException } from '../exceptions/invalid-request.exception';
import { InvalidScopeException } from '../exceptions/invalid-scope.exception';
import { AUTHORIZATION_SERVER_OPTIONS, CONSENT_SERVICE } from '../metadata/metadata.keys';
import { OAuth2ConsentService } from '../services/oauth2-consent.service';
import { AuthorizationRequest } from '../types/authorization-request';
import { ConsentContextInteractionRequest } from '../types/consent-context.interaction-request';
import { ConsentContextInteractionResponse } from '../types/consent-context.interaction-response';
import { ConsentDecisionInteractionRequest } from '../types/consent-decision.interaction-request';
import { ConsentDecisionInteractionResponse } from '../types/consent-decision.interaction-response';
import { ConsentInteractionType } from './consent.interaction-type';

describe('Consent Interaction Type', () => {
  let interactionType: ConsentInteractionType;

  const consentServiceMock = jest.mocked<OAuth2ConsentService>({
    create: jest.fn(),
    findOne: jest.fn(),
    remove: jest.fn(),
    save: jest.fn(),
  });

  const authorizationServerOptions = <AuthorizationServerOptions>{ issuer: 'https://server.example.com' };

  beforeEach(() => {
    const container = new DependencyInjectionContainer();

    container.bind<AuthorizationServerOptions>(AUTHORIZATION_SERVER_OPTIONS).toValue(authorizationServerOptions);
    container.bind<OAuth2ConsentService>(CONSENT_SERVICE).toValue(consentServiceMock);
    container.bind(ConsentInteractionType).toSelf().asSingleton();

    interactionType = container.resolve(ConsentInteractionType);
  });

  describe('name', () => {
    it('should have "consent" as its name.', () => {
      expect(interactionType.name).toBe('consent');
    });
  });

  describe('handleContext()', () => {
    let parameters: ConsentContextInteractionRequest;

    beforeEach(() => {
      parameters = { interaction_type: 'consent', consent_challenge: '' };
    });

    it('should throw when the parameter "consent_challenge" is not provided.', async () => {
      Reflect.deleteProperty(parameters, 'consent_challenge');

      await expect(interactionType.handleContext(parameters)).rejects.toThrow(
        new InvalidRequestException({ error_description: 'Invalid parameter "consent_challenge".' })
      );
    });

    it('should throw when no consent is found.', async () => {
      consentServiceMock.findOne.mockResolvedValueOnce(null);

      await expect(interactionType.handleContext(parameters)).rejects.toThrow(
        new AccessDeniedException({ error_description: 'Invalid Consent.' })
      );
    });

    it('should throw when the consent is expired.', async () => {
      consentServiceMock.findOne.mockResolvedValueOnce(<OAuth2Consent<AuthorizationRequest>>{
        id: 'consent_id',
        expiresAt: new Date(Date.now() - 3600000),
      });

      await expect(interactionType.handleContext(parameters)).rejects.toThrow(
        new AccessDeniedException({ error_description: 'Expired Consent.' })
      );
    });

    it('should return a valid first time consent context response.', async () => {
      const consentParams = <AuthorizationRequest>{
        response_type: 'code',
        client_id: 'client_id',
        redirect_uri: 'https://client.example.com/callback',
        scope: 'foo bar baz',
        state: 'client_state',
        response_mode: 'query',
      };

      consentServiceMock.findOne.mockResolvedValueOnce(<OAuth2Consent<AuthorizationRequest>>{
        id: 'consent_id',
        scopes: <string[]>[],
        parameters: consentParams,
        client: { id: 'client_id' },
        user: { id: 'user_id' },
        session: { id: 'session_id' },
      });

      const params = new URLSearchParams(consentParams);

      await expect(interactionType.handleContext(parameters)).resolves.toMatchObject<ConsentContextInteractionResponse>(
        {
          skip: false,
          requested_scope: 'foo bar baz',
          subject: 'user_id',
          request_url: `https://server.example.com/oauth/authorize?${params.toString()}`,
          login_challenge: 'session_id',
          client: <OAuth2Client>{ id: 'client_id' },
          context: {},
        }
      );
    });
  });

  describe('handleDecision()', () => {
    let parameters: ConsentDecisionInteractionRequest;

    beforeEach(() => {
      parameters = { interaction_type: 'consent', consent_challenge: '', decision: '' };
    });

    it('should throw when the parameter "consent_challenge" is not provided.', async () => {
      Reflect.deleteProperty(parameters, 'consent_challenge');

      await expect(interactionType.handleDecision(parameters)).rejects.toThrow(
        new InvalidRequestException({ error_description: 'Invalid parameter "consent_challenge".' })
      );
    });

    it('should throw when the parameter "decision" is not provided.', async () => {
      Reflect.deleteProperty(parameters, 'decision');

      await expect(interactionType.handleDecision(parameters)).rejects.toThrow(
        new InvalidRequestException({ error_description: 'Invalid parameter "decision".' })
      );
    });

    it('should throw when no consent is found.', async () => {
      consentServiceMock.findOne.mockResolvedValueOnce(null);

      await expect(interactionType.handleDecision(parameters)).rejects.toThrow(
        new AccessDeniedException({ error_description: 'Invalid Consent.' })
      );
    });

    it('should throw when the consent is expired.', async () => {
      consentServiceMock.findOne.mockResolvedValueOnce(<OAuth2Consent<AuthorizationRequest>>{
        id: 'consent_id',
        expiresAt: new Date(Date.now() - 3600000),
      });

      await expect(interactionType.handleDecision(parameters)).rejects.toThrow(
        new AccessDeniedException({ error_description: 'Expired Consent.' })
      );
    });

    it('should throw when providing an invalid decision.', async () => {
      Reflect.set(parameters, 'decision', 'unknown');

      consentServiceMock.findOne.mockResolvedValueOnce(<OAuth2Consent<AuthorizationRequest>>{ id: 'consent_id' });

      await expect(interactionType.handleDecision(parameters)).rejects.toThrow(
        new InvalidRequestException({ error_description: 'Unsupported decision "unknown".' })
      );
    });

    it('should throw when the parameter "grant_scope" is not provided.', async () => {
      Reflect.set(parameters, 'decision', 'accept');

      consentServiceMock.findOne.mockResolvedValueOnce(<OAuth2Consent<AuthorizationRequest>>{ id: 'consent_id' });

      await expect(interactionType.handleDecision(parameters)).rejects.toThrow(
        new InvalidRequestException({ error_description: 'Invalid parameter "grant_scope".' })
      );
    });

    it('should throw when granting a scope not requested by the client.', async () => {
      Reflect.set(parameters, 'decision', 'accept');
      Reflect.set(parameters, 'grant_scope', 'foo bar qux');

      consentServiceMock.findOne.mockResolvedValueOnce(<OAuth2Consent<AuthorizationRequest>>{
        id: 'consent_id',
        parameters: { scope: 'foo bar baz' },
      });

      await expect(interactionType.handleDecision(parameters)).rejects.toThrow(
        new InvalidScopeException({ error_description: 'The granted scope was not requested by the Client.' })
      );
    });

    it('should return a valid consent accept decision interaction response.', async () => {
      Reflect.set(parameters, 'decision', 'accept');
      Reflect.set(parameters, 'grant_scope', 'foo bar');

      const consentParams = <AuthorizationRequest>{
        response_type: 'code',
        client_id: 'client_id',
        redirect_uri: 'https://client.example.com/callback',
        scope: 'foo bar baz',
        state: 'client_state',
        response_mode: 'query',
      };

      const consent = <OAuth2Consent<AuthorizationRequest>>{ id: 'consent_id', parameters: consentParams };

      consentServiceMock.findOne.mockResolvedValueOnce(consent);

      const params = new URLSearchParams(consentParams);

      await expect(
        interactionType.handleDecision(parameters)
      ).resolves.toMatchObject<ConsentDecisionInteractionResponse>({
        redirect_to: `https://server.example.com/oauth/authorize?${params.toString()}`,
      });

      expect(consent.scopes).toEqual<string[]>(['foo', 'bar']);
      expect(consentServiceMock.save).toHaveBeenCalledTimes(1);
    });

    it('should throw when the parameter "error" is not provided.', async () => {
      Reflect.set(parameters, 'decision', 'deny');

      consentServiceMock.findOne.mockResolvedValueOnce(<OAuth2Consent<AuthorizationRequest>>{ id: 'consent_id' });

      await expect(interactionType.handleDecision(parameters)).rejects.toThrow(
        new InvalidRequestException({ error_description: 'Invalid parameter "error".' })
      );
    });

    it('should throw when the parameter "error_description" is not provided.', async () => {
      Reflect.set(parameters, 'decision', 'deny');
      Reflect.set(parameters, 'error', 'custom_error');

      Reflect.deleteProperty(parameters, 'error_description');

      consentServiceMock.findOne.mockResolvedValueOnce(<OAuth2Consent<AuthorizationRequest>>{ id: 'consent_id' });

      await expect(interactionType.handleDecision(parameters)).rejects.toThrow(
        new InvalidRequestException({ error_description: 'Invalid parameter "error_description".' })
      );
    });

    it('should return a valid login deny decision interaction response.', async () => {
      Reflect.set(parameters, 'decision', 'deny');
      Reflect.set(parameters, 'error', 'custom_error');
      Reflect.set(parameters, 'error_description', 'Custom error description.');

      consentServiceMock.findOne.mockResolvedValueOnce(<OAuth2Consent<AuthorizationRequest>>{ id: 'consent_id' });

      const params = new URLSearchParams({ error: parameters.error, error_description: parameters.error_description });

      await expect(
        interactionType.handleDecision(parameters)
      ).resolves.toMatchObject<ConsentDecisionInteractionResponse>({
        redirect_to: `https://server.example.com/oauth/error?${params.toString()}`,
      });

      expect(consentServiceMock.remove).toHaveBeenCalledTimes(1);
    });
  });
});
