import { Injectable } from '@unvision/di';

import { Request, RequestHandler, Response, Router } from 'express';
import { OutgoingHttpHeader } from 'http';

import { AuthorizationServer } from '../../authorization-server/authorization-server';
import { Request as OAuth2Request } from '../../http/request';
import { Response as OAuth2Response } from '../../http/response';

type HttpMethod = 'get' | 'post';

/**
 * Integration of OAuth 2.0 Authorization Server on ExpressJS.
 */
@Injectable()
export class ExpressProvider extends AuthorizationServer {
  /**
   * Express Router used to define the OAuth 2.0 Endpoints.
   */
  public readonly router: Router = Router();

  /**
   * Bootstraps the Express Authorization Server.
   */
  public async bootstrap(): Promise<void> {
    this.endpoints.forEach((endpoint) => {
      endpoint.httpMethods.forEach((method) => {
        this.router[<HttpMethod>method](endpoint.path, this._mountEndpoint(endpoint.name));
      });
    });
  }

  /**
   * Mounts the OAuth 2.0 Endpoint as an ExpressJS Route.
   *
   * @param endpoint Name of the Endpoint.
   */
  private _mountEndpoint(endpoint: string): RequestHandler {
    return async (request: Request, response: Response): Promise<void> => {
      const oauth2Request = this._createOAuth2Request(request);
      const oauth2Response = await this.endpoint(endpoint, oauth2Request);

      return this._parseOAuth2Response(oauth2Response, response);
    };
  }

  /**
   * Creates an OAuth 2.0 Http Request from the provided Express Request.
   *
   * @param request Express Request.
   * @returns OAuth 2.0 Http Request.
   */
  private _createOAuth2Request(request: Request): OAuth2Request {
    return {
      body: request.body,
      cookies: request.cookies,
      headers: request.headers,
      method: request.method.toLowerCase(),
      path: request.path,
      query: request.query,
    };
  }

  /**
   * Parses the data of the OAuth 2.0 Http Response into the Express Response.
   *
   * @param oauth2Response OAuth 2.0 Http Response.
   * @param response Express Response.
   */
  private _parseOAuth2Response(oauth2Response: OAuth2Response, response: Response): void {
    const { body, cookies, headers, statusCode } = oauth2Response;

    Object.entries(headers).forEach(([name, value]) => response.setHeader(name, <OutgoingHttpHeader>value));
    Object.entries(cookies).forEach(([name, value]) => response.cookie(name, value));

    response.status(statusCode).send(body);
  }
}
