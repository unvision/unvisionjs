import { Constructor } from '@unvision/di';

import { Router } from 'express';

import { AuthorizationServerMetadata } from '../../metadata/authorization-server-metadata';
import { AuthorizationServerMetadataOptions } from '../../metadata/authorization-server-metadata.options';
import { AuthorizationServerFactory } from '../../metadata/authorization-server.factory';
import { ExpressProvider } from './express.provider';

/**
 * Enables the OAuth 2.0 Authorization Server into the Express Application.
 *
 * @param options Configuration options of the Authorization Server.
 */
export async function expressProvider(options: AuthorizationServerMetadataOptions): Promise<Router> {
  const Provider = <Constructor<ExpressProvider>>(
    Reflect.decorate([AuthorizationServerMetadata(options)], class extends ExpressProvider {})
  );

  const provider = await AuthorizationServerFactory.create(Provider);

  await provider.bootstrap();

  return provider.router;
}
