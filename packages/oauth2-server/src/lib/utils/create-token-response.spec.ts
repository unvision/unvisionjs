import { OAuth2AccessToken } from '../entities/oauth2-access-token.entity';
import { OAuth2RefreshToken } from '../entities/oauth2-refresh-token.entity';
import { TokenResponse } from '../types/token-response';
import { createTokenResponse } from './create-token-response';

describe('createTokenResponse()', () => {
  it('should return a token response based on the data of the provided access token.', () => {
    expect(
      createTokenResponse(<OAuth2AccessToken>{
        token: 'access_token',
        scopes: ['foo', 'bar'],
        expiresAt: new Date(Date.now() + 3600000),
      })
    ).toStrictEqual<TokenResponse>({
      access_token: 'access_token',
      token_type: 'Bearer',
      expires_in: 3600,
      scope: 'foo bar',
      refresh_token: undefined,
    });
  });

  it('should return a token response based on the data of the provided access token and refresh token.', () => {
    expect(
      createTokenResponse(
        <OAuth2AccessToken>{ token: 'access_token', scopes: ['foo', 'bar'], expiresAt: new Date(Date.now() + 3600000) },
        <OAuth2RefreshToken>{ token: 'refresh_token' }
      )
    ).toStrictEqual<TokenResponse>({
      access_token: 'access_token',
      token_type: 'Bearer',
      expires_in: 3600,
      scope: 'foo bar',
      refresh_token: 'refresh_token',
    });
  });
});
