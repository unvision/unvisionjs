import { OAuth2AccessToken } from '../entities/oauth2-access-token.entity';
import { OAuth2RefreshToken } from '../entities/oauth2-refresh-token.entity';
import { TokenResponse } from '../types/token-response';

/**
 * Returns a formatted Token Response based on the provided Access Token and optional Refresh Token.
 *
 * @param accessToken Access Token issued to the Client.
 * @param refreshToken Refresh Token issued to the Client.
 * @returns Formatted Token Response.
 */
export function createTokenResponse(accessToken: OAuth2AccessToken, refreshToken?: OAuth2RefreshToken): TokenResponse {
  return {
    access_token: accessToken.token,
    token_type: 'Bearer',
    expires_in: Math.ceil((accessToken.expiresAt.getTime() - Date.now()) / 1000),
    scope: accessToken.scopes.join(' '),
    refresh_token: refreshToken?.token,
  };
}
