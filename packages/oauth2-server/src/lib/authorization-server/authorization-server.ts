import { Injectable, InjectAll } from '@unvision/di';

import { Endpoint } from '../endpoints/endpoint';
import { Request } from '../http/request';
import { Response } from '../http/response';
import { ENDPOINT } from '../metadata/metadata.keys';

/**
 * Abstract Base Class for the OAuth 2.0 Authorization Server.
 */
@Injectable()
export abstract class AuthorizationServer {
  /**
   * Endpoints of the Authorization Server.
   */
  @InjectAll(ENDPOINT)
  protected readonly endpoints!: Endpoint[];

  /**
   * Creates an HTTP Response for the requested Endpoint.
   *
   * @param name Name of the Endpoint.
   * @param request HTTP Request.
   * @returns HTTP Response.
   */
  public async endpoint(name: string, request: Request): Promise<Response> {
    const endpoint = this.endpoints.find((endpoint) => endpoint.name === name);

    if (endpoint === undefined) {
      throw new TypeError(`Unsupported Endpoint "${name}".`);
    }

    return await endpoint.handle(request);
  }
}
