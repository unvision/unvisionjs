import { SupportedJsonWebSignatureAlgorithm } from '@unvision/jose';

import { UserInteractionOptions } from './user-interaction.options';

/**
 * Configuration Parameters to customize the behaviour of the Authorization Server.
 */
export interface AuthorizationServerOptions {
  /**
   * Identifier of the Authorization Server's Issuer.
   */
  readonly issuer: string;

  /**
   * Scopes registered at the Authorization Server.
   */
  readonly scopes: string[];

  /**
   * Client Authentication Methods registered at the Authorization Server.
   *
   * @default ['client_secret_basic']
   */
  readonly clientAuthenticationMethods: string[];

  /**
   * JSON Web Signature Algoithms for Client Authentication registered at the Authorization Server.
   */
  readonly clientAuthenticationSignatureAlgorithms: SupportedJsonWebSignatureAlgorithm[];

  /**
   * Grant Types registered at the Authorization Server.
   *
   * @default ['authorization_code']
   */
  readonly grantTypes: string[];

  /**
   * Response Types registered at the Authorization Server.
   *
   * @default ['code']
   */
  readonly responseTypes: string[];

  /**
   * Response Modes registered at the Authorization Server.
   *
   * @default ['query']
   */
  readonly responseModes: string[];

  /**
   * PKCE Methods registered at the Authorization Server.
   *
   * @default ['S256']
   */
  readonly pkceMethods: string[];

  /**
   * Defines the Parameters of the User Interaction.
   */
  readonly userInteraction?: UserInteractionOptions;

  /**
   * Enables Refresh Token Rotation on the Authorization Server.
   *
   * @default false
   */
  readonly enableRefreshTokenRotation: boolean;

  /**
   * Enables Access Token Revocation on the Authorization Server.
   *
   * @default true
   */
  readonly enableAccessTokenRevocation: boolean;

  /**
   * Enables Refresh Token Introspection on the Authorization Server.
   *
   * @default false
   */
  readonly enableRefreshTokenIntrospection: boolean;
}
