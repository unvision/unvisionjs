import { AuthorizationRequest } from '../types/authorization-request';
import { OAuth2Client } from './oauth2-client.entity';
import { OAuth2User } from './oauth2-user.entity';

/**
 * OAuth 2.0 Session Entity.
 */
export interface OAuth2Session {
  /**
   * Identifier of the Session.
   */
  readonly id: string;

  /**
   * Parameters of the Authorization Request.
   */
  readonly parameters: AuthorizationRequest;

  /**
   * Creation Date of the Session.
   */
  readonly createdAt: Date;

  /**
   * Expiration Date of the Session.
   *
   * *note: a **null** or **undefined** value indicates that the grant does not expire.*
   */
  readonly expiresAt?: Date | null;

  /**
   * Client requesting authorization.
   */
  readonly client: OAuth2Client;

  /**
   * Authenticated End User.
   */
  user: OAuth2User | null;
}
