import { AuthorizationRequest } from '../types/authorization-request';
import { OAuth2Client } from './oauth2-client.entity';
import { OAuth2Session } from './oauth2-session.entity';
import { OAuth2User } from './oauth2-user.entity';

/**
 * OAuth 2.0 Consent Entity.
 */
export interface OAuth2Consent<TParams extends AuthorizationRequest> extends Record<string, any> {
  /**
   * Identifier of the Consent.
   */
  readonly id: string;

  /**
   * Scopes granted by the Authenticated End User.
   */
  scopes: string[];

  /**
   * Parameters of the Authorization Request.
   */
  readonly parameters: TParams;

  /**
   * Creation Date of the Consent.
   */
  readonly createdAt: Date;

  /**
   * Expiration Date of the Consent.
   *
   * *note: a **null** or **undefined** value indicates that the consent does not expire.*
   */
  readonly expiresAt?: Date | null;

  /**
   * Client authorized by the Authenticated End User.
   */
  readonly client: OAuth2Client;

  /**
   * Authenticated End User.
   */
  readonly user: OAuth2User;

  /**
   * Session containing the Login Interaction result.
   */
  readonly session: OAuth2Session;
}
