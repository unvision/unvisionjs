import { OAuth2Client } from './oauth2-client.entity';
import { OAuth2User } from './oauth2-user.entity';

/**
 * OAuth 2.0 Access Token Entity.
 */
export interface OAuth2AccessToken extends Record<string, any> {
  /**
   * Identifier of the Access Token.
   */
  readonly token: string;

  /**
   * Scopes granted to the Client.
   */
  readonly scopes: string[];

  /**
   * Revocation status of the Access Token.
   */
  isRevoked: boolean;

  /**
   * Issuance Date of the Access Token.
   */
  readonly issuedAt: Date;

  /**
   * Expiration Date of the Access Token.
   */
  readonly expiresAt: Date;

  /**
   * Date when the Access Token will become valid.
   */
  readonly validAfter: Date;

  /**
   * Client that requested the Access Token.
   */
  readonly client: OAuth2Client;

  /**
   * End User that granted authorization to the Client.
   */
  readonly user?: OAuth2User | null;
}
