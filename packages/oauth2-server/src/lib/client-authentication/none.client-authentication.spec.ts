import { DependencyInjectionContainer } from '@unvision/di';

import { OAuth2Client } from '../entities/oauth2-client.entity';
import { InvalidClientException } from '../exceptions/invalid-client.exception';
import { Request } from '../http/request';
import { CLIENT_SERVICE } from '../metadata/metadata.keys';
import { OAuth2ClientService } from '../services/oauth2-client.service';
import { NoneClientAuthentication } from './none.client-authentication';

describe('None Client Authentication Method', () => {
  let clientAuthentication: NoneClientAuthentication;

  const clientServiceMock = jest.mocked<OAuth2ClientService>({
    findOne: jest.fn(),
  });

  beforeEach(() => {
    const container = new DependencyInjectionContainer();

    container.bind<OAuth2ClientService>(CLIENT_SERVICE).toValue(clientServiceMock);
    container.bind(NoneClientAuthentication).toSelf().asSingleton();

    clientAuthentication = container.resolve(NoneClientAuthentication);
  });

  describe('name', () => {
    it('should have "none" as its name.', () => {
      expect(clientAuthentication.name).toBe('none');
    });
  });

  describe('hasBeenRequested()', () => {
    const methodRequests: [Record<string, any>, boolean][] = [
      [{}, false],
      [{ client_id: '' }, true],
      [{ client_id: 'foo' }, true],
      [{ client_secret: '' }, false],
      [{ client_secret: 'bar' }, false],
      [{ client_id: '', client_secret: '' }, false],
      [{ client_id: 'foo', client_secret: '' }, false],
      [{ client_id: '', client_secret: 'bar' }, false],
      [{ client_id: 'foo', client_secret: 'bar' }, false],
    ];

    it.each(methodRequests)('should check if the authentication method has beed requested.', (body, expected) => {
      const request: Request = { body, cookies: {}, headers: {}, method: 'POST', path: '/oauth/token', query: {} };

      expect(clientAuthentication.hasBeenRequested(request)).toBe(expected);
    });
  });

  describe('authenticate()', () => {
    let request: Request;

    beforeEach(() => {
      request = {
        body: { client_id: 'client_id' },
        cookies: {},
        headers: {},
        method: 'POST',
        path: '/oauth/token',
        query: {},
      };
    });

    it('should reject when a client is not found.', async () => {
      clientServiceMock.findOne.mockResolvedValueOnce(null);

      await expect(clientAuthentication.authenticate(request)).rejects.toThrow(
        new InvalidClientException({ error_description: 'Invalid Credentials.' })
      );
    });

    it('should reject a client with a secret.', async () => {
      clientServiceMock.findOne.mockResolvedValueOnce(<OAuth2Client>{ id: 'client_id', secret: 'client_secret' });

      await expect(clientAuthentication.authenticate(request)).rejects.toThrow(
        new InvalidClientException({
          error_description: 'This Client is not allowed to use the Authentication Method "none".',
        })
      );
    });

    it('should reject a client not authorized to use this authentication method.', async () => {
      clientServiceMock.findOne.mockResolvedValueOnce(<OAuth2Client>{
        id: 'client_id',
        authenticationMethod: 'unknown',
      });

      await expect(clientAuthentication.authenticate(request)).rejects.toThrow(
        new InvalidClientException({
          error_description: 'This Client is not allowed to use the Authentication Method "none".',
        })
      );
    });

    it('should return an instance of a client.', async () => {
      const client = <OAuth2Client>{ id: 'client_id', authenticationMethod: 'none' };

      clientServiceMock.findOne.mockResolvedValueOnce(client);

      await expect(clientAuthentication.authenticate(request)).resolves.toBe(client);
    });
  });
});
