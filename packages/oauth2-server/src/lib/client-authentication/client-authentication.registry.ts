import { Constructor } from '@unvision/di';

import { ClientAuthentication } from './client-authentication';
import { ClientSecretBasicClientAuthentication } from './client-secret-basic.client-authentication';
import { ClientSecretJwtClientAuthentication } from './client-secret-jwt.client-authentication';
import { ClientSecretPostClientAuthentication } from './client-secret-post.client-authentication';
import { NoneClientAuthentication } from './none.client-authentication';
import { PrivateKeyJwtClientAuthentication } from './private-key-jwt.client-authentication';

export const clientAuthenticationRegistry: Record<string, Constructor<ClientAuthentication>> = {
  client_secret_basic: ClientSecretBasicClientAuthentication,
  client_secret_jwt: ClientSecretJwtClientAuthentication,
  client_secret_post: ClientSecretPostClientAuthentication,
  none: NoneClientAuthentication,
  private_key_jwt: PrivateKeyJwtClientAuthentication,
};
