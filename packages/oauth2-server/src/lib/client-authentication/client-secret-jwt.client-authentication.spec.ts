import { DependencyInjectionContainer } from '@unvision/di';
import { JsonWebKeyParameters, SupportedJsonWebSignatureAlgorithm } from '@unvision/jose';

import { AuthorizationServerOptions } from '../authorization-server/options/authorization-server.options';
import { OAuth2Client } from '../entities/oauth2-client.entity';
import { InvalidClientException } from '../exceptions/invalid-client.exception';
import { AUTHORIZATION_SERVER_OPTIONS, CLIENT_SERVICE } from '../metadata/metadata.keys';
import { OAuth2ClientService } from '../services/oauth2-client.service';
import { ClientSecretJwtClientAuthentication } from './client-secret-jwt.client-authentication';

describe('Client Secret JWT Client Authentication Method', () => {
  let clientAuthentication: ClientSecretJwtClientAuthentication;

  const clientServiceMock = jest.mocked<OAuth2ClientService>({
    findOne: jest.fn(),
  });

  const authorizationServerOptions = <AuthorizationServerOptions>{};

  beforeEach(() => {
    const container = new DependencyInjectionContainer();

    container.bind<AuthorizationServerOptions>(AUTHORIZATION_SERVER_OPTIONS).toValue(authorizationServerOptions);
    container.bind<OAuth2ClientService>(CLIENT_SERVICE).toValue(clientServiceMock);
    container.bind(ClientSecretJwtClientAuthentication).toSelf().asSingleton();

    clientAuthentication = container.resolve(ClientSecretJwtClientAuthentication);
  });

  describe('algorithms', () => {
    it('should have \'["HS256", "HS384", "HS512"]\' as its value.', () => {
      expect(clientAuthentication['algorithms']).toEqual<SupportedJsonWebSignatureAlgorithm[]>([
        'HS256',
        'HS384',
        'HS512',
      ]);
    });
  });

  describe('name', () => {
    it('should have "client_secret_jwt" as its name.', () => {
      expect(clientAuthentication.name).toBe('client_secret_jwt');
    });
  });

  describe('getClientKey()', () => {
    it('should throw when the client does not have a secret.', async () => {
      const client = <OAuth2Client>{ id: 'client_id' };

      await expect(clientAuthentication['getClientKey'](client)).rejects.toThrow(
        new InvalidClientException({
          error_description: 'This Client is not allowed to use the Authentication Method "client_secret_jwt".',
        })
      );
    });

    it('should throw when the client secret is expired.', async () => {
      const client = <OAuth2Client>{
        id: 'client_id',
        secret: 'dZD9jxWOFEiSi-9AjOwmvJKaEJRUBbXl',
        secretExpiresAt: new Date(Date.now() - 3600000),
      };

      await expect(clientAuthentication['getClientKey'](client)).rejects.toThrow(
        new InvalidClientException({
          error_description: 'This Client is not allowed to use the Authentication Method "client_secret_jwt".',
        })
      );
    });

    it('should return a json web key based on the secret of the client.', async () => {
      const client = <OAuth2Client>{ id: 'client_id', secret: 'dZD9jxWOFEiSi-9AjOwmvJKaEJRUBbXl' };

      await expect(clientAuthentication['getClientKey'](client)).resolves.toMatchObject<JsonWebKeyParameters>({
        kty: 'oct',
        k: 'ZFpEOWp4V09GRWlTaS05QWpPd212SkthRUpSVUJiWGw',
      });
    });
  });
});
