import { OAuth2Client } from '../entities/oauth2-client.entity';
import { Request } from '../http/request';

/**
 * Interface of a Client Authentication Method.
 */
export interface ClientAuthentication {
  /**
   * Name of the Client Authentication Method.
   */
  readonly name: string;

  /**
   * Checks if the Client Authentication Method has been requested by the Client.
   *
   * @param request HTTP Request.
   */
  hasBeenRequested(request: Request): boolean;

  /**
   * Authenticates and returns the Client of the Request.
   *
   * @param request HTTP Request.
   * @returns Authenticated Client.
   */
  authenticate(request: Request): Promise<OAuth2Client>;
}
