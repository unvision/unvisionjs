import { Injectable, InjectAll } from '@unvision/di';

import { ClientAuthentication } from '../client-authentication/client-authentication';
import { OAuth2Client } from '../entities/oauth2-client.entity';
import { InvalidClientException } from '../exceptions/invalid-client.exception';
import { Request } from '../http/request';
import { CLIENT_AUTHENTICATION } from '../metadata/metadata.keys';

/**
 * Handler used to authenticate the Client of the OAuth 2.0 Request.
 */
@Injectable()
export class ClientAuthenticationHandler {
  /**
   * Instantiates a new Client Authentication Handler.
   *
   * @param clientAuthenticationMethods Client Authentication Methods supported by the Authorization Server.
   */
  public constructor(
    @InjectAll(CLIENT_AUTHENTICATION) private readonly clientAuthenticationMethods: ClientAuthentication[]
  ) {}

  /**
   * Authenticates the Client based on the Client Authentication Methods supported by the Authorization Server.
   *
   * @param request HTTP Request.
   * @returns Authenticated Client.
   */
  public async authenticate(request: Request): Promise<OAuth2Client> {
    const methods = this.clientAuthenticationMethods.filter((method) => method.hasBeenRequested(request));

    if (methods.length === 0) {
      throw new InvalidClientException({ error_description: 'No Client Authentication Method detected.' });
    }

    if (methods.length > 1) {
      throw new InvalidClientException({ error_description: 'Multiple Client Authentication Methods detected.' });
    }

    const [method] = methods;

    return await method!.authenticate(request);
  }
}
