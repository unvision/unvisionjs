import { DependencyInjectionContainer } from '@unvision/di';

import { AuthorizationServerOptions } from '../authorization-server/options/authorization-server.options';
import { OAuth2Client } from '../entities/oauth2-client.entity';
import { InvalidScopeException } from '../exceptions/invalid-scope.exception';
import { AUTHORIZATION_SERVER_OPTIONS } from '../metadata/metadata.keys';
import { ScopeHandler } from './scope.handler';

describe('Scope Handler', () => {
  let scopeHandler: ScopeHandler;

  const client = <OAuth2Client>{ scopes: ['foo', 'bar'] };

  const authorizationServerOptions = <AuthorizationServerOptions>{ scopes: ['foo', 'bar', 'baz', 'qux'] };

  beforeEach(() => {
    const container = new DependencyInjectionContainer();

    container.bind<AuthorizationServerOptions>(AUTHORIZATION_SERVER_OPTIONS).toValue(authorizationServerOptions);
    container.bind(ScopeHandler).toSelf().asSingleton();

    scopeHandler = container.resolve(ScopeHandler);
  });

  describe('checkRequestedScope()', () => {
    it('should not reject when not requesting any scope.', () => {
      expect(() => scopeHandler.checkRequestedScope()).not.toThrow();
    });

    it('should reject when requesting an unsupported Scope.', () => {
      expect(() => scopeHandler.checkRequestedScope('foo unknown qux')).toThrow(
        new InvalidScopeException({ error_description: 'Unsupported scope "unknown".' })
      );
    });

    it.each(['foo', 'bar baz', 'foo bar baz', 'foo baz bar'])(
      'should not reject when requesting supported scopes.',
      (scope) => {
        expect(() => scopeHandler.checkRequestedScope(scope)).not.toThrow();
      }
    );
  });

  describe('getAllowedScopes()', () => {
    it('should return the default scopes of the client when a scope is not requested.', () => {
      expect(scopeHandler.getAllowedScopes(client)).toEqual(expect.arrayContaining(['foo', 'bar']));
    });

    it("should return the requested scope from the client's allowed scopes.", () => {
      expect(scopeHandler.getAllowedScopes(client, 'foo')).toEqual(expect.arrayContaining(['foo']));
      expect(scopeHandler.getAllowedScopes(client, 'foo bar')).toEqual(expect.arrayContaining(['foo', 'bar']));
    });

    it('should restrict the requested scope to the ones allowed to the client.', () => {
      expect(scopeHandler.getAllowedScopes(client, 'qux bar')).toEqual(expect.arrayContaining(['bar']));
      expect(scopeHandler.getAllowedScopes(client, 'bar qux foo')).toEqual(expect.arrayContaining(['bar', 'foo']));
    });
  });
});
