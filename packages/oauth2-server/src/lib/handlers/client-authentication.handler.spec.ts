import { DependencyInjectionContainer } from '@unvision/di';

import { ClientAuthentication } from '../client-authentication/client-authentication';
import { OAuth2Client } from '../entities/oauth2-client.entity';
import { InvalidClientException } from '../exceptions/invalid-client.exception';
import { Request } from '../http/request';
import { CLIENT_AUTHENTICATION } from '../metadata/metadata.keys';
import { ClientAuthenticationHandler } from './client-authentication.handler';

describe('Client Authentication Handler', () => {
  let clientAuthenticationHandler: ClientAuthenticationHandler;
  let request: Request;

  const clientAuthenticationMethodsMocks = [
    jest.mocked<ClientAuthentication>({
      name: 'client_secret_basic',
      hasBeenRequested: jest.fn(),
      authenticate: jest.fn(),
    }),
    jest.mocked<ClientAuthentication>({
      name: 'client_secret_post',
      hasBeenRequested: jest.fn(),
      authenticate: jest.fn(),
    }),
    jest.mocked<ClientAuthentication>({
      name: 'none',
      hasBeenRequested: jest.fn(),
      authenticate: jest.fn(),
    }),
  ];

  beforeEach(() => {
    const container = new DependencyInjectionContainer();

    clientAuthenticationMethodsMocks.forEach((clientAuthentication) => {
      container.bind<ClientAuthentication>(CLIENT_AUTHENTICATION).toValue(clientAuthentication);
    });

    container.bind(ClientAuthenticationHandler).toSelf().asSingleton();

    clientAuthenticationHandler = container.resolve(ClientAuthenticationHandler);

    request = { body: {}, cookies: {}, headers: {}, method: 'POST', path: '/oauth/token', query: {} };
  });

  it('should reject not using a client authentication method.', async () => {
    clientAuthenticationMethodsMocks.forEach((method) => method.hasBeenRequested.mockReturnValueOnce(false));

    await expect(clientAuthenticationHandler.authenticate(request)).rejects.toThrow(
      new InvalidClientException({ error_description: 'No Client Authentication Method detected.' })
    );
  });

  it('should reject using multiple client authentication methods.', async () => {
    clientAuthenticationMethodsMocks.forEach((method) => method.hasBeenRequested.mockReturnValueOnce(true));

    await expect(clientAuthenticationHandler.authenticate(request)).rejects.toThrow(
      new InvalidClientException({ error_description: 'Multiple Client Authentication Methods detected.' })
    );
  });

  it('should return an authenticated client.', async () => {
    const client = <OAuth2Client>{
      id: 'client_id',
      secret: 'client_secret',
      authenticationMethod: 'client_secret_basic',
    };

    clientAuthenticationMethodsMocks[0]!.hasBeenRequested.mockReturnValueOnce(true);
    clientAuthenticationMethodsMocks[0]!.authenticate.mockResolvedValueOnce(client);

    await expect(clientAuthenticationHandler.authenticate(request)).resolves.toBe(client);
  });
});
