import path from 'path';

export default {
  displayName: '@unvision/oauth2-server',
  preset: '../../jest.preset.js',
  globals: {
    'ts-jest': {
      tsconfig: '<rootDir>/tsconfig.spec.json',
    },
  },
  testEnvironment: 'node',
  transform: {
    '^.+\\.[tj]s$': 'ts-jest',
  },
  moduleFileExtensions: ['ts', 'js', 'html'],
  coverageDirectory: '../../coverage/packages/oauth2-server',
  setupFilesAfterEnv: [path.join(__dirname, 'jest.setup.ts')],
  clearMocks: true,
};
