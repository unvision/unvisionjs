# Changelog

This file was generated using [@jscutlery/semver](https://github.com/jscutlery/semver).

## [0.2.0](https://gitlab.com/unvision/unvisionjs/compare/oauth2-server-0.1.1...oauth2-server-0.2.0) (2022-08-22)


### Features

* **oauth2-server:** add check for session and consent parameters ([55c48a0](https://gitlab.com/unvision/unvisionjs/commit/55c48a0523ec25fca776819e0eb08d2625e940d6))
* **oauth2-server:** add consent interaction type ([545c438](https://gitlab.com/unvision/unvisionjs/commit/545c438820ed9c0fbc3f70f5fe858ca92bdd87b9))
* **oauth2-server:** add discovery endpoint ([089cb8a](https://gitlab.com/unvision/unvisionjs/commit/089cb8a63520ea934680400bad64577183e8ed37))
* **oauth2-server:** add grant service to authorization endpoint ([230427a](https://gitlab.com/unvision/unvisionjs/commit/230427a49842c7e22f6e61ba35460633b4cadece))
* **oauth2-server:** add interaction endpoint and interaction type ([ebc71a3](https://gitlab.com/unvision/unvisionjs/commit/ebc71a3d3af2be1729d24decf13e1b11e14ac1e3))
* **oauth2-server:** add login interaction type ([3868bb8](https://gitlab.com/unvision/unvisionjs/commit/3868bb82696cdbd6a9ce3fe2a4f39cc079d62124))
* **oauth2-server:** remove user from http request ([84db481](https://gitlab.com/unvision/unvisionjs/commit/84db481cfb443b3e7d0d5f5691c81312091b397c))

## [0.1.1](https://gitlab.com/unvision/unvisionjs/compare/oauth2-server-0.1.0...oauth2-server-0.1.1) (2022-07-31)


### Bug Fixes

* **oauth2-server:** add import of buffer class ([4dfe647](https://gitlab.com/unvision/unvisionjs/commit/4dfe647cb9760eed215ebe7dfcdcc282a93a28dd))
* **oauth2-server:** remove unknown error message from response ([0a8f2b0](https://gitlab.com/unvision/unvisionjs/commit/0a8f2b0aecf375600a4bc455fc91bce6f7bb90a0))


### Reverts

* **oauth2-server:** revert commit 61564dbb ([bddf7ec](https://gitlab.com/unvision/unvisionjs/commit/bddf7ecabb99b62e600132fe27e7519230ba2089))

## 0.1.0 (2022-07-30)


### Features

* **oauth2-server:** add authorization server and its metadata ([ad7f140](https://gitlab.com/unvision/unvisionjs/commit/ad7f1406e03435e01977f2e27b10796ed28ef84c))
* **oauth2-server:** add client authentication methods and handler ([ce8e35b](https://gitlab.com/unvision/unvisionjs/commit/ce8e35bcf24f2159d200919209753f9d38ffcf46))
* **oauth2-server:** add cookies to request and response ([34e61da](https://gitlab.com/unvision/unvisionjs/commit/34e61da73d86d513dafb55465bb3b491e465dc01))
* **oauth2-server:** add default development services ([6d92c82](https://gitlab.com/unvision/unvisionjs/commit/6d92c82b6d3c813818447c8934b08865b67a5a38))
* **oauth2-server:** add endpoints ([3f7c017](https://gitlab.com/unvision/unvisionjs/commit/3f7c0174804305cb1a13be1ec8a2338c11862818))
* **oauth2-server:** add exceptions ([bf123db](https://gitlab.com/unvision/unvisionjs/commit/bf123db32b538788cb8fb8a5cb979a1966c68046))
* **oauth2-server:** add exports ([2d71f6a](https://gitlab.com/unvision/unvisionjs/commit/2d71f6a0209ab4294b516b74855385571a681bdf))
* **oauth2-server:** add express provider ([7cf2f1e](https://gitlab.com/unvision/unvisionjs/commit/7cf2f1ec4ca28660b4fef36be9da33606cf294b4))
* **oauth2-server:** add grant-types ([fcd98da](https://gitlab.com/unvision/unvisionjs/commit/fcd98da8462c806b0fd7dabba31f6ffae2cd8e37))
* **oauth2-server:** add http request and response ([d389cd3](https://gitlab.com/unvision/unvisionjs/commit/d389cd398ca738632bf0d47d8ffd2a6d5cfd5c02))
* **oauth2-server:** add jwt profile for client authentication and grant type ([617f6ec](https://gitlab.com/unvision/unvisionjs/commit/617f6ecf45ec621d0e412fb01261164a941db48b))
* **oauth2-server:** add pkce methods ([7bd7cd3](https://gitlab.com/unvision/unvisionjs/commit/7bd7cd3039173afdea2e8fd502338941e06add32))
* **oauth2-server:** add response modes ([2ef6fba](https://gitlab.com/unvision/unvisionjs/commit/2ef6fba02af8ead0cca41faff794d4d68ce01472))
* **oauth2-server:** add response types ([674ac95](https://gitlab.com/unvision/unvisionjs/commit/674ac953a93a66d00194f2b9dc8ccf9c1acb18f2))
* **oauth2-server:** set http request as class ([61564db](https://gitlab.com/unvision/unvisionjs/commit/61564dbb9a651c03fc07587a436a46219805e9b9))


### Bug Fixes

* **oauth2-server:** add missing exports ([69ac675](https://gitlab.com/unvision/unvisionjs/commit/69ac675832f138d00692ae6df55eced29c519fe1))
* **oauth2-server:** fix "expires_in" attribute in createTokenResponse() ([8c2da4a](https://gitlab.com/unvision/unvisionjs/commit/8c2da4a99c5bfa49579985b5a2449653f2335cbc))
* **oauth2-server:** fix authorization endpoint server error exception ([20d15bc](https://gitlab.com/unvision/unvisionjs/commit/20d15bcccd19e89ee5da213689cb822c646c56c3))
* **oauth2-server:** fix typing of express request ([71b8757](https://gitlab.com/unvision/unvisionjs/commit/71b8757f31c80a6154a977795be8a99e950b6bda))
* **oauth2-server:** remove "post" from authorization endpoint http methods ([26b0f15](https://gitlab.com/unvision/unvisionjs/commit/26b0f153cb4d2d53313104166d6b97cf09cf7e1d))
* **oauth2-server:** remove enable refresh token rotation from the client ([18d17ad](https://gitlab.com/unvision/unvisionjs/commit/18d17ad047ca5e30f4c824ced03f355958bec04e))
