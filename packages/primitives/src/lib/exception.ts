/**
 * Exception class that keeps the Stack Trace of rethrown Errors.
 */
export class Exception extends Error {
  /**
   * Previous Error of the Chain.
   */
  public readonly originalError?: Error;

  /**
   * Instantiates a new Exception with an optional message of rethrown Error.
   *
   * @param messageOrOriginalError Message of the Exception or rethrown Error.
   */
  public constructor(messageOrOriginalError?: string | Error);

  /**
   * Instantiates a new Exception with the provided message and chaining the provided Error's Stack Trace.
   *
   * @param message Message of the Exception.
   * @param originalError Rethrown Error.
   */
  public constructor(message: string | null, originalError: Error);

  /**
   * Instantiates a new Exception based on the provided parameters.
   *
   * @param messageOrOriginalError Message of the Exception or rethrown Error.
   * @param originalError Rethrown Error.
   */
  public constructor(messageOrOriginalError?: string | Error | null, originalError?: Error) {
    originalError = messageOrOriginalError instanceof Error ? messageOrOriginalError : originalError;

    super(
      messageOrOriginalError === null
        ? undefined
        : typeof messageOrOriginalError === 'string'
        ? messageOrOriginalError
        : originalError?.message
    );

    this.name = this.constructor.name;

    Object.defineProperty(this, 'originalError', { enumerable: false, value: originalError, writable: true });

    if (this.message === '' && typeof this.getDefaultMessage === 'function') {
      this.message = this.getDefaultMessage();
    }

    // TODO: Check if this does not cause a memory leak.
    const thisStack = this.stack ?? '';
    const originalStack = (this.originalError?.stack ?? '').split('\n').slice(0, Error.stackTraceLimit).join('\n');

    this.stack =
      this.originalError === undefined ? `${thisStack}\n${originalStack}` : `${thisStack}\nFrom ${originalStack}`;
  }

  /**
   * Returns the default Exception Message.
   */
  protected getDefaultMessage?(): string;
}
