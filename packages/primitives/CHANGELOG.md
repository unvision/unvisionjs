# Changelog

This file was generated using [@jscutlery/semver](https://github.com/jscutlery/semver).

## [0.1.1](https://gitlab.com/unvision/unvisionjs/compare/primitives-0.1.0...primitives-0.1.1) (2022-08-22)

## 0.1.0 (2022-06-29)


### Features

* **primitives:** initial setup and add exception class ([32240a9](https://gitlab.com/unvision/unvisionjs/commit/32240a9960fbfb6610a3ac466093d2cc9a33250e))
