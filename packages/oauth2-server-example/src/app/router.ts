import { Router } from 'express';

import { authRouter } from './routes/auth.routes';
import { homeRouter } from './routes/home.routes';
import { oauthRouter } from './routes/oauth.routes';

const router = Router();

router.use('/', homeRouter);
router.use('/auth', authRouter);
router.use('/oauth', oauthRouter);

export { router };
