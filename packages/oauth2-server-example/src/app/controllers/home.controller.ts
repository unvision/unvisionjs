import { Request, Response } from 'express';

export class HomeController {
  public static async home(request: Request, response: Response): Promise<void> {
    return response.render('home', { request, title: 'Home' });
  }
}
