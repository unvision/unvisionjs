import axios from 'axios';
import { Request, Response } from 'express';
import { URL, URLSearchParams } from 'url';

export class LoginController {
  public static async get(request: Request, response: Response): Promise<void> {
    const loginChallenge = <string>request.query.login_challenge;

    if (typeof loginChallenge !== 'string') {
      return response.render('auth/login', {
        request,
        title: 'Login',
        error: request.flash('error'),
        success: request.flash('success'),
      });
    }

    const url = new URL('http://localhost:3000/oauth/interaction');
    const searchParams = new URLSearchParams({ interaction_type: 'login', login_challenge: loginChallenge });

    url.search = searchParams.toString();

    const { data } = await axios.get(url.href);

    if (data.skip) {
      return response.redirect(303, data.request_url);
    }

    return response.render('auth/login', {
      request,
      title: 'Login',
      login_challenge: loginChallenge,
      error: request.flash('error'),
      success: request.flash('success'),
    });
  }

  public static async post(request: Request, response: Response): Promise<void> {
    const { login_challenge: loginChallenge } = request.body;

    if (typeof loginChallenge !== 'string') {
      return response.redirect(303, '');
    }

    const reqBody = new URLSearchParams({
      interaction_type: 'login',
      login_challenge: loginChallenge,
      decision: 'accept',
      subject: Reflect.get(request.user!, 'id'),
    });

    const {
      data: { redirect_to: redirectTo },
    } = await axios.post('http://localhost:3000/oauth/interaction', reqBody.toString(), {
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    });

    return response.redirect(303, redirectTo);
  }
}
