import { Request, Response } from 'express';

export class LogoutController {
  public static async logout(request: Request, response: Response) {
    request.logout(() => undefined);
    return response.redirect(303, '/auth/login');
  }
}
