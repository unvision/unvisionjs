import { getContainer } from '@unvision/di';
import { OAuth2User, USER_SERVICE } from '@unvision/oauth2-server';

import { randomUUID } from 'crypto';
import { Request, Response } from 'express';

import { UserService } from '../../services/user.service';

export class RegisterController {
  public static async get(request: Request, response: Response): Promise<void> {
    return response.render('auth/register', { request, title: 'Register' });
  }

  public static async post(request: Request, response: Response): Promise<void> {
    const userService = getContainer('oauth2').resolve<UserService>(USER_SERVICE);

    const { username, password } = request.body;

    const user: OAuth2User = { id: randomUUID(), username, password };

    userService['users'].push(user);

    request.flash('success', 'User created successfully');

    return response.redirect(303, '/auth/login');
  }
}
