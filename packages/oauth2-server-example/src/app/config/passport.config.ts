import { getContainer } from '@unvision/di';
import { OAuth2User, USER_SERVICE } from '@unvision/oauth2-server';

import { Express } from 'express';
import passport from 'passport';
import { Strategy as LocalStrategy } from 'passport-local';

import { UserService } from '../services/user.service';

export function passportConfig(app: Express): void {
  const container = getContainer('oauth2');

  container.bind<UserService>(USER_SERVICE).toClass(UserService);

  const userService = container.resolve<UserService>(USER_SERVICE);

  passport.use(
    new LocalStrategy(async (username, password, done) => {
      const user = await userService.findByUsername(username);

      if (user === null) {
        return done(null, false, { message: 'Invalid Credentials' });
      }

      if (password !== user.password) {
        return done(null, false, { message: 'Invalid Credentials' });
      }

      return done(null, user);
    })
  );

  passport.serializeUser<string>((user, done) => {
    return done(null, (<OAuth2User>user).id);
  });

  passport.deserializeUser(async (id: string, done) => {
    const user = await userService.findOne(id);
    return done(null, user ?? false);
  });

  app.use(passport.initialize());
  app.use(passport.session());
}
