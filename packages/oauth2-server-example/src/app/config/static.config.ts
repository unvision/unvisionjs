import express, { Express } from 'express';
import path from 'path';

export function staticConfig(app: Express): void {
  // Relative to dist
  app.use('/static', express.static(path.join(__dirname, 'assets')));
}
