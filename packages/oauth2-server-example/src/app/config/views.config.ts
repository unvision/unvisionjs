import { Express } from 'express';
import nunjucks from 'nunjucks';
import path from 'path';

export function viewsConfig(app: Express): void {
  // Relative to dist
  nunjucks.configure(path.join(__dirname, 'views'), { autoescape: true, express: app });
  app.set('view engine', 'njk');
}
