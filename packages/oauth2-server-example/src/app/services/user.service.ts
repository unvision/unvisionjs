import { Injectable } from '@unvision/di';
import { DefaultUserService, OAuth2User } from '@unvision/oauth2-server';

@Injectable()
export class UserService extends DefaultUserService {
  public async findByUsername(username: string): Promise<OAuth2User | null> {
    return this.users.find((user) => user.username === username) ?? null;
  }
}
