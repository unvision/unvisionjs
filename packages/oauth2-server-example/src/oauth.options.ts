import { AuthorizationServerMetadataOptions } from '@unvision/oauth2-server';

import { UserService } from './app/services/user.service';

export const oauthOptions: AuthorizationServerMetadataOptions = {
  issuer: 'http://localhost:3000',
  scopes: ['foo', 'bar', 'baz', 'qux'],
  clientAuthenticationMethods: ['client_secret_basic', 'client_secret_post', 'none'],
  grantTypes: ['authorization_code', 'refresh_token', 'client_credentials', 'password'],
  responseTypes: ['code', 'token'],
  responseModes: ['form_post', 'fragment', 'query'],
  pkceMethods: ['plain', 'S256'],
  userInteraction: {
    consentUrl: '/auth/consent',
    errorUrl: '/oauth/error',
    loginUrl: '/auth/login',
  },
  enableRefreshTokenRotation: false,
  enableRevocationEndpoint: true,
  enableIntrospectionEndpoint: true,
  enableAccessTokenRevocation: true,
  enableRefreshTokenIntrospection: true,
  userService: UserService,
};
