import 'reflect-metadata';

import { expressProvider } from '@unvision/oauth2-server';

import flash from 'connect-flash';
import cookieParser from 'cookie-parser';
import express, { urlencoded } from 'express';
import session from 'express-session';

import { passportConfig } from './app/config/passport.config';
import { staticConfig } from './app/config/static.config';
import { viewsConfig } from './app/config/views.config';
import { router } from './app/router';
import { oauthOptions } from './oauth.options';

async function main(): Promise<void> {
  const app = express();
  const secret = 'super_safe_and_secret_passphrase_that_nobody_will_ever_be_able_to_guess';

  app.use(urlencoded({ extended: true }));
  app.use(cookieParser(secret));
  app.use(
    session({
      secret,
      name: 'unvision',
      resave: false,
      saveUninitialized: false,
    })
  );
  app.use(flash());

  passportConfig(app);
  staticConfig(app);
  viewsConfig(app);

  app.use(await expressProvider(oauthOptions));
  app.use(router);

  app.listen(3000, () => console.log('Authorization Server is running on http://localhost:3000'));
}

main();
