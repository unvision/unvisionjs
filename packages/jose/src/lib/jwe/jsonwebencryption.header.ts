import { InvalidJoseHeaderException } from '../exceptions/invalid-jose-header.exception';
import { UnsupportedAlgorithmException } from '../exceptions/unsupported-algorithm.exception';
import { JsonWebKeyParameters } from '../jwk/jsonwebkey.parameters';
import { A128KW, A192KW, A256KW } from './algorithms/alg/aes.algorithm';
import { dir } from './algorithms/alg/dir.algorithm';
import { A128GCMKW, A192GCMKW, A256GCMKW } from './algorithms/alg/gcm.algorithm';
import { JsonWebEncryptionKeyWrapAlgorithm } from './algorithms/alg/jsonwebencryption-key-wrap.algorithm';
import { RSA1_5, RSA_OAEP, RSA_OAEP_256, RSA_OAEP_384, RSA_OAEP_512 } from './algorithms/alg/rsa.algorithm';
import { SupportedJsonWebEncryptionKeyWrapAlgorithm } from './algorithms/alg/types/supported-jsonwebencryption-keywrap-algorithm';
import { A128CBC_HS256, A192CBC_HS384, A256CBC_HS512 } from './algorithms/enc/cbc.algorithm';
import { A128GCM, A192GCM, A256GCM } from './algorithms/enc/gcm.algorithm';
import { JsonWebEncryptionContentEncryptionAlgorithm } from './algorithms/enc/jsonwebencryption-content-encryption.algorithm';
import { SupportedJsonWebEncryptionContentEncryptionAlgorithm } from './algorithms/enc/types/supported-jsonwebencryption-content-encryption-algorithm';
import { DEF } from './algorithms/zip/def.algorithm';
import { JsonWebEncryptionCompressionAlgorithm } from './algorithms/zip/jsonwebencryption-compression.algorithm';
import { SupportedJsonWebEncryptionCompressionAlgorithm } from './algorithms/zip/types/supported-jsonwebencryption-compression-algorithm';
import { JsonWebEncryptionHeaderParameters } from './jsonwebencryption.header.parameters';

/**
 * Implementation of the JSON Web Encryption Header.
 *
 * @see https://www.rfc-editor.org/rfc/rfc7516.html#section-4
 */
export class JsonWebEncryptionHeader implements JsonWebEncryptionHeaderParameters {
  /**
   * Supported JSON Web Encryption Key Wrap Algorithms.
   */
  private static readonly keyWrapAlgorithms: Record<
    SupportedJsonWebEncryptionKeyWrapAlgorithm,
    JsonWebEncryptionKeyWrapAlgorithm
  > = {
    'RSA-OAEP': RSA_OAEP,
    'RSA-OAEP-256': RSA_OAEP_256,
    'RSA-OAEP-384': RSA_OAEP_384,
    'RSA-OAEP-512': RSA_OAEP_512,
    A128GCMKW,
    A128KW,
    A192GCMKW,
    A192KW,
    A256GCMKW,
    A256KW,
    dir,
    RSA1_5,
  };

  /**
   * Supported JSON Web Encryption Content Encryption Algorithms.
   */
  private static readonly contentEncryptionAlgorithms: Record<
    SupportedJsonWebEncryptionContentEncryptionAlgorithm,
    JsonWebEncryptionContentEncryptionAlgorithm
  > = {
    'A128CBC-HS256': A128CBC_HS256,
    'A192CBC-HS384': A192CBC_HS384,
    'A256CBC-HS512': A256CBC_HS512,
    A128GCM,
    A192GCM,
    A256GCM,
  };

  /**
   * Supported JSON Web Encryption Compression Algorithms.
   */
  private static readonly compressionAlgorithms: Record<
    SupportedJsonWebEncryptionCompressionAlgorithm,
    JsonWebEncryptionCompressionAlgorithm
  > = {
    DEF,
  };

  /**
   * JSON Web Encryption Key Wrap Algorithm used by the JSON Web Encryption Header.
   */
  public readonly keyWrapAlgorithm!: JsonWebEncryptionKeyWrapAlgorithm;

  /**
   * JSON Web Encryption Content Encryption Algorithm used by the JSON Web Encryption Header.
   */
  public readonly contentEncryptionAlgorithm!: JsonWebEncryptionContentEncryptionAlgorithm;

  /**
   * JSON Web Encryption Compression Algorithm used by the JSON Web Encryption Header.
   */
  public readonly compressionAlgorithm!: JsonWebEncryptionCompressionAlgorithm | null;

  /**
   * JSON Web Encryption Key Wrap Algorithm used to Wrap and Unwrap the Content Encryption Key.
   */
  public readonly alg!: SupportedJsonWebEncryptionKeyWrapAlgorithm;

  /**
   * JSON Web Encryption Content Encryption Algorithm used to Encrypt and Decrypt the Plaintext of the Token.
   */
  public readonly enc!: SupportedJsonWebEncryptionContentEncryptionAlgorithm;

  /**
   * JSON Web Encryption Compression Algorithm used to Compress and Decompress the Plaintext of the Token.
   */
  public readonly zip?: SupportedJsonWebEncryptionCompressionAlgorithm;

  /**
   * URI of a Set of Public JSON Web Keys that contains the JSON Web Key used to Encrypt the Token.
   */
  public readonly jku?: string;

  /**
   * JSON Web Key used to Encrypt the Token.
   */
  public readonly jwk?: JsonWebKeyParameters;

  /**
   * Identifier of the JSON Web Key used to Encrypt the Token.
   */
  public readonly kid?: string;

  /**
   * URI of the X.509 certificate of the JSON Web Key used to Encrypt the Token.
   */
  public readonly x5u?: string;

  /**
   * Chain of X.509 certificates of the JSON Web Key used to Encrypt the Token.
   */
  public readonly x5c?: string[];

  /**
   * SHA-1 Thumbprint of the X.509 certificate of the JSON Web Key used to Encrypt the Token.
   */
  public readonly x5t?: string;

  /**
   * SHA-256 Thumbprint of the X.509 certificate of the JSON Web Key used to Encrypt the Token.
   */
  public readonly 'x5t#S256'?: string;

  /**
   * Defines the type of the Token.
   */
  public readonly typ?: string;

  /**
   * Defines the type of the Payload of the Token.
   */
  public readonly cty?: string;

  /**
   * Defines the parameters that MUST be present in the JSON Web Encryption Header.
   */
  public readonly crit?: string[];

  /**
   * Additional JSON Web Encryption Header Parameters.
   */
  readonly [parameter: string]: any;

  /**
   * Instantiates a new JSON Web Encryption Header based on the provided Parameters.
   *
   * @param parameters JSON Web Encryption Header Parameters.
   */
  public constructor(parameters: JsonWebEncryptionHeaderParameters) {
    if (parameters instanceof JsonWebEncryptionHeader) {
      return parameters;
    }

    if (typeof parameters.alg !== 'string') {
      throw new InvalidJoseHeaderException('Invalid header parameter "alg".');
    }

    if (typeof parameters.enc !== 'string') {
      throw new InvalidJoseHeaderException('Invalid header parameter "enc".');
    }

    if (parameters.zip !== undefined && typeof parameters.zip !== 'string') {
      throw new InvalidJoseHeaderException('Invalid header parameter "zip".');
    }

    if (JsonWebEncryptionHeader.keyWrapAlgorithms[parameters.alg] === undefined) {
      throw new UnsupportedAlgorithmException(
        `Unsupported JSON Web Encryption Key Wrap Algorithm "${parameters.alg}".`
      );
    }

    if (JsonWebEncryptionHeader.contentEncryptionAlgorithms[parameters.enc] === undefined) {
      throw new UnsupportedAlgorithmException(
        `Unsupported JSON Web Encryption Content Encryption Algorithm "${parameters.enc}".`
      );
    }

    if (parameters.zip !== undefined) {
      if (JsonWebEncryptionHeader.compressionAlgorithms[parameters.zip] === undefined) {
        throw new UnsupportedAlgorithmException(
          `Unsupported JSON Web Encryption Compression Algorithm "${parameters.zip}".`
        );
      }
    }

    if (parameters.jku !== undefined) {
      throw new InvalidJoseHeaderException('Unsupported header parameter "jku".');
    }

    if (parameters.jwk !== undefined) {
      throw new InvalidJoseHeaderException('Unsupported header parameter "jwk".');
    }

    if (parameters.kid !== undefined && typeof parameters.kid !== 'string') {
      throw new InvalidJoseHeaderException('Invalid header parameter "kid".');
    }

    if (parameters.x5u !== undefined) {
      throw new InvalidJoseHeaderException('Unsupported header parameter "x5u".');
    }

    if (parameters.x5c !== undefined) {
      throw new InvalidJoseHeaderException('Unsupported header parameter "x5c".');
    }

    if (parameters.x5t !== undefined) {
      throw new InvalidJoseHeaderException('Unsupported header parameter "x5t".');
    }

    if (parameters['x5t#S256'] !== undefined) {
      throw new InvalidJoseHeaderException('Unsupported header parameter "x5t#S256".');
    }

    if (parameters.crit !== undefined) {
      if (!Array.isArray(parameters.crit) || parameters.crit.length === 0) {
        throw new InvalidJoseHeaderException('Invalid header parameter "crit".');
      }

      if (parameters.crit.some((criticalParam) => typeof criticalParam !== 'string' || criticalParam.length === 0)) {
        throw new InvalidJoseHeaderException('Invalid header parameter "crit".');
      }

      parameters.crit.forEach((criticalParam) => {
        if (parameters[criticalParam] === undefined) {
          throw new InvalidJoseHeaderException(`Missing required header parameter "${criticalParam}".`);
        }
      });
    }

    Object.defineProperty(this, 'keyWrapAlgorithm', {
      value: JsonWebEncryptionHeader.keyWrapAlgorithms[parameters.alg],
    });

    Object.defineProperty(this, 'contentEncryptionAlgorithm', {
      value: JsonWebEncryptionHeader.contentEncryptionAlgorithms[parameters.enc],
    });

    Object.defineProperty(this, 'compressionAlgorithm', {
      value: parameters.zip !== undefined ? JsonWebEncryptionHeader.compressionAlgorithms[parameters.zip] : null,
    });

    Object.assign(this, parameters);
  }
}
