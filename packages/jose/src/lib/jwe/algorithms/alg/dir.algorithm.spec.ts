import { Buffer } from 'buffer';

import { JsonWebKey } from '../../../jwk/jsonwebkey';
import { JsonWebEncryptionContentEncryptionAlgorithm } from '../enc/jsonwebencryption-content-encryption.algorithm';
import { dir } from './dir.algorithm';

describe('JSON Web Encryption Direct Key Wrap Algorithm', () => {
  it('should wrap and unwrap a content encryption key.', async () => {
    const enc = <JsonWebEncryptionContentEncryptionAlgorithm>{
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      validateContentEncryptionKey: (_: Buffer) => undefined,
    };

    const key = new JsonWebKey({ kty: 'oct', k: 'EBESExQVFhcYGRobHB0eHw' });
    const [cek, ek] = await dir.wrap(enc, key);

    expect(cek).toEqual(Buffer.from(<string>key.k, 'base64url'));
    expect(ek).toEqual(Buffer.alloc(0));

    await expect(dir.unwrap(enc, key, ek)).resolves.toEqual(Buffer.from(<string>key.k, 'base64url'));
  });
});
