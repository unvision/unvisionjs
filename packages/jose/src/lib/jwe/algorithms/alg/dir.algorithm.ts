import { Buffer } from 'buffer';

import { InvalidJsonWebEncryptionException } from '../../../exceptions/invalid-jsonwebencryption.exception';
import { JsonWebKey } from '../../../jwk/jsonwebkey';
import { JsonWebEncryptionContentEncryptionAlgorithm } from '../enc/jsonwebencryption-content-encryption.algorithm';
import { JsonWebEncryptionKeyWrapAlgorithm } from './jsonwebencryption-key-wrap.algorithm';

/**
 * Implementation of the JSON Web Encryption Direct Key Wrap Algorithm.
 *
 * @see https://www.rfc-editor.org/rfc/rfc7518.html#section-4.5
 */
class DirAlgorithm extends JsonWebEncryptionKeyWrapAlgorithm {
  /**
   * Instantiates a new JSON Web Encryption Direct Key Wrap Algorithm to Wrap and Unwrap Content Encryption Keys.
   */
  public constructor() {
    super('dir', 'oct');
  }

  /**
   * Returns an empty Buffer as the Wrapped Key since the Algorithm does not Wrap the provided Content Encryption Key.
   *
   * @param enc JSON Web Encryption Content Encryption Algorithm.
   * @param key JSON Web Key to be used as the Content Encryption Key used to Encrypt the Plaintext.
   * @returns Wrap Key as the Content Encryption Key and an empty Buffer as the Wrapped Content Encryption Key.
   */
  public async wrap(enc: JsonWebEncryptionContentEncryptionAlgorithm, key: JsonWebKey): Promise<[Buffer, Buffer]> {
    this.validateJsonWebKey(key);

    const cek = key['cryptoKey'].export();
    const ek = Buffer.alloc(0);

    enc.validateContentEncryptionKey(cek);

    return [cek, ek];
  }

  /**
   * Returns the provided JSON Web Key as the Content Encryption Key.
   *
   * @param enc JSON Web Encryption Content Encryption Algorithm.
   * @param key JSON Web Key used as the Content Encryption Key.
   * @param ek ~Wrapped Content Encryption Key~.
   * @returns Provided JSON Web Key as the Content Encryption Key.
   */
  public async unwrap(enc: JsonWebEncryptionContentEncryptionAlgorithm, key: JsonWebKey, ek: Buffer): Promise<Buffer> {
    if (ek.length !== 0) {
      throw new InvalidJsonWebEncryptionException('Expected the Encrypted Content Encryption Key to be empty.');
    }

    const cek = key['cryptoKey'].export();

    enc.validateContentEncryptionKey(cek);

    return cek;
  }
}

/**
 * Direct use of a shared symmetric key as the CEK.
 */
export const dir = new DirAlgorithm();
