import { Buffer } from 'buffer';
import { CipherGCMTypes, createCipheriv, createDecipheriv, randomBytes } from 'crypto';
import { promisify } from 'util';

import { InvalidJsonWebKeyException } from '../../../exceptions/invalid-jsonwebkey.exception';
import { JsonWebKey } from '../../../jwk/jsonwebkey';
import { SupportedJsonWebEncryptionKeyWrapAlgorithm } from './types/supported-jsonwebencryption-keywrap-algorithm';
import { JsonWebEncryptionContentEncryptionAlgorithm } from '../enc/jsonwebencryption-content-encryption.algorithm';
import { JsonWebEncryptionKeyWrapAlgorithm } from './jsonwebencryption-key-wrap.algorithm';

const randomBytesAsync = promisify(randomBytes);

/**
 * Implementation of the JSON Web Encryption AES-GCM Key Wrap Algorithm.
 *
 * @see https://www.rfc-editor.org/rfc/rfc7518.html#section-4.7
 */
class GcmAlgorithm extends JsonWebEncryptionKeyWrapAlgorithm {
  /**
   * Size of the Initialization Vector in bits.
   */
  private readonly ivSize: number = 96;

  /**
   * Size of the Authentication Tag in bytes.
   */
  private readonly authTagLength: number = 16;

  /**
   * Size of the Content Encryption Key in bits.
   */
  private readonly keySize: number;

  /**
   * Name of the Cipher Algorithm.
   */
  private readonly cipher: CipherGCMTypes;

  /**
   * Instantiates a new JSON Web Encryption AES-GCM Key Wrap Algorithm to Wrap and Unwrap Content Encryption Keys.
   *
   * @param algorithm Name of the JSON Web Encryption Key Wrap Algorithm.
   */
  public constructor(algorithm: SupportedJsonWebEncryptionKeyWrapAlgorithm) {
    super(algorithm, 'oct');

    this.keySize = Number.parseInt(this.algorithm.substring(1, 4));
    this.cipher = <CipherGCMTypes>`aes-${this.keySize}-gcm`;
  }

  /**
   * Wraps the provided Content Encryption Key using the provide JSON Web Key.
   *
   * @param enc JSON Web Encryption Content Encryption Algorithm.
   * @param key JSON Web Key used to Wrap the provided Content Encryption Key.
   * @returns Wrapped Content Encryption Key and optional additional JSON Web Encryption Header Parameters.
   */
  public async wrap(
    enc: JsonWebEncryptionContentEncryptionAlgorithm,
    key: JsonWebKey
  ): Promise<[Buffer, Buffer, NodeJS.Dict<any>]> {
    this.validateJsonWebKey(key);

    const iv = await randomBytesAsync(this.ivSize / 8);

    const cryptoKey = key['cryptoKey'];
    const cipher = createCipheriv(this.cipher, cryptoKey, iv, { authTagLength: this.authTagLength });

    cipher.setAAD(Buffer.alloc(0));

    const cek = await enc.generateContentEncryptionKey();
    const ek = Buffer.concat([cipher.update(cek), cipher.final()]);
    const tag = cipher.getAuthTag();

    return [cek, ek, { iv: iv.toString('base64url'), tag: tag.toString('base64url') }];
  }

  /**
   * Unwraps the provided Encrypted Key using the provided JSON Web Key.
   *
   * @param enc JSON Web Encryption Content Encryption Algorithm.
   * @param key JSON Web Key used to Unwrap the Wrapped Content Encryption Key.
   * @param ek Wrapped Content Encryption Key.
   * @param header JSON Web Encryption Header containing the additional Parameters.
   * @returns Unwrapped Content Encryption Key.
   */
  public async unwrap(
    enc: JsonWebEncryptionContentEncryptionAlgorithm,
    key: JsonWebKey,
    ek: Buffer,
    header: NodeJS.Dict<any>
  ): Promise<Buffer> {
    this.validateJsonWebKey(key);

    const iv = Buffer.from(header.iv, 'base64url');
    const tag = Buffer.from(header.tag, 'base64url');

    const cryptoKey = key['cryptoKey'];
    const decipher = createDecipheriv(this.cipher, cryptoKey, iv, { authTagLength: this.authTagLength });

    decipher.setAAD(Buffer.alloc(0));
    decipher.setAuthTag(tag);

    const cek = Buffer.concat([decipher.update(ek), decipher.final()]);

    enc.validateContentEncryptionKey(cek);

    return cek;
  }

  /**
   * Checks if the provided JSON Web Key can be used by the requesting JSON Web Encryption AES-GCM Key Wrap Algorithm.
   *
   * @param key JSON Web Key to be checked.
   * @throws {InvalidJsonWebKeyException} The provided JSON Web Key is invalid.
   */
  protected override validateJsonWebKey(key: JsonWebKey): void {
    super.validateJsonWebKey(key);

    const exportedKey = key['cryptoKey'].export();

    if (exportedKey.length * 8 !== this.keySize) {
      throw new InvalidJsonWebKeyException('Invalid JSON Web Key Secret Size.');
    }
  }
}

/**
 * Key wrapping with AES GCM using 128-bit key.
 */
export const A128GCMKW = new GcmAlgorithm('A128GCMKW');

/**
 * Key wrapping with AES GCM using 192-bit key.
 */
export const A192GCMKW = new GcmAlgorithm('A192GCMKW');

/**
 * Key wrapping with AES GCM using 256-bit key.
 */
export const A256GCMKW = new GcmAlgorithm('A256GCMKW');
