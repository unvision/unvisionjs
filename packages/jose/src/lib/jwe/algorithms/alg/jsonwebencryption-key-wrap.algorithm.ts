import { Buffer } from 'buffer';

import { InvalidJsonWebKeyException } from '../../../exceptions/invalid-jsonwebkey.exception';
import { SupportedJsonWebKeyAlgorithm } from '../../../jwk/algorithms/types/supported-jsonwebkey-algorithm';
import { JsonWebKey } from '../../../jwk/jsonwebkey';
import { SupportedJsonWebEncryptionKeyWrapAlgorithm } from './types/supported-jsonwebencryption-keywrap-algorithm';
import { JsonWebEncryptionContentEncryptionAlgorithm } from '../enc/jsonwebencryption-content-encryption.algorithm';

/**
 * Abstract Base Class for the JSON Web Encryption Key Wrap Algorithm.
 *
 * All JSON Web Encryption Key Wrap Algorithms **MUST** extend this base class and implement its abstract methods.
 *
 * @see https://www.rfc-editor.org/rfc/rfc7518.html#section-4
 */
export abstract class JsonWebEncryptionKeyWrapAlgorithm {
  /**
   * Name of the JSON Web Encryption Key Wrap Algorithm.
   */
  protected readonly algorithm: SupportedJsonWebEncryptionKeyWrapAlgorithm;

  /**
   * Type of JSON Web Key supported by this JSON Web Encryption Key Wrap Algorithm.
   */
  protected readonly keyType: SupportedJsonWebKeyAlgorithm;

  /**
   * Instantiates a new JSON Web Encryption Key Wrap Algorithm to Wrap and Unwrap Content Encryption Keys.
   *
   * @param algorithm Name of the JSON Web Encryption Key Wrap Algorithm.
   * @param keyType Type of JSON Web Key supported by this JSON Web Encryption Key Wrap Algorithm.
   */
  public constructor(algorithm: SupportedJsonWebEncryptionKeyWrapAlgorithm, keyType: SupportedJsonWebKeyAlgorithm) {
    this.algorithm = algorithm;
    this.keyType = keyType;
  }

  /**
   * Wraps the provided Content Encryption Key using the provide JSON Web Key.
   *
   * @param enc JSON Web Encryption Content Encryption Algorithm.
   * @param key JSON Web Key used to Wrap the provided Content Encryption Key.
   * @param header Optional JSON Web Encryption Header containing the additional Parameters.
   * @returns Generated Content Encryption Key, Wrapped Content Encryption Key and optional JSON Web Encryption Header.
   */
  public abstract wrap(
    enc: JsonWebEncryptionContentEncryptionAlgorithm,
    key: JsonWebKey,
    header?: NodeJS.Dict<any>
  ): Promise<[Buffer, Buffer, NodeJS.Dict<any>?]>;

  /**
   * Unwraps the provided Encrypted Key using the provided JSON Web Key.
   *
   * @param enc JSON Web Encrytpion Content Encryption Algorithm.
   * @param key JSON Web Key used to Unwrap the Wrapped Content Encryption Key.
   * @param ek Wrapped Content Encryption Key.
   * @param header Optional JSON Web Encryption Header containing the additional Parameters.
   * @returns Unwrapped Content Encryption Key.
   */
  public abstract unwrap(
    enc: JsonWebEncryptionContentEncryptionAlgorithm,
    key: JsonWebKey,
    ek: Buffer,
    header?: NodeJS.Dict<any>
  ): Promise<Buffer>;

  /**
   * Checks if the provided JSON Web Key can be used by the requesting JSON Web Encryption Key Wrap Algorithm.
   *
   * @param key JSON Web Key to be checked.
   * @throws {InvalidJsonWebKeyException} The provided JSON Web Key is invalid.
   */
  protected validateJsonWebKey(key: JsonWebKey): void {
    if (!(key instanceof JsonWebKey)) {
      throw new InvalidJsonWebKeyException();
    }

    if (key.alg !== undefined && key.alg !== this.algorithm) {
      throw new InvalidJsonWebKeyException(`This JSON Web Key is intended to be used by the Algorithm "${key.alg}".`);
    }

    if (key.kty !== this.keyType) {
      throw new InvalidJsonWebKeyException(
        `This JSON Web Encryption Key Wrap Algorithm only accepts "${this.keyType}" JSON Web Keys.`
      );
    }
  }
}
