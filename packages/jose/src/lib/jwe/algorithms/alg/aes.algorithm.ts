import { Buffer } from 'buffer';
import { createCipheriv, createDecipheriv } from 'crypto';

import { InvalidJsonWebKeyException } from '../../../exceptions/invalid-jsonwebkey.exception';
import { JsonWebKey } from '../../../jwk/jsonwebkey';
import { SupportedJsonWebEncryptionKeyWrapAlgorithm } from './types/supported-jsonwebencryption-keywrap-algorithm';
import { JsonWebEncryptionContentEncryptionAlgorithm } from '../enc/jsonwebencryption-content-encryption.algorithm';
import { JsonWebEncryptionKeyWrapAlgorithm } from './jsonwebencryption-key-wrap.algorithm';

/**
 * Implementation of the JSON Web Encryption AES Key Wrap Algorithm.
 *
 * @see https://www.rfc-editor.org/rfc/rfc7518.html#section-4.4
 */
class AesAlgorithm extends JsonWebEncryptionKeyWrapAlgorithm {
  /**
   * Size of the Content Encryption Key in bits.
   */
  private readonly keySize: number;

  /**
   * Name of the Cipher Algorithm.
   */
  private readonly cipher: string;

  /**
   * Instantiates a new JSON Web Encryption AES Key Wrap Algorithm to Wrap and Unwrap Content Encryption Keys.
   *
   * @param algorithm Name of the JSON Web Encryption Key Wrap Algorithm.
   */
  public constructor(algorithm: SupportedJsonWebEncryptionKeyWrapAlgorithm) {
    super(algorithm, 'oct');

    this.keySize = Number.parseInt(this.algorithm.substring(1, 4));
    this.cipher = `aes${this.keySize}-wrap`;
  }

  /**
   * Wraps the provided Content Encryption Key using the provide JSON Web Key.
   *
   * @param enc JSON Web Encryption Content Encryption Algorithm.
   * @param key JSON Web Key used to Wrap the provided Content Encryption Key.
   * @returns Generated Content Encryption Key, Wrapped Content Encryption Key and optional JSON Web Encryption Header.
   */
  public async wrap(enc: JsonWebEncryptionContentEncryptionAlgorithm, key: JsonWebKey): Promise<[Buffer, Buffer]> {
    this.validateJsonWebKey(key);

    const cryptoKey = key['cryptoKey'];
    const cipher = createCipheriv(this.cipher, cryptoKey, Buffer.alloc(8, 0xa6));

    const cek = await enc.generateContentEncryptionKey();
    const ek = Buffer.concat([cipher.update(cek), cipher.final()]);

    return [cek, ek];
  }

  /**
   * Unwraps the provided Encrypted Key using the provided JSON Web Key.
   *
   * @param enc JSON Web Encrytpion Content Encryption Algorithm.
   * @param key JSON Web Key used to Unwrap the Wrapped Content Encryption Key.
   * @param ek Wrapped Content Encryption Key.
   * @returns Unwrapped Content Encryption Key.
   */
  public async unwrap(enc: JsonWebEncryptionContentEncryptionAlgorithm, key: JsonWebKey, ek: Buffer): Promise<Buffer> {
    this.validateJsonWebKey(key);

    const cryptoKey = key['cryptoKey'];
    const decipher = createDecipheriv(this.cipher, cryptoKey, Buffer.alloc(8, 0xa6));

    const cek = Buffer.concat([decipher.update(ek), decipher.final()]);

    enc.validateContentEncryptionKey(cek);

    return cek;
  }

  /**
   * Checks if the provided JSON Web Key can be used by the requesting JSON Web Encryption AES Key Wrap Algorithm.
   *
   * @param key JSON Web Key to be checked.
   * @throws {InvalidJsonWebKeyException} The provided JSON Web Key is invalid.
   */
  protected override validateJsonWebKey(key: JsonWebKey): void {
    super.validateJsonWebKey(key);

    const exportedKey = key['cryptoKey'].export();

    if (exportedKey.length * 8 !== this.keySize) {
      throw new InvalidJsonWebKeyException('Invalid JSON Web Key Secret Size.');
    }
  }
}

/**
 * AES Key Wrap with default initial value using 128-bit key.
 */
export const A128KW = new AesAlgorithm('A128KW');

/**
 * AES Key Wrap with default initial value using 192-bit key.
 */
export const A192KW = new AesAlgorithm('A192KW');

/**
 * AES Key Wrap with default initial value using 256-bit key.
 */
export const A256KW = new AesAlgorithm('A256KW');
