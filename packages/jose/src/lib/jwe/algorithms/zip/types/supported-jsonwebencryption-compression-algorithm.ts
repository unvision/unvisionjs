/**
 * JSON Web Encryption Compression Algorithms supported by UnVision.
 */
export type SupportedJsonWebEncryptionCompressionAlgorithm = 'DEF';
