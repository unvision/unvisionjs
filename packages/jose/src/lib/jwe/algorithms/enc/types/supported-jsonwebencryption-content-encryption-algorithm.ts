/**
 * JSON Web Encryption Content Encryption Algorithms supported by UnVision.
 */
export type SupportedJsonWebEncryptionContentEncryptionAlgorithm =
  | 'A128CBC-HS256'
  | 'A192CBC-HS384'
  | 'A256CBC-HS512'
  | 'A128GCM'
  | 'A192GCM'
  | 'A256GCM';
