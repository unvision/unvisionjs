import { Exception } from '@unvision/primitives';

/**
 * Base JOSE exception.
 */
export abstract class JoseException extends Exception {}
