import { Buffer } from 'buffer';
import { KeyObject } from 'crypto';

import { InvalidJsonWebKeyException } from '../exceptions/invalid-jsonwebkey.exception';
import { UnsupportedAlgorithmException } from '../exceptions/unsupported-algorithm.exception';
import { JsonWebKey } from './jsonwebkey';
import { JsonWebKeyParameters } from './jsonwebkey.parameters';
import { KeyOperation } from './types/key-operation';
import { PublicKeyUse } from './types/public-key-use';

const secretParams: JsonWebKeyParameters = { kty: 'oct', k: 'qDM80igvja4Tg_tNsEuWDhl2bMM6_NgJEldFhIEuwqQ' };

const publicParams: JsonWebKeyParameters = {
  kty: 'EC',
  crv: 'P-256',
  x: '4c_cS6IT6jaVQeobt_6BDCTmzBaBOTmmiSCpjd5a6Og',
  y: 'mnrPnCFTDkGdEwilabaqM7DzwlAFgetZTmP9ycHPxF8',
};

const privateParams: JsonWebKeyParameters = { ...publicParams, d: 'bwVX6Vx-TOfGKYOPAcu2xhaj3JUzs-McsC-suaHnFBo' };

const invalidUses: any[] = [null, true, 1, 1.2, 1n, Buffer.alloc(1), Symbol.for('a'), () => 1, {}, []];
const invalidKeyOps: any[] = [null, true, 1, 1.2, 1n, 'a', Buffer.alloc(1), Symbol.for('a'), () => 1, {}];

const invalidAlgs: any[] = [...invalidUses];
const invalidKids: any[] = [...invalidUses];

const invalidX5Us: any[] = [null, true, 1, 1.2, 1n, Buffer.alloc(1), 'a', Symbol.for('a'), () => 1, {}, []];
const invalidX5Cs: any[] = [...invalidX5Us];
const invalidX5Ts: any[] = [...invalidX5Us];
const invalidX5T256s: any[] = [...invalidX5Us];

const invalidJwkStrings: any[] = [
  undefined,
  null,
  true,
  1,
  1.2,
  1n,
  'a',
  Symbol.for('a'),
  Buffer.alloc(1),
  () => 1,
  {},
  [],
];

const invalidUseKeyOps: [PublicKeyUse, KeyOperation[]][] = [
  ['enc', ['sign']],
  ['enc', ['verify']],
  ['enc', ['decrypt', 'sign']],
  ['sig', ['decrypt']],
  ['sig', ['deriveBits']],
  ['sig', ['deriveKey']],
  ['sig', ['encrypt']],
  ['sig', ['unwrapKey']],
  ['sig', ['wrapKey']],
  ['sig', ['sign', 'decrypt']],
];

describe('JSON Web Key', () => {
  describe('constructor', () => {
    it('should throw when no "kty" is provided.', () => {
      // @ts-expect-error Invalid parameter "kty".
      expect(() => new JsonWebKey({})).toThrow(new InvalidJsonWebKeyException('Invalid key parameter "kty".'));
    });

    it('should throw when the provided "kty" is unsupported.', () => {
      // @ts-expect-error Unsupported "kty".
      expect(() => new JsonWebKey({ kty: 'unknown' })).toThrow(
        new UnsupportedAlgorithmException('Unsupported JSON Web Key Algorithm "unknown".')
      );
    });

    it.each(invalidUses)('should throw when the provided "use" is invalid.', (use) => {
      expect(() => new JsonWebKey(secretParams, { use })).toThrow(
        new InvalidJsonWebKeyException('Invalid key parameter "use".')
      );
    });

    it.each(invalidKeyOps)('should throw when the provided "key_ops" is invalid.', (keyOps) => {
      expect(() => new JsonWebKey(secretParams, { key_ops: keyOps })).toThrow(
        new InvalidJsonWebKeyException('Invalid key parameter "key_ops".')
      );
    });

    it('should throw when the provided "key_ops" is an empty array.', () => {
      expect(() => new JsonWebKey(secretParams, { key_ops: [] })).toThrow(
        new InvalidJsonWebKeyException('Invalid key parameter "key_ops".')
      );
    });

    it('should throw when the provided "key_ops" is not an array of strings.', () => {
      // @ts-expect-error Invalid "key_ops".
      expect(() => new JsonWebKey(secretParams, { key_ops: ['sign', 123] })).toThrow(
        new InvalidJsonWebKeyException('Invalid key parameter "key_ops".')
      );
    });

    it('should throw when the provided "key_ops" array contains repeated values.', () => {
      expect(() => new JsonWebKey(secretParams, { key_ops: ['sign', 'sign'] })).toThrow(
        new InvalidJsonWebKeyException('Key parameter "key_ops" cannot have repeated operations.')
      );
    });

    it.each(invalidUseKeyOps)(
      'should throw when there\'s an invalid combination of "use" and "key_ops".',
      (use, keyOps) => {
        expect(() => new JsonWebKey(privateParams, { use, key_ops: keyOps })).toThrow(
          new InvalidJsonWebKeyException('Invalid combination of "use" and "key_ops".')
        );
      }
    );

    it.each(invalidAlgs)('should throw when the provided "alg" is invalid.', (alg) => {
      expect(() => new JsonWebKey(secretParams, { alg })).toThrow(
        new InvalidJsonWebKeyException('Invalid key parameter "alg".')
      );
    });

    it.each(invalidKids)('should throw when the provided "kid" is invalid.', (kid) => {
      expect(() => new JsonWebKey(secretParams, { kid })).toThrow(
        new InvalidJsonWebKeyException('Invalid key parameter "kid".')
      );
    });

    it.each(invalidX5Us)('should throw when providing the unsupported parameter "x5u".', (x5u) => {
      expect(() => new JsonWebKey(publicParams, { x5u })).toThrow(
        new InvalidJsonWebKeyException('Unsupported key parameter "x5u".')
      );
    });

    it.each(invalidX5Cs)('should throw when providing the unsupported parameter "x5c".', (x5c) => {
      expect(() => new JsonWebKey(publicParams, { x5c })).toThrow(
        new InvalidJsonWebKeyException('Unsupported key parameter "x5c".')
      );
    });

    it.each(invalidX5Ts)('should throw when providing the unsupported parameter "x5t".', (x5t) => {
      expect(() => new JsonWebKey(publicParams, { x5t })).toThrow(
        new InvalidJsonWebKeyException('Unsupported key parameter "x5t".')
      );
    });

    it.each(invalidX5T256s)('should throw when providing the unsupported parameter "x5t#S256".', (x5tS256) => {
      expect(() => new JsonWebKey(publicParams, { 'x5t#S256': x5tS256 })).toThrow(
        new InvalidJsonWebKeyException('Unsupported key parameter "x5t#S256".')
      );
    });

    it('should create an instance of a json web key.', () => {
      const key = new JsonWebKey(publicParams);

      expect(key).toBeInstanceOf(JsonWebKey);
      expect(key).toMatchObject<JsonWebKeyParameters>(publicParams);

      expect(key['cryptoKey']).toBeInstanceOf(KeyObject);
    });
  });

  describe('parse()', () => {
    it.each(invalidJwkStrings)('should throw when the provided data is invalid.', (invalidJwkString) => {
      expect(() => JsonWebKey.parse(invalidJwkString)).toThrow(InvalidJsonWebKeyException);
    });

    it('should parse a valid jwk string into a json web key object.', () => {
      const jwk = '{"kty":"oct","k":"qDM80igvja4Tg_tNsEuWDhl2bMM6_NgJEldFhIEuwqQ"}';

      expect(JsonWebKey.parse(jwk)).toMatchObject<JsonWebKeyParameters>(secretParams);
    });
  });

  describe('toJSON()', () => {
    const secretKey = new JsonWebKey(secretParams, { kid: 'secret-key' });
    const privateKey = new JsonWebKey(privateParams, { kid: 'private-key' });

    it('should only return the jwk parameters of the secret key.', () => {
      expect(secretKey.toJSON()).toMatchObject({ kid: 'secret-key' });
    });

    it('should return the parameters of the secret key.', () => {
      expect(secretKey.toJSON(false)).toMatchObject<JsonWebKeyParameters>({ ...secretParams, kid: 'secret-key' });
    });

    it('should return the public parameters of the private key.', () => {
      expect(privateKey.toJSON()).toMatchObject<JsonWebKeyParameters>({ ...publicParams, kid: 'private-key' });
    });

    it('should return the private parameters of the private key.', () => {
      expect(privateKey.toJSON(false)).toMatchObject<JsonWebKeyParameters>({ ...privateParams, kid: 'private-key' });
    });
  });
});
