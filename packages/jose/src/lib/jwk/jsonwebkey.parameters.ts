import { SupportedJsonWebEncryptionKeyWrapAlgorithm } from '../jwe/algorithms/alg/types/supported-jsonwebencryption-keywrap-algorithm';
import { SupportedJsonWebSignatureAlgorithm } from '../jws/algorithms/types/supported-jsonwebsignature-algorithm';
import { SupportedEllipticCurve } from './algorithms/types/supported-elliptic-curve';
import { SupportedJsonWebKeyAlgorithm } from './algorithms/types/supported-jsonwebkey-algorithm';
import { KeyOperation } from './types/key-operation';
import { PublicKeyUse } from './types/public-key-use';

/**
 * Parameters of the JSON Web Key.
 */
export interface JsonWebKeyParameters extends Record<string, any> {
  /**
   * Type of the JSON Web Key.
   */
  readonly kty: SupportedJsonWebKeyAlgorithm;

  /**
   * Octet Sequence Base64Url encoded Secret.
   */
  readonly k?: string;

  /**
   * Elliptic Curve.
   */
  readonly crv?: SupportedEllipticCurve;

  /**
   * Elliptic Curve X Coordinate.
   */
  readonly x?: string;

  /**
   * Elliptic Curve Y Coordinate.
   */
  readonly y?: string;

  /**
   * RSA Modulus.
   */
  readonly n?: string;

  /**
   * RSA Public Exponent.
   */
  readonly e?: string;

  /**
   * Elliptic Curve Private Value.
   * RSA Private Exponent.
   */
  readonly d?: string;

  /**
   * RSA First Prime Factor.
   */
  readonly p?: string;

  /**
   * RSA Second Prime Factor.
   */
  readonly q?: string;

  /**
   * RSA First Factor CRT Exponent.
   */
  readonly dp?: string;

  /**
   * RSA Second Factor CRT Exponent.
   */
  readonly dq?: string;

  /**
   * RSA First Factor CRT Coefficient.
   */
  readonly qi?: string;

  /**
   * Indicates whether a Public JSON Web Key is used for Plaintext Encryption or Signature Verification.
   */
  readonly use?: PublicKeyUse;

  /**
   * Operations for which the JSON Web Key are intended to be used.
   */
  readonly key_ops?: KeyOperation[];

  /**
   * JSON Web Encryption Key Wrap Algorithm or JSON Web Signature Algorithm allowed to use this JSON Web Key.
   */
  readonly alg?: SupportedJsonWebEncryptionKeyWrapAlgorithm | SupportedJsonWebSignatureAlgorithm;

  /**
   * Identifier of the JSON Web Key.
   */
  readonly kid?: string;

  /**
   * URL of the X.509 certificate of the JSON Web Key.
   */
  readonly x5u?: string;

  /**
   * Chain of X.509 certificates of the JSON Web Key.
   */
  readonly x5c?: string[];

  /**
   * SHA-1 Thumbprint of the X.509 certificate of the JSON Web Key.
   */
  readonly x5t?: string;

  /**
   * SHA-256 Thumbprint of the X.509 certificate of the JSON Web Key.
   */
  readonly 'x5t#S256'?: string;
}
