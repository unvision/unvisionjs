import { Buffer } from 'buffer';

import { InvalidJsonWebKeyException } from '../../exceptions/invalid-jsonwebkey.exception';
import { UnsupportedEllipticCurveException } from '../../exceptions/unsupported-elliptic-curve.exception';
import { JsonWebKeyParameters } from '../jsonwebkey.parameters';
import { EcKeyAlgorithm } from './eckey.algorithm';

const publicParams: JsonWebKeyParameters = {
  kty: 'EC',
  crv: 'P-256',
  x: '4c_cS6IT6jaVQeobt_6BDCTmzBaBOTmmiSCpjd5a6Og',
  y: 'mnrPnCFTDkGdEwilabaqM7DzwlAFgetZTmP9ycHPxF8',
};

const privateParams: JsonWebKeyParameters = { ...publicParams, d: 'bwVX6Vx-TOfGKYOPAcu2xhaj3JUzs-McsC-suaHnFBo' };

const invalidCurves: any[] = [undefined, null, true, 1, 1.2, 1n, Buffer.alloc(1), Symbol.for('foo'), () => 1, {}, []];
const invalidCoords: any[] = [undefined, null, true, 1, 1.2, 1n, Buffer.alloc(1), Symbol.for('foo'), () => 1, {}, []];
const invalidPrivateValues: any[] = [null, true, 1, 1.2, 1n, Buffer.alloc(1), Symbol.for('foo'), () => 1, {}, []];

const algorithm = new EcKeyAlgorithm();

describe('JSON Web Key Elliptic Curve Algorithm', () => {
  describe('privateParameters', () => {
    it('should have ["d"] as its value.', () => {
      expect(algorithm.privateParameters).toEqual(['d']);
    });
  });

  describe('validateParameters()', () => {
    it.each(invalidCurves)('should throw when passing an invalid curve type.', (crv) => {
      expect(() => algorithm.validateParameters({ ...publicParams, crv })).toThrow(
        new InvalidJsonWebKeyException('Invalid parameter "crv".')
      );
    });

    it('should throw when passing an unsupported curve.', () => {
      // @ts-expect-error Unsupported Elliptic Curve.
      expect(() => algorithm.validateParameters({ ...publicParams, crv: 'unknown' })).toThrow(
        new UnsupportedEllipticCurveException('Unsupported Elliptic Curve "unknown".')
      );
    });

    it.each(invalidCoords)('should throw when passing an invalid x coordinate.', (x) => {
      expect(() => algorithm.validateParameters({ ...publicParams, x })).toThrow(
        new InvalidJsonWebKeyException('Invalid key parameter "x".')
      );
    });

    it.each(invalidCoords)('should throw when passing an invalid y coordinate.', (y) => {
      expect(() => algorithm.validateParameters({ ...publicParams, y })).toThrow(
        new InvalidJsonWebKeyException('Invalid key parameter "y".')
      );
    });

    it.each(invalidPrivateValues)('should throw when passing an invalid private value.', (d) => {
      expect(() => algorithm.validateParameters({ ...publicParams, d })).toThrow(
        new InvalidJsonWebKeyException('Invalid key parameter "d".')
      );
    });
  });

  describe('loadCryptoKey()', () => {
    it('should load a public elliptic curve crypto key.', () => {
      const publicKey = algorithm.loadCryptoKey(publicParams);

      expect(publicKey.asymmetricKeyType).toBe('ec');
      expect(publicKey.type).toBe('public');
    });

    it('should load a private elliptic curve crypto key.', () => {
      const privateKey = algorithm.loadCryptoKey(privateParams);

      expect(privateKey.asymmetricKeyType).toBe('ec');
      expect(privateKey.type).toBe('private');
    });
  });
});
