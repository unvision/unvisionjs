import { KeyObject } from 'crypto';

import { JsonWebKeyParameters } from '../jsonwebkey.parameters';

export interface JsonWebKeyAlgorithm {
  /**
   * Private Parameters of the JSON Web Key Algorithm.
   */
  readonly privateParameters: string[];

  /**
   * Validates the provided JSON Web Key Parameters.
   *
   * @param parameters Parameters of the JSON Web Key.
   */
  validateParameters(parameters: JsonWebKeyParameters): void;

  /**
   * Loads the provided JSON Web Key into a NodeJS Crypto Key.
   *
   * @param parameters Parameters of the JSON Web Key.
   * @returns NodeJS Crypto Key.
   */
  loadCryptoKey(parameters: JsonWebKeyParameters): KeyObject;
}
