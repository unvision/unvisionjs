import { Buffer } from 'buffer';
import { createSecretKey, KeyObject } from 'crypto';

import { InvalidJsonWebKeyException } from '../../exceptions/invalid-jsonwebkey.exception';
import { JsonWebKeyParameters } from '../jsonwebkey.parameters';
import { JsonWebKeyAlgorithm } from './jsonwebkey.algorithm';

/**
 * Implementation of the **Octet Sequence** JSON Web Key Algorithm.
 *
 * @see https://www.rfc-editor.org/rfc/rfc7518.html#section-6.4
 */
export class OctKeyAlgorithm implements JsonWebKeyAlgorithm {
  /**
   * Private Parameters of the JSON Web Key Octet Sequence Algorithm.
   */
  public readonly privateParameters: string[] = ['k'];

  /**
   * Validates the provided JSON Web Key Parameters.
   *
   * @param parameters Parameters of the JSON Web Key.
   */
  public validateParameters(parameters: JsonWebKeyParameters): void {
    if (typeof parameters.k !== 'string') {
      throw new InvalidJsonWebKeyException('Invalid key parameter "k".');
    }

    if (Buffer.byteLength(parameters.k, 'base64url') === 0) {
      throw new InvalidJsonWebKeyException('The Secret cannot be empty.');
    }
  }

  /**
   * Loads the provided JSON Web Key into a NodeJS Crypto Key.
   *
   * @param parameters Parameters of the JSON Web Key.
   * @returns NodeJS Crypto Key.
   */
  public loadCryptoKey(parameters: JsonWebKeyParameters): KeyObject {
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    return createSecretKey(parameters.k!, 'base64url');
  }
}
