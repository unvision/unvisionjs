import { Buffer } from 'buffer';
import { createPrivateKey, createPublicKey, JsonWebKeyInput as CryptoJsonWebKeyInput, KeyObject } from 'crypto';

import { InvalidJsonWebKeyException } from '../../exceptions/invalid-jsonwebkey.exception';
import { JsonWebKeyParameters } from '../jsonwebkey.parameters';
import { JsonWebKeyAlgorithm } from './jsonwebkey.algorithm';

/**
 * Implementation of the **RSA** JSON Web Key Algorithm.
 *
 * @see https://www.rfc-editor.org/rfc/rfc7518.html#section-6.3
 */
export class RsaKeyAlgorithm implements JsonWebKeyAlgorithm {
  /**
   * Private Parameters of the JSON Web Key RSA Algorithm.
   */
  public readonly privateParameters: string[] = ['d', 'p', 'q', 'dp', 'dq', 'qi'];

  /**
   * Validates the provided JSON Web Key Parameters.
   *
   * @param parameters Parameters of the JSON Web Key.
   */
  public validateParameters(parameters: JsonWebKeyParameters): void {
    if (typeof parameters.n !== 'string') {
      throw new InvalidJsonWebKeyException('Invalid key parameter "n".');
    }

    if (Buffer.byteLength(parameters.n, 'base64url') < 256) {
      throw new InvalidJsonWebKeyException('The modulus MUST have AT LEAST 2048 bits.');
    }

    if (typeof parameters.e !== 'string') {
      throw new InvalidJsonWebKeyException('Invalid key parameter "e".');
    }

    // TODO: Validate the following values based on the previous ones.
    if (this.privateParameters.some((parameter) => parameters[parameter] !== undefined)) {
      if (typeof parameters.d !== 'string') {
        throw new InvalidJsonWebKeyException('Invalid key parameter "d".');
      }

      if (typeof parameters.p !== 'string') {
        throw new InvalidJsonWebKeyException('Invalid key parameter "p".');
      }

      if (typeof parameters.q !== 'string') {
        throw new InvalidJsonWebKeyException('Invalid key parameter "q".');
      }

      if (typeof parameters.dp !== 'string') {
        throw new InvalidJsonWebKeyException('Invalid key parameter "dp".');
      }

      if (typeof parameters.dq !== 'string') {
        throw new InvalidJsonWebKeyException('Invalid key parameter "dq".');
      }

      if (typeof parameters.qi !== 'string') {
        throw new InvalidJsonWebKeyException('Invalid key parameter "qi".');
      }
    }
  }

  /**
   * Loads the provided JSON Web Key into a NodeJS Crypto Key.
   *
   * @param parameters Parameters of the JSON Web Key.
   * @returns NodeJS Crypto Key.
   */
  public loadCryptoKey(parameters: JsonWebKeyParameters): KeyObject {
    const input: CryptoJsonWebKeyInput = { format: 'jwk', key: parameters };
    return parameters.d === undefined ? createPublicKey(input) : createPrivateKey(input);
  }
}
