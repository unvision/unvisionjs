import { Buffer } from 'buffer';

import { InvalidJsonWebKeyException } from '../../exceptions/invalid-jsonwebkey.exception';
import { JsonWebKeyParameters } from '../jsonwebkey.parameters';
import { OctKeyAlgorithm } from './octkey.algorithm';

const secretParams: JsonWebKeyParameters = { kty: 'oct', k: 'qDM80igvja4Tg_tNsEuWDhl2bMM6_NgJEldFhIEuwqQ' };

const invalidSecrets: any[] = [undefined, null, true, 1, 1.2, 1n, Buffer.alloc(1), Symbol.for('foo'), () => 1, {}, []];

const algorithm = new OctKeyAlgorithm();

describe('JSON Web Key Octet Sequence Algorithm', () => {
  describe('privateParameters', () => {
    it('should have ["k"] as its value.', () => {
      expect(algorithm.privateParameters).toEqual(['k']);
    });
  });

  describe('validateParameters()', () => {
    it.each(invalidSecrets)('should throw when passing an invalid secret.', (k) => {
      expect(() => algorithm.validateParameters({ kty: 'oct', k })).toThrow(
        new InvalidJsonWebKeyException('Invalid key parameter "k".')
      );
    });

    it('should throw when passing an empty secret.', () => {
      expect(() => algorithm.validateParameters({ kty: 'oct', k: '' })).toThrow(
        new InvalidJsonWebKeyException('The Secret cannot be empty.')
      );
    });
  });

  describe('loadCryptoKey()', () => {
    it('should load a secret octet sequence key.', () => {
      const secretKey = algorithm.loadCryptoKey(secretParams);

      expect(secretKey.type).toBe('secret');
    });
  });
});
