import { createPrivateKey, createPublicKey, JsonWebKeyInput as CryptoJsonWebKeyInput, KeyObject } from 'crypto';

import { InvalidJsonWebKeyException } from '../../exceptions/invalid-jsonwebkey.exception';
import { UnsupportedEllipticCurveException } from '../../exceptions/unsupported-elliptic-curve.exception';
import { JsonWebKeyParameters } from '../jsonwebkey.parameters';
import { JsonWebKeyAlgorithm } from './jsonwebkey.algorithm';
import { SupportedEllipticCurve } from './types/supported-elliptic-curve';

/**
 * Meta information of the Elliptic Curves.
 */
interface EllipticCurveParams {
  /**
   * Identifier of the Elliptic Curve.
   */
  readonly id: SupportedEllipticCurve;

  /**
   * Name of the Elliptic Curve as registered with NodeJS' `crypto` module.
   */
  readonly name: string;

  /**
   * String representation of the Object Identifier of the Elliptic Curve.
   */
  readonly oid: string;

  /**
   * Length of the Private Value and parameters of the key's Coordinate.
   */
  readonly length: number;
}

/**
 * Elliptic Curves Registry.
 */
const ELLIPTIC_CURVES_REGISTRY: EllipticCurveParams[] = [
  { id: 'P-256', name: 'prime256v1', oid: '1.2.840.10045.3.1.7', length: 32 },
  { id: 'P-384', name: 'secp384r1', oid: '1.3.132.0.34', length: 48 },
  { id: 'P-521', name: 'secp521r1', oid: '1.3.132.0.35', length: 66 },
];

/**
 * Implementation of the **Elliptic Curve** JSON Web Key Algorithm.
 *
 * @see https://www.rfc-editor.org/rfc/rfc7518.html#section-6.2
 */
export class EcKeyAlgorithm implements JsonWebKeyAlgorithm {
  /**
   * Private Parameters of the JSON Web Key Elliptic Curve Algorithm.
   */
  public readonly privateParameters: string[] = ['d'];

  /**
   * Validates the provided JSON Web Key Parameters.
   *
   * @param parameters Parameters of the JSON Web Key.
   */
  public validateParameters(parameters: JsonWebKeyParameters): void {
    if (typeof parameters.crv !== 'string') {
      throw new InvalidJsonWebKeyException('Invalid parameter "crv".');
    }

    const curve = ELLIPTIC_CURVES_REGISTRY.find((ellipticCurve) => ellipticCurve.id === parameters.crv);

    if (curve === undefined) {
      throw new UnsupportedEllipticCurveException(`Unsupported Elliptic Curve "${parameters.crv}".`);
    }

    if (typeof parameters.x !== 'string') {
      throw new InvalidJsonWebKeyException('Invalid key parameter "x".');
    }

    if (typeof parameters.y !== 'string') {
      throw new InvalidJsonWebKeyException('Invalid key parameter "y".');
    }

    if (parameters.d !== undefined) {
      if (typeof parameters.d !== 'string') {
        throw new InvalidJsonWebKeyException('Invalid key parameter "d".');
      }
    }
  }

  /**
   * Loads the provided JSON Web Key into a NodeJS Crypto Key.
   *
   * @param parameters Parameters of the JSON Web Key.
   * @returns NodeJS Crypto Key.
   */
  public loadCryptoKey(parameters: JsonWebKeyParameters): KeyObject {
    const input: CryptoJsonWebKeyInput = { format: 'jwk', key: parameters };
    return parameters.d === undefined ? createPublicKey(input) : createPrivateKey(input);
  }
}
