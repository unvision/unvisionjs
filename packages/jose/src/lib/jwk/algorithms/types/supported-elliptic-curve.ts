/**
 * Elliptic Curves supported by UnVision.
 */
export type SupportedEllipticCurve = 'P-256' | 'P-384' | 'P-521';
