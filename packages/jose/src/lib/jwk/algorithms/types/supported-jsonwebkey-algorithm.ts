/**
 * JSON Web Key Algorithms supported by UnVision.
 */
export type SupportedJsonWebKeyAlgorithm = 'EC' | 'RSA' | 'oct';
