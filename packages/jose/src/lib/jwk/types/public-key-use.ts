/**
 * Indicates whether a Public Key is used for Plaintext Encryption or Signature Verification.
 */
export type PublicKeyUse = 'enc' | 'sig';
