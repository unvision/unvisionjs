import { KeyObject } from 'crypto';

import { InvalidJsonWebKeyException } from '../exceptions/invalid-jsonwebkey.exception';
import { JoseException } from '../exceptions/jose.exception';
import { UnsupportedAlgorithmException } from '../exceptions/unsupported-algorithm.exception';
import { SupportedJsonWebEncryptionKeyWrapAlgorithm } from '../jwe/algorithms/alg/types/supported-jsonwebencryption-keywrap-algorithm';
import { SupportedJsonWebSignatureAlgorithm } from '../jws/algorithms/types/supported-jsonwebsignature-algorithm';
import { EcKeyAlgorithm } from './algorithms/eckey.algorithm';
import { JsonWebKeyAlgorithm } from './algorithms/jsonwebkey.algorithm';
import { OctKeyAlgorithm } from './algorithms/octkey.algorithm';
import { RsaKeyAlgorithm } from './algorithms/rsakey.algorithm';
import { SupportedEllipticCurve } from './algorithms/types/supported-elliptic-curve';
import { SupportedJsonWebKeyAlgorithm } from './algorithms/types/supported-jsonwebkey-algorithm';
import { JsonWebKeyParameters } from './jsonwebkey.parameters';
import { KeyOperation } from './types/key-operation';
import { PublicKeyUse } from './types/public-key-use';

/**
 * Implementation of a JSON Web Key.
 *
 * @see https://www.rfc-editor.org/rfc/rfc7517.html#section-4
 */
export class JsonWebKey implements JsonWebKeyParameters {
  /**
   * Type of the JSON Web Key.
   */
  public readonly kty!: SupportedJsonWebKeyAlgorithm;

  /**
   * Octet Sequence Base64Url encoded Secret.
   */
  public readonly k?: string;

  /**
   * Elliptic Curve.
   */
  public readonly crv?: SupportedEllipticCurve;

  /**
   * Elliptic Curve X Coordinate.
   */
  public readonly x?: string;

  /**
   * Elliptic Curve Y Coordinate.
   */
  public readonly y?: string;

  /**
   * RSA Modulus.
   */
  public readonly n?: string;

  /**
   * RSA Public Exponent.
   */
  public readonly e?: string;

  /**
   * Elliptic Curve Private Value.
   * RSA Private Exponent.
   */
  public readonly d?: string;

  /**
   * RSA First Prime Factor.
   */
  public readonly p?: string;

  /**
   * RSA Second Prime Factor.
   */
  public readonly q?: string;

  /**
   * RSA First Factor CRT Exponent.
   */
  public readonly dp?: string;

  /**
   * RSA Second Factor CRT Exponent.
   */
  public readonly dq?: string;

  /**
   * RSA First Factor CRT Coefficient.
   */
  public readonly qi?: string;

  /**
   * Indicates whether a Public JSON Web Key is used for Plaintext Encryption or Signature Verification.
   */
  public readonly use?: PublicKeyUse;

  /**
   * Operations for which the JSON Web Key are intended to be used.
   */
  public readonly key_ops?: KeyOperation[];

  /**
   * JSON Web Encryption Key Wrap Algorithm or JSON Web Signature Algorithm allowed to use this JSON Web Key.
   */
  public readonly alg?: SupportedJsonWebEncryptionKeyWrapAlgorithm | SupportedJsonWebSignatureAlgorithm;

  /**
   * Identifier of the JSON Web Key.
   */
  public readonly kid?: string;

  /**
   * URL of the X.509 certificate of the JSON Web Key.
   */
  public readonly x5u?: string;

  /**
   * Chain of X.509 certificates of the JSON Web Key.
   */
  public readonly x5c?: string[];

  /**
   * SHA-1 Thumbprint of the X.509 certificate of the JSON Web Key.
   */
  public readonly x5t?: string;

  /**
   * SHA-256 Thumbprint of the X.509 certificate of the JSON Web Key.
   */
  public readonly 'x5t#S256'?: string;

  /**
   * Additional JSON Web Key Parameters.
   */
  readonly [parameter: string]: any;

  /**
   * Supported JSON Web Key Algorithms.
   */
  private static readonly algorithms: Record<SupportedJsonWebKeyAlgorithm, JsonWebKeyAlgorithm> = {
    EC: new EcKeyAlgorithm(),
    RSA: new RsaKeyAlgorithm(),
    oct: new OctKeyAlgorithm(),
  };

  /**
   * NodeJS Crypto Key.
   */
  protected readonly cryptoKey!: KeyObject;

  /**
   * Instantiates a new JSON Web Key based on the provided Parameters.
   *
   * @param parameters Parameters of the JSON Web Key.
   * @param additionalParameters Additional JSON Web Key Parameters. Overrides the attributes of `parameters`.
   */
  public constructor(parameters: JsonWebKeyParameters, additionalParameters: Partial<JsonWebKeyParameters> = {}) {
    const params = { ...parameters, ...additionalParameters };

    if (params.kty === undefined) {
      throw new InvalidJsonWebKeyException('Invalid key parameter "kty".');
    }

    let algorithm: JsonWebKeyAlgorithm;

    if ((algorithm = JsonWebKey.algorithms[params.kty]) === undefined) {
      throw new UnsupportedAlgorithmException(`Unsupported JSON Web Key Algorithm "${params.kty}".`);
    }

    if (params.use !== undefined && typeof params.use !== 'string') {
      throw new InvalidJsonWebKeyException('Invalid key parameter "use".');
    }

    if (params.key_ops !== undefined) {
      if (
        !Array.isArray(params.key_ops) ||
        params.key_ops.length === 0 ||
        params.key_ops.some((p) => typeof p !== 'string')
      ) {
        throw new InvalidJsonWebKeyException('Invalid key parameter "key_ops".');
      }

      if (new Set(params.key_ops).size !== params.key_ops.length) {
        throw new InvalidJsonWebKeyException('Key parameter "key_ops" cannot have repeated operations.');
      }
    }

    if (params.use !== undefined && params.key_ops !== undefined) {
      const sig: KeyOperation[] = ['sign', 'verify'];
      const enc: KeyOperation[] = ['encrypt', 'decrypt', 'wrapKey', 'unwrapKey', 'deriveKey', 'deriveBits'];

      if (
        (params.use === 'sig' && params.key_ops.some((p) => !sig.includes(p))) ||
        (params.use === 'enc' && params.key_ops.some((p) => !enc.includes(p)))
      ) {
        throw new InvalidJsonWebKeyException('Invalid combination of "use" and "key_ops".');
      }
    }

    if (params.alg !== undefined && typeof params.alg !== 'string') {
      throw new InvalidJsonWebKeyException('Invalid key parameter "alg".');
    }

    if (params.kid !== undefined && typeof params.kid !== 'string') {
      throw new InvalidJsonWebKeyException('Invalid key parameter "kid".');
    }

    if (params.x5u !== undefined) {
      throw new InvalidJsonWebKeyException('Unsupported key parameter "x5u".');
    }

    if (params.x5c !== undefined) {
      throw new InvalidJsonWebKeyException('Unsupported key parameter "x5c".');
    }

    if (params.x5t !== undefined) {
      throw new InvalidJsonWebKeyException('Unsupported key parameter "x5t".');
    }

    if (params['x5t#S256'] !== undefined) {
      throw new InvalidJsonWebKeyException('Unsupported key parameter "x5t#S256".');
    }

    try {
      algorithm.validateParameters(params);
    } catch (exc: any) {
      throw exc instanceof JoseException ? exc : new InvalidJsonWebKeyException(null, exc);
    }

    Object.defineProperty(this, 'cryptoKey', { value: algorithm.loadCryptoKey(params) });

    Object.assign(this, params);
  }

  /**
   * Parses a JSON String into a JSON Web Key.
   *
   * @param data JSON String representation of the JSON Web Key to be parsed.
   * @returns Instance of a JSON Web Key based on the provided JSON String.
   */
  public static parse(data: string): JsonWebKey {
    try {
      return new JsonWebKey(JSON.parse(data));
    } catch (exc: any) {
      throw exc instanceof JoseException ? exc : new InvalidJsonWebKeyException(null, exc);
    }
  }

  /**
   * Returns the Parameters of the JSON Web Key.
   *
   * @param exportPublic Exports only the Public Parameters of the JSON Web Key.
   */
  public toJSON(exportPublic = true): JsonWebKeyParameters {
    if (!exportPublic) {
      return this;
    }

    const { privateParameters } = JsonWebKey.algorithms[this.kty];

    const publicParameters = Object.keys(this).filter((key) => !privateParameters.includes(key));

    return Object.fromEntries(publicParameters.map((key: any) => [key, Reflect.get(this, key)]));
  }
}
