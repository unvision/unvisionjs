import { Buffer } from 'buffer';
import { randomBytes, randomInt } from 'crypto';

import { InvalidJsonWebKeyException } from '../../exceptions/invalid-jsonwebkey.exception';
import { JsonWebKey } from '../../jwk/jsonwebkey';
import { HS256, HS384, HS512 } from './hmac.algorithm';

const generateOctKey = (size: number): JsonWebKey => {
  return new JsonWebKey({ kty: 'oct', k: randomBytes(size).toString('base64url') });
};

const message = Buffer.from('Super secret message.');

describe('JSON Web Signature HMAC Algorithm HS256', () => {
  it('should reject a small secret.', async () => {
    const key = generateOctKey(randomInt(0, 32));
    await expect(HS256.sign(message, key)).rejects.toThrow(InvalidJsonWebKeyException);
  });

  it('should sign and verify a message.', async () => {
    const key = generateOctKey(32);
    const signature = await HS256.sign(message, key);

    expect(signature).toEqual(expect.any(Buffer));

    await expect(HS256.verify(signature, message, key)).resolves.not.toThrow();
  });
});

describe('JSON Web Signature HMAC Algorithm HS384', () => {
  it('should reject a small secret.', async () => {
    const key = generateOctKey(randomInt(0, 48));
    await expect(HS384.sign(message, key)).rejects.toThrow(InvalidJsonWebKeyException);
  });

  it('should sign and verify a message.', async () => {
    const key = generateOctKey(48);
    const signature = await HS384.sign(message, key);

    expect(signature).toEqual(expect.any(Buffer));

    await expect(HS384.verify(signature, message, key)).resolves.not.toThrow();
  });
});

describe('JSON Web Signature HMAC Algorithm HS512', () => {
  it('should reject a small secret.', async () => {
    const key = generateOctKey(randomInt(0, 64));
    await expect(HS512.sign(message, key)).rejects.toThrow(InvalidJsonWebKeyException);
  });

  it('should sign and verify a message.', async () => {
    const key = generateOctKey(64);
    const signature = await HS512.sign(message, key);

    expect(signature).toEqual(expect.any(Buffer));

    await expect(HS512.verify(signature, message, key)).resolves.not.toThrow();
  });
});
