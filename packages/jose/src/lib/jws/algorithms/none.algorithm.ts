import { Buffer } from 'buffer';

import { JsonWebSignatureAlgorithm } from './jsonwebsignature.algorithm';

/**
 * Implementation of the JSON Web Signature **none** Algorithm.
 */
class NoneAlgorithm extends JsonWebSignatureAlgorithm {
  /**
   * Instantiates a new JSON Web Signature **none** Algorithm to Sign and Verify Messages.
   */
  public constructor() {
    super('none');
  }

  /**
   * Signs a Message with the provided JSON Web Key.
   *
   * @param _message Message to be Signed.
   * @returns Resulting Signature of the provided Message.
   */
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  public async sign(_message: Buffer): Promise<Buffer> {
    return Buffer.alloc(0);
  }

  /**
   * Checks if the provided Signature matches the provided Message based on the provide JSON Web Key.
   *
   * @param _signature Signature to be matched against the provided Message.
   * @param _message Message to be matched against the provided Signature.
   */
  // eslint-disable-next-line @typescript-eslint/no-unused-vars, @typescript-eslint/no-empty-function
  public async verify(_signature: Buffer, _message: Buffer): Promise<void> {}
}

/**
 * No digital signature or MAC performed.
 */
export const none = new NoneAlgorithm();
