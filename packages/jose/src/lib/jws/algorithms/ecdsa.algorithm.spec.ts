import { Buffer } from 'buffer';
import { generateKeyPairSync } from 'crypto';

import { InvalidJsonWebKeyException } from '../../exceptions/invalid-jsonwebkey.exception';
import { SupportedEllipticCurve } from '../../jwk/algorithms/types/supported-elliptic-curve';
import { JsonWebKey } from '../../jwk/jsonwebkey';
import { JsonWebKeyParameters } from '../../jwk/jsonwebkey.parameters';
import { ES256, ES384, ES512 } from './ecdsa.algorithm';

const generateEcKey = (curve: SupportedEllipticCurve): JsonWebKey => {
  const curves: Record<SupportedEllipticCurve, string> = {
    'P-256': 'prime256v1',
    'P-384': 'secp384r1',
    'P-521': 'secp521r1',
  };

  const { privateKey } = generateKeyPairSync('ec', { namedCurve: curves[curve] });

  return new JsonWebKey(<JsonWebKeyParameters>privateKey.export({ format: 'jwk' }));
};

const message = Buffer.from('Super secret message.');

describe('JWS ECDSA Algorithm ES256', () => {
  it('should reject a different curve.', async () => {
    const key = generateEcKey('P-384');
    await expect(ES256.sign(message, key)).rejects.toThrow(InvalidJsonWebKeyException);
  });

  it('should sign and verify a message.', async () => {
    const key = generateEcKey('P-256');
    const signature = await ES256.sign(message, key);

    expect(signature).toEqual(expect.any(Buffer));

    await expect(ES256.verify(signature, message, key)).resolves.not.toThrow();
  });
});

describe('JWS ECDSA Algorithm ES384', () => {
  it('should reject a different curve.', async () => {
    const key = generateEcKey('P-521');
    await expect(ES384.sign(message, key)).rejects.toThrow(InvalidJsonWebKeyException);
  });

  it('should sign and verify a message.', async () => {
    const key = generateEcKey('P-384');
    const signature = await ES384.sign(message, key);

    expect(signature).toEqual(expect.any(Buffer));

    await expect(ES384.verify(signature, message, key)).resolves.not.toThrow();
  });
});

describe('JWS ECDSA Algorithm ES512', () => {
  it('should reject a different curve.', async () => {
    const key = generateEcKey('P-256');
    await expect(ES512.sign(message, key)).rejects.toThrow(InvalidJsonWebKeyException);
  });

  it('should sign a message.', async () => {
    const key = generateEcKey('P-521');
    const signature = await ES512.sign(message, key);

    expect(signature).toEqual(expect.any(Buffer));

    await expect(ES512.verify(signature, message, key)).resolves.not.toThrow();
  });
});
