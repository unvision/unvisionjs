import { Buffer } from 'buffer';
import { createHmac, KeyObject, timingSafeEqual } from 'crypto';

import { InvalidJsonWebKeyException } from '../../exceptions/invalid-jsonwebkey.exception';
import { InvalidJsonWebSignatureException } from '../../exceptions/invalid-jsonwebsignature.exception';
import { JsonWebKey } from '../../jwk/jsonwebkey';
import { JsonWebSignatureAlgorithm } from './jsonwebsignature.algorithm';
import { SupportedJsonWebSignatureAlgorithm } from './types/supported-jsonwebsignature-algorithm';

/**
 * Implementation of the JSON Web Signature HMAC Algorithm.
 */
class HmacAlgorithm extends JsonWebSignatureAlgorithm {
  /**
   * Hash Algorithm used to Sign and Verify Messages.
   */
  protected override readonly hash!: string;

  /**
   * Size of the Secret accepted by the JSON Web Signature HMAC Algorithm.
   */
  protected readonly keySize: number;

  /**
   * Instantiates a new JSON Web Signature HMAC Algorithm to Sign and Verify Messages.
   *
   * @param keySize Size of the Secret accepted by the JSON Web Signature HMAC Algorithm.
   */
  public constructor(keySize: number) {
    const bitSize = keySize << 3;

    super(<SupportedJsonWebSignatureAlgorithm>`HS${bitSize}`, `SHA${bitSize}`, 'oct');

    this.keySize = keySize;
  }

  /**
   * Signs a Message with the provided JSON Web Key.
   *
   * @param message Message to be Signed.
   * @param key JSON Web Key used to Sign the provided Message.
   * @returns Resulting Signature of the provided Message.
   */
  public async sign(message: Buffer, key: JsonWebKey): Promise<Buffer> {
    this.validateJsonWebKey(key);

    const cryptoKey: KeyObject = key['cryptoKey'];
    const signature = createHmac(this.hash, cryptoKey).update(message).digest();

    return signature;
  }

  /**
   * Checks if the provided Signature matches the provided Message based on the provide JSON Web Key.
   *
   * @param signature Signature to be matched against the provided Message.
   * @param message Message to be matched against the provided Signature.
   * @param key JSON Web Key used to verify the Signature and Message.
   */
  public async verify(signature: Buffer, message: Buffer, key: JsonWebKey): Promise<void> {
    this.validateJsonWebKey(key);

    const calculatedSignature = await this.sign(message, key);

    if (!timingSafeEqual(signature, calculatedSignature)) {
      throw new InvalidJsonWebSignatureException();
    }
  }

  /**
   * Checks if the provided JSON Web Key can be used by the JSON Web Signature HMAC Algorithm.
   *
   * @param key JSON Web Key to be checked.
   * @throws {InvalidJsonWebKeyException} The provided JSON Web Key is invalid.
   */
  protected override validateJsonWebKey(key: JsonWebKey): void {
    super.validateJsonWebKey(key);

    if (Buffer.from(<string>key.k, 'base64url').length < this.keySize) {
      throw new InvalidJsonWebKeyException(`The size of the OctKey Secret must be at least ${this.keySize} bytes.`);
    }
  }
}

/**
 * HMAC using SHA-256.
 */
export const HS256 = new HmacAlgorithm(32);

/**
 * HMAC using SHA-384.
 */
export const HS384 = new HmacAlgorithm(48);

/**
 * HMAC using SHA-512.
 */
export const HS512 = new HmacAlgorithm(64);
