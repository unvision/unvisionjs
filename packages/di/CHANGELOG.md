# Changelog

This file was generated using [@jscutlery/semver](https://github.com/jscutlery/semver).

## [0.2.0](https://gitlab.com/unvision/unvisionjs/compare/di-0.1.1...di-0.2.0) (2022-08-22)


### Features

* **di:** add abstract constructor type ([5aa5797](https://gitlab.com/unvision/unvisionjs/commit/5aa57972e0a6a8c3fa61fb2951d3c9ee40106b72))


### Bug Fixes

* **di:** add import buffer package ([4757dfa](https://gitlab.com/unvision/unvisionjs/commit/4757dfa12734989c77a022c7eafcc6dfcd153984))

## [0.1.1](https://gitlab.com/unvision/unvisionjs/compare/di-0.1.0...di-0.1.1) (2022-07-01)


### Bug Fixes

* **di:** add exports to index.ts ([d2157b7](https://gitlab.com/unvision/unvisionjs/commit/d2157b785a9d35d41aab7001a84d168d8b822e89))
* **di:** exclude stub files from distribution ([1b83c7d](https://gitlab.com/unvision/unvisionjs/commit/1b83c7dc01efa6fd99e044c59aba5495536f1b5b))

## 0.1.0 (2022-07-01)


### Features

* **di:** code migration from @guarani/di ([5ef221a](https://gitlab.com/unvision/unvisionjs/commit/5ef221aa4cc84309139190248da8b1593839c463))
