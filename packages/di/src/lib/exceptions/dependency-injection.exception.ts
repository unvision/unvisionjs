import { Exception } from '@unvision/primitives';

/**
 * Base class for the Dependency Injection Exceptions.
 */
export abstract class DependencyInjectionException extends Exception {}
