import { PARAMTOKENS, PROPTOKENS } from '../metadata/metadata.keys';
import { setTokenDescriptor } from '../metadata/set-token-descriptor';
import { AbstractConstructor } from '../types/abstract-constructor.type';
import { Constructor } from '../types/constructor.type';
import { InjectableToken } from '../types/injectable-token.type';

/**
 * Injects all the resolved instances of the requested Token.
 *
 * @param token Token used to resolve the dependencies.
 */
export function InjectAll<T = any>(token: InjectableToken<T>): ParameterDecorator & PropertyDecorator {
  return function (
    target: object | AbstractConstructor<T> | Constructor<T>,
    propertyKey: string | symbol | undefined,
    parameterIndex?: number
  ): void {
    // Injects into an argument of the constructor.
    if (propertyKey === undefined && parameterIndex !== undefined && typeof target !== 'object') {
      setTokenDescriptor<T>(PARAMTOKENS, target, parameterIndex, { token, multiple: true });
    }

    // Injects into a property of the target.
    if (propertyKey !== undefined && parameterIndex === undefined) {
      setTokenDescriptor<T>(PROPTOKENS, target, propertyKey, { token, multiple: true });
    }
  };
}
