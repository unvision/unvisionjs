import { PARAMTOKENS, PROPTOKENS } from '../metadata/metadata.keys';
import { setTokenDescriptor } from '../metadata/set-token-descriptor';
import { AbstractConstructor } from '../types/abstract-constructor.type';
import { Constructor } from '../types/constructor.type';
import { InjectableToken } from '../types/injectable-token.type';

/**
 * Marks the parameter or property for injection by the Container.
 *
 * @param token Token used to resolve the dependency.
 */
export function Inject<T = any>(token?: InjectableToken<T>): ParameterDecorator & PropertyDecorator {
  return function (
    target: object | AbstractConstructor<T> | Constructor<T>,
    propertyKey: string | symbol | undefined,
    parameterIndex?: number
  ): void {
    // Injects into an argument of the constructor.
    if (propertyKey === undefined && parameterIndex !== undefined && typeof target !== 'object') {
      const paramType: InjectableToken<any> = Reflect.getMetadata('design:paramtypes', target)[parameterIndex];
      setTokenDescriptor<T>(PARAMTOKENS, target, parameterIndex, { token: token ?? paramType, multiple: false });
    }

    // Injects into a property of the target.
    if (propertyKey !== undefined && parameterIndex === undefined) {
      const propType = Reflect.getMetadata('design:type', target, propertyKey);
      setTokenDescriptor<T>(PROPTOKENS, target, propertyKey, { token: token ?? propType, multiple: false });
    }
  };
}
