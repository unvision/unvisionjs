import { PARAMTOKENS, PROPTOKENS } from '../metadata/metadata.keys';
import { setTokenDescriptor } from '../metadata/set-token-descriptor';
import { AbstractConstructor } from '../types/abstract-constructor.type';
import { Constructor } from '../types/constructor.type';

/**
 * Marks the dependency as **optional**.
 *
 * This informs the Dependency Injection Container to inject **undefined** when the Injectable Token
 * of the dependency is not registered.
 */
export function Optional(): ParameterDecorator & PropertyDecorator {
  return function (
    target: object | AbstractConstructor<any> | Constructor<any>,
    propertyKey: string | symbol | undefined,
    parameterIndex?: number
  ): void {
    // Constructor parameters
    if (propertyKey === undefined && parameterIndex !== undefined && typeof target !== 'object') {
      setTokenDescriptor<any>(PARAMTOKENS, target, parameterIndex, { optional: true });
    }

    // Target's property
    if (propertyKey !== undefined && parameterIndex === undefined) {
      setTokenDescriptor<any>(PROPTOKENS, target, propertyKey, { optional: true });
    }
  };
}
