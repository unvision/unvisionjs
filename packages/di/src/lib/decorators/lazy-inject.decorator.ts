import { PARAMTOKENS, PROPTOKENS } from '../metadata/metadata.keys';
import { setTokenDescriptor } from '../metadata/set-token-descriptor';
import { AbstractConstructor } from '../types/abstract-constructor.type';
import { Constructor } from '../types/constructor.type';
import { LazyToken } from '../types/lazy-token.type';

/**
 * The LazyToken is a special InjectableToken used to allow the injection of Tokens that,
 * otherwise, would cause a Circular Dependency error.
 *
 * @param wrappedToken Function that wraps the Constructor Injectable Token to be lazy injected.
 */
export function LazyInject<T = any>(wrappedToken: () => Constructor<T>): PropertyDecorator & ParameterDecorator {
  return function (
    target: object | AbstractConstructor<T> | Constructor<T>,
    propertyKey: string | symbol | undefined,
    parameterIndex?: number
  ): void {
    const lazyToken = new LazyToken(wrappedToken);

    // Injects into an argument of the constructor.
    if (propertyKey === undefined && parameterIndex !== undefined && typeof target !== 'object') {
      setTokenDescriptor<T>(PARAMTOKENS, target, parameterIndex, { token: lazyToken, multiple: false });
    }

    // Injects into a property of the target.
    if (propertyKey !== undefined && parameterIndex === undefined) {
      setTokenDescriptor<T>(PROPTOKENS, target, propertyKey, { token: lazyToken, multiple: false });
    }
  };
}
