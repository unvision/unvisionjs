/**
 * Metadata Key for the Constructor Parameters.
 */
export const PARAMTOKENS = Symbol('unvision:di:paramtokens');

/**
 * Metadata Key for the list of Constructor Parameters' Descriptors.
 */
export const PARAMTYPES = Symbol('unvision:di:paramtypes');

/**
 * Metadata Key for the Properties Parameters.
 */
export const PROPTOKENS = Symbol('unvision:di:proptokens');
