import { AbstractConstructor } from './abstract-constructor.type';
import { Constructor } from './constructor.type';
import { LazyToken } from './lazy-token.type';

/**
 * Describes the acceptable formats for an Injectable token.
 */
export type InjectableToken<T> = string | symbol | AbstractConstructor<T> | Constructor<T> | LazyToken<T>;

/**
 * Checks if the provided object is an Injectable Token.
 *
 * @param obj Object to be checked.
 */
export function isInjectableToken<T>(obj: unknown): obj is InjectableToken<T> {
  return (
    typeof obj === 'string' ||
    typeof obj === 'symbol' ||
    (typeof obj === 'function' && obj.prototype !== undefined) ||
    obj instanceof LazyToken<T>
  );
}
